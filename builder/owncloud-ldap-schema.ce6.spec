# $Id$
# Authority: Marc
# Upstream: Marc Serrat <marc@parallels.com>
 
%define real_name owncloud-ldap-schema
%define version 1.0
%define release %(echo ${BUILD_NUMBER:-unknown})
 
Summary: RPM To initiate a clean openldap installation for having owncloud
 
Name: owncloud-ldap-schema
Version: %{version}
Release: %{release}.ce6
License: Artistic/GPL
Group: Development
 
%define tmp_build_dir %(mktemp -d %{_tmppath}/%{name}-XXXXXX)

 
BuildArch: noarch
BuildRoot: %{tmp_build_dir}
 
Requires: redhat-release >= 6.1 redhat-release < 7.0 centos-release-SCL mod_ssl mysql-server aps-php-runtime openldap openldap-servers openldap-clients php54-php-common php54-php-pdo php54-php php54-php-xml php54-php-pecl-apc php54-php-gd php54-php-pecl-memcache php54-php-pecl-zendopcache php54-php-odbc php54-php-xmlrpc php54-php-recode php54-php-snmp php54-php-dba php54-php-mbstring php54-php-fpm php54-php-ldap php54-scldevel php54-runtime php54-php-cli php54-php-devel php54-php-process php54-php-pear php54 php54-php-mysqlnd php54-php-pgsql php54-php-bcmath php54-php-tidy php54-php-enchant php54-php-soap php54-php-pspell php54-php-intl php54-php-imap
 
Source: owncloud-ldap-schema.tar.gz
 
%description
RPM To initiate a clean openldap installation for having owncloud
 
%prep
%setup -n %{name} -q
 
%build
 
%install
tar -xvvzf owncloud-ldap-schema.tar.gz -C $RPM_BUILD_ROOT/
 
### Clean up buildroot
 
### Clean up docs
 
%clean
rm -rf $RPM_BUILD_ROOT
 
%files
%defattr(-, root, root, 0755)
/usr/share/doc/owncloud-ldap/nis.schema
/usr/share/doc/owncloud-ldap/owncloud-base.ldif
/usr/share/doc/owncloud-ldap/owncloud.sql
/usr/share/doc/owncloud-ldap/owncloudsite.tar.gz
/usr/share/doc/owncloud-ldap/nis.ldif
/usr/share/doc/owncloud-ldap/ldap.conf
/usr/share/doc/owncloud-ldap/slapd.conf
/usr/share/doc/owncloud-ldap/cronquotas
/usr/share/doc/owncloud-ldap/tester.php
/usr/share/doc/owncloud-ldap/updater.sh
/usr/share/doc/owncloud-ldap/updater2.sh
/usr/share/doc/owncloud-ldap/ocapps.sh
/var/www/html/OwnCloud-ldap/globals.php
/var/www/html/OwnCloud-ldap/index.php
/var/www/html/OwnCloud-ldap/samples.php
/var/www/html/OwnCloud-ldap/tenant.php
/var/www/html/OwnCloud-ldap/domain.php
/var/www/html/OwnCloud-ldap/users.php
/var/www/html/OwnCloud-ldap/tobehtaccess
/var/www/html/OwnCloud-ldap/updatequotas.php
/var/www/html/OwnCloud-ldap/additionalexamples/applicationcreatingresource.php
/var/www/html/OwnCloud-ldap/additionalexamples/applicationlistingusers.php
/var/www/html/OwnCloud-ldap/additionalexamples/getadmins.php
 
%post -p /bin/bash
cp /usr/share/openldap-servers/DB_CONFIG.example /var/lib/ldap/DB_CONFIG
perl -pi -e 's/SLAPD_LDAPS=no/SLAPD_LDAPS=YES/g' /etc/sysconfig/ldap
perl -pi -e 's/SLAPD_LDAP=yes/SLAPD_LDAP=NO/g' /etc/sysconfig/ldap
perl -pi -e 's/SLAPD_LDAPI=yes/SLAPD_LDAPI=NO/g' /etc/sysconfig/ldap
echo "SLAPD_OPTIONS=-4" >> /etc/sysconfig/ldap
perl -pi -e 's/AllowOverride None/AllowOverride All/g' /etc/httpd/conf/httpd.conf
perl -pi -e 's/#SSLVerifyClient require/SSLVerifyClient optional_no_ca/g' /etc/httpd/conf.d/ssl.conf
echo 'SLAPD_URLS="ldaps://127.0.0.1"' >> /etc/sysconfig/ldap
echo 'LDAPTLS_REQCERT=NEVER' >> /etc/php.d/ldap.ini 
mkdir -p /var/www/html/OwnCloud-ldap/config
echo "order deny,allow" > /var/www/html/OwnCloud-ldap/config/.htaccess
echo "deny from all" >> /var/www/html/OwnCloud-ldap/config/.htaccess
openssl req -new -x509 -nodes -subj "/C=US/ST=None/L=None/O=None/CN=www.owncloud.com" -out /etc/pki/tls/certs/owncloud_cert.pem -keyout /etc/pki/tls/certs/owncloud_key.pem -days 365
chown -Rf root:ldap /etc/pki/tls/certs/owncloud_cert.pem
chown -Rf root:ldap /etc/pki/tls/certs/owncloud_key.pem
chmod -Rf 750 /etc/pki/tls/certs/owncloud_key.pem
cp -f /usr/share/doc/owncloud-ldap/ldap.conf /etc/openldap
cp -f /usr/share/doc/owncloud-ldap/slapd.conf /etc/openldap
PASSWORD=`openssl rand -base64 25 | head -c 24`
SLAPD=`slappasswd -s $PASSWORD`
SLAPDM=`echo $PASSWORD | base64`
sleep 2
SLAPD=`echo $SLAPD | sed -e 's/\//\\\\\//g'`
perl -pi -e 's/poalab/'$SLAPD'/g' /etc/openldap/slapd.conf
sleep 2
/etc/rc.d/init.d/slapd restart
sleep 2
cat /usr/share/doc/owncloud-ldap/nis.schema > /etc/openldap/schema/nis.schema
cat /usr/share/doc/owncloud-ldap/nis.ldif > /etc/openldap/schema/nis.ldif
rm -rf /etc/openldap/slapd.d
mkdir -p /etc/openldap/slapd.d
slaptest -f /etc/openldap/slapd.conf -F /etc/openldap/slapd.d
chown -R ldap:ldap /etc/openldap/slapd.d
chown -R ldap:ldap /var/lib/ldap/
chmod 0600 /etc/openldap/slapd.conf
/etc/init.d/slapd restart
sleep 2
ldapadd -x -w $PASSWORD -D "cn=admin,dc=owncloud,dc=com" -f /usr/share/doc/owncloud-ldap/owncloud-base.ldif
php /usr/share/doc/owncloud-ldap/tester.php $PASSWORD yes
tar -xvvzf /usr/share/doc/owncloud-ldap/owncloudsite.tar.gz -C /var/www/html/
chown -R apache:apache /var/www/html
mv /var/www/html/OwnCloud-ldap/tobehtaccess /var/www/html/OwnCloud-ldap/.htaccess
/etc/init.d/httpd restart
cp /usr/share/doc/owncloud-ldap/cronquotas /etc/cron.d/
/etc/init.d/crond restart
/etc/rc.d/init.d/mysqld restart
mysql -e"create database owncloud"
mysql owncloud < /usr/share/doc/owncloud-ldap/owncloud.sql
sleep 10
chmod 750 -R /var/www/html/OwnCloud-ldap
chmod 750 -R /var/www/html/owncloudsite
mkdir -p /var/www/ownclouddata
chown apache:apache /var/www/ownclouddata
chmod 750 /var/www/ownclouddata
mkdir -p /var/www/ownclouddata_deletedusers
chown apache:apache /var/www/ownclouddata_deletedusers
chmod 750 /var/www/ownclouddata_deletedusers
if [ -f /root/.my.cnf ];
then
	echo "Mysql was already secured"
else
	mysql -e"update user set password=password('$PASSWORD') where password=''" mysql
	mysql -e"update oc_appconfig set configvalue='$SLAPDM' where configkey='ldap_agent_password'" owncloud
	echo "[client]" > /root/.my.cnf
	echo "user=root" >> /root/.my.cnf
	echo "pass=$PASSWORD" >> /root/.my.cnf
	echo "Password of mysql set to $PASSWORD"
	perl -pi -e 's/\[mysqld\]/\[mysqld\]\nbind-address = 127.0.0.1/g' /etc/my.cnf
	sleep 2
	chmod 0700 /root/.my.cnf
	/etc/init.d/mysqld restart
	sleep 2
fi
/sbin/chkconfig mysqld on
/sbin/chkconfig slapd on
#echo "Deploying owncloud site from internet"
#sh /usr/share/doc/owncloud-ldap/updater.sh 5.0.18
#echo "Updating to version 6.0.6"
#sh /usr/share/doc/owncloud-ldap/updater.sh 6.0.6
#echo "Updating to version 7.0.4"
#sh /usr/share/doc/owncloud-ldap/updater.sh 7.0.4
#echo "fixing ldap password after 7.0.4 upgrade"
#/usr/bin/php /var/www/html/owncloudsite/console.php ldap:set-config "" ldapAgentPassword $PASSWORD
#echo "Installing 3ty party owncloud apps"
#sh /usr/share/doc/owncloud-ldap/ocapps.sh
source /opt/rh/php54/enable
perl -pi -e 's/5.0.20/8.1.3/g' /var/www/html/owncloudsite/config/config.php
cp /etc/php.d/aps* /opt/rh/php54/root/etc/php.d/
mv  /etc/httpd/conf.d/php.conf  /etc/httpd/conf.d/php.old.disabled
/etc/init.d/httpd restart
source /opt/rh/php54/enable
/usr/bin/chsh apache -s /bin/bash
su apache source /opt/rh/php54/enable
bash /usr/share/doc/owncloud-ldap/updater2.sh 8.1.3
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ app:enable user_ldap'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:create-empty-config'
su apache -m -c "/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config \"\" ldapAgentPassword \"$PASSWORD\""
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" hasMemberOfFilterSupport 0'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" homeFolderNamingRule "attr:homeDirectory"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" lastJpegPhotoLookup 0'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapAgentName "cn=admin,dc=owncloud,dc=com"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapAttributesForGroupSearch ou=Group,dc=owncloud,dc=com'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapAttributesForUserSearch "description;emailAddress"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapBackupPort 389'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapBase "dc=owncloud,dc=com"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapBaseGroups "dc=owncloud,dc=com"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapBaseUsers "ou=users,dc=owncloud,dc=com"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapCacheTTL 3'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapEmailAttribute emailAddress'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapGroupDisplayName gidnumber'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapGroupFilter "objectClass=posixGroup"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapGroupFilterMode 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapGroupMemberAssocAttr memberUid'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapHost "ldaps://127.0.0.1"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapLoginFilter "(|(uid=%uid)(email=\$email))"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapLoginFilterEmail 0'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapLoginFilterMode 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapLoginFilterUsername 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapNestedGroups 0'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapPagingSize 500'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapPort 636'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapQuotaAttribute ownCloudQuota'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapUserDisplayName uid'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapUserFilter "objectClass=posixAccount"'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapUserFilterMode 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapUuidGroupAttribute auto'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapUuidUserAttribute auto'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" turnOffCertCheck 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapConfigurationActive 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapExperiencedAdmin 1'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ ldap:set-config "" ldapTLS 0'
su apache -c '/opt/rh/php54/root/usr/bin/php /var/www/html/owncloudsite/occ app:enable user_ldap'
/usr/bin/chsh apache -s /sbin/nologin
%changelog
* Sun Jun 23 2010 Marc Serrat <marc@parallels.com> - 1.0
- Initial package.
