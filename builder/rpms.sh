#!/bin/sh

set -e

dir=`dirname $0`
if [ -n "$dir" ]; then
  dir="$dir/"
fi

###Preparing owncloud-ldap-schema.tar.gz
initplace=$PWD
echo "sample-aps2-owncloud-nextcp version 1.0-${BUILD_NUMBER}" > ../ui/version.html
php ./remplace.php ../APP-META.xml
for i in `find ../scripts -type f`; do php ./remplace.php $i; done
php ./remplace.php ../ui/json/ownclouduser.json
php ./remplace.php ../ui/json/ownclouddomain.json
php ./remplace.php ../ui/json/newSample.json
rm -rf ../ui/common/TYPES.js
php ./generate_json.php
mkdir -p ./tmp/var/www/html
cp -r ../scripts tmp/var/www/html/
mv tmp/var/www/html/scripts tmp/var/www/html/OwnCloud-ldap
rm -rf tmp/var/www/html/OwnCloud-ldap/.svn
mkdir -p ./tmp/usr/share/doc/owncloud-ldap/
cp ../ldapfiles/install_files/* ./tmp/usr/share/doc/owncloud-ldap/
rm -rf ./tmp/usr/share/doc/owncloud-ldap/.svn
cd ./tmp && tar -cvzf owncloud-ldap-schema.tar.gz *
mv $initplace/tmp/owncloud-ldap-schema.tar.gz $initplace
rm -rf $initplace/tmp
#rm -rf $initplace/owncloud-ldap-schema.tar.gz
cd $initplace
declare -a arr=("owncloud-ldap-schema.ce6.spec" "owncloud-ldap-schema.ce7.spec")
#declare -a arr=("owncloud-ldap-schema.ce6.spec")
#declare -a arr=("wbb.phoenix.ce6.spec")
for i in "${arr[@]}"
do
spec=`rpm --eval '%{_specdir}'/$i`
cat ${dir}$i | fgrep -v '%prep' | fgrep -v '%setup' > $spec
#swcutchglog "$spec"
name=`rpm -q --specfile $spec --qf '%{name}\n' | head -n1`
echo rpmbuild -bb $spec
td=`mktemp -d /var/tmp/${name}-XXXXXX`; trap "rm -rf $td" 0
set +e
rpmbuild --buildroot $td --define "_builddir $PWD" -bb $spec
rc=$?
rm -rf $td
rpmd=`rpm --eval '%{_rpmdir}'`
rpmf=`rpm -q --specfile $spec --qf '%{arch}/%{name}-%{version}-%{release}.%{arch}.rpm\n'`
mv $rpmd/$rpmf $initplace/../ldapfiles
done
trap "" 0
cd $initplace/../
rm -rf ldapfiles/owncloudsite.tar.gz
perl -pi -e 's/BUILDREMPLACE/'${BUILD_NUMBER}.'/g' APP-META.xml
pmake APSRELEASE=${BUILD_NUMBER} clean publish
exit $rc
