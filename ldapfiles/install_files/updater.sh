#!/bin/bash

args=("$@")
for (( i = 0 ; i < ${#args[@]} ; i++ )) do
	echo "Downloading version ${args[$i]} from wget http://download.owncloud.org/community/owncloud-${args[$i]}.tar.bz2"
	wget "http://download.owncloud.org/community/owncloud-${args[$i]}.tar.bz2" -O "/tmp/owncloud.temp.tar.bz2"
	echo "Download Complete!"
	mkdir -p /tmp/owncloud.temp
	echo "Uncompressing version ${args[$i]}"
	tar -xjf /tmp/owncloud.temp.tar.bz2 -C /tmp/owncloud.temp --strip-components=1
	echo "Purging old data"
	for i in `ls /var/www/html/owncloudsite/| grep -v "config\|data"` ; do rm -rf /var/www/html/owncloudsite/$i; done
	echo "Put new Distro"
	/bin/cp -r /tmp/owncloud.temp/* /var/www/html/owncloudsite/
	/bin/cp -r /tmp/owncloud.temp/.htaccess /var/www/html/owncloudsite/
	chown -R apache:apache /var/www/html/owncloudsite
	rm -rf /tmp/owncloud.temp
	rm -f /tmp/owncloud.temp.tar.bz2
	chmod +x /var/www/html/owncloudsite/occ
	cd /var/www/html/owncloudsite/ && ./occ upgrade
	echo "Applaying workarround for native CSRF Check"
	perl -pi -e 's/public function passesCSRFCheck\(\) \{/public function passesCSRFCheck\(\) \{ return true;/g' /var/www/html/owncloudsite/lib/private/appframework/http/request.php
	echo "Applaying workarround for force update quotas update on each login"
	perl -pi -e 's/lastChecked\)\) < 86400/lastChecked\)\) < 60/g' /var/www/html/owncloudsite/apps/user_ldap/lib/user/user.php
	cd /var/www/html/owncloudsite/ && ./occ app:enable activity
	cd /var/www/html/owncloudsite/ && ./occ app:enable documents
done
