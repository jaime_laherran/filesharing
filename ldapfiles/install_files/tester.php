<?php
error_reporting(0);
#############################################################################################################################################
## This Script is Intented to be used to ensure that BVT don't fails due issues when init the LDAP from the RPM
#############################################################################################################################################

$try = 0;
if (isset($argv[1])){
	print "Let's try if given password ".$argv[1] ." is the good one!\n";
	$ldap_conn = ldap_binda($argv[1]);
	if($ldap_conn == -1){
		print "Given password is not valid!\n";
		if($argv[2]=="yes"){
			print "Testing if we have default password\n";
			$ldap_conn = ldap_binda();
			if($ldap_conn == -1){
				print "Seams you deployed LDAP by yourself, please remember to follow manual configuration steps\n";
				die;
			}
			$password="olcRootPW: {MD5}".base64_encode(pack("H*",md5($argv[1])));
			$old="olcRootPW:: cG9hbGFi";
			$file='/etc/openldap/slapd.d/cn=config/olcDatabase={2}bdb.ldif';
			$result=file_put_contents($file,str_replace($old,$password,file_get_contents($file)));
			if($result){	
				print "Password set to ".$argv[1]."\n";
				system("service slapd restart");
				sleep(30);
				$ldap_conn = ldap_binda($argv[1]);
				if($ldap_conn == -1){
					print "password change procedure failed";
					die;
				}
			}
			else{
				print "Failed to reset admin password\n";
				die;
			}
		}
		else{
			print "We hadn't been able to test the password and no brute force change was set\n";
			die;
		}
	}
	else{
		
		print "Given password is valid\n";
		die;
	}
}
if(isset($argv[1])){
	$password=$argv[1];
}
else{
	$password="poalab";
}
while ($try < 5){
	$add["objectClass"][0]="posixGroup";
        $add["objectClass"][1]="top";
        $add["cn"]=1;
	$add["userPassword"]=$password;
	$add["gidNumber"]=1;
	$add["description"]="Test Add";
	$adding=ldap_delete($ldap_conn,"cn=1,ou=Group,dc=owncloud,dc=com");
	$adding=ldap_add($ldap_conn,"cn=1,ou=Group,dc=owncloud,dc=com",$add);
	print_r($adding,true);
	if(!$adding){
		echo "LDAP Error ".ldap_errno($ldap_conn)."\n";
		system("ldapadd -x -w $password -D \"cn=admin,dc=owncloud,dc=com\" -f /usr/share/doc/owncloud-ldap/owncloud-base.ldif");
		$try=$try+1;	
	}
	else{
		echo "LDAP Is init OK! Removing what was added";
		$ldap_conn = $this->ldap_binda($argv[1]);
                $adding=ldap_delete($ldap_conn,"cn=1,ou=Group,dc=owncloud,dc=com");
		
		break;
	}
}

if ($try==5){
	echo "LDAP is broken, issue while initializing it due some hardware issue, most probable load average";
}
function ldap_binda($password="poalab")
        {
                $ldap_addr = "ldaps://127.0.0.1";
                $ldap_conn = ldap_connect($ldap_addr) or die("Couldn't connect!");
                ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
                $ldap_rdn = "admin";
                $ldap_pass = $password;
                $flag_ldap = ldap_bind($ldap_conn,"cn=$ldap_rdn,dc=owncloud,dc=com",$ldap_pass);
		if ($flag_ldap){
			echo "LDAP bind successful...";
			return $ldap_conn;
		}
		else{
			echo "LDAP bind failed...";
			return -1;
		}
		return $flag_ldap;
        }

?>
