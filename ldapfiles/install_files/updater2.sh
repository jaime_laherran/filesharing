#!/bin/bash

args=("$@")
for (( i = 0 ; i < ${#args[@]} ; i++ )) do
	echo "Downloading version ${args[$i]} from wget http://download.owncloud.org/community/owncloud-${args[$i]}.tar.bz2"
	if [ ! -d "/var/www/html/tmp" ]
		then
		mkdir /var/www/html/tmp
	fi
	wget "http://download.owncloud.org/community/owncloud-${args[$i]}.tar.bz2" -O "/var/www/html/tmp/owncloud.temp.tar.bz2"
	echo "Download Complete!"
	mkdir -p /var/www/html/tmp/owncloud.temp
	echo "Uncompressing version ${args[$i]}"
	tar -xjf /var/www/html/tmp/owncloud.temp.tar.bz2 -C /var/www/html/tmp/owncloud.temp --strip-components=1
	echo "Purging old data"
	for i in `ls /var/www/html/owncloudsite/| grep -v "config\|data"` ; do rm -rf /var/www/html/owncloudsite/$i; done
	echo "Put new Distro"
	/bin/cp -r /var/www/html/tmp/owncloud.temp/* /var/www/html/owncloudsite/
	/bin/cp -r /var/www/html/tmp/owncloud.temp/.htaccess /var/www/html/owncloudsite/
	chown -R apache:apache /var/www/html/owncloudsite
	rm -rf /var/www/html/tmp
	#rm -rf /tmp/owncloud.temp
	#rm -f /tmp/owncloud.temp.tar.bz2
	chmod +x /var/www/html/owncloudsite/occ
	if [ -f "/opt/rh/php54/enable" ] 
		then
		interpreter="/opt/rh/php54/root"
		source /opt/rh/php54/enable
	else
		interpreter=""
	fi
	echo "Upgrading minimalistic database to version ${args[$i]}"
	echo "interpreter of php set to $interpreter"
	#su apache -m -c '$interpreter/usr/bin/php /var/www/html/owncloudsite/occ upgrade'
	sudo -i -u apache $interpreter/usr/bin/php /var/www/html/owncloudsite/occ upgrade
	echo "Applaying workarround for native CSRF Check"
	perl -pi -e 's/public function passesCSRFCheck\(\) \{/public function passesCSRFCheck\(\) \{ return true;/g' /var/www/html/owncloudsite/lib/private/appframework/http/request.php
	perl -pi -e 's/public function isTrustedDomain\(\$domainWithPort\) \{/public function isTrustedDomain\(\$domainWithPort\) \{ return true;/g' /var/www/html/owncloudsite/lib/private/security/trusteddomainhelper.php
	echo "Applaying workarround for force update quotas update on each login"
	perl -pi -e 's/lastChecked\)\) < 86400/lastChecked\)\) < 60/g' /var/www/html/owncloudsite/apps/user_ldap/lib/user/user.php
	#su apache -c '$interpreter/usr/bin/php /var/www/html/owncloudsite/occ app:enable activity'
	sudo -i -u apache $interpreter/usr/bin/php /var/www/html/owncloudsite/occ app:enable activity
	#su apache -c '$interpreter/usr/bin/php /var/www/html/owncloudsite/occ app:enable documents'
	sudo -i -u apache $interpreter/usr/bin/php /var/www/html/owncloudsite/occ app:enable documents
done
