#!/bin/bash
if [ ! -d "/var/www/html/owncloudsite/apps/ocDashboard" ]; then
	echo "Downloading OC Dashboard"
	wget https://apps.owncloud.com/CONTENT/content-files/160265-ocDashboard_v1.3.1.zip -O /tmp/oc_dash.zip
	echo "Uncompressing OC DashBoard"
	/usr/bin/unzip /tmp/oc_dash.zip "ocDashboard/*" -d /var/www/html/owncloudsite/apps/
	cd /var/www/html/owncloudsite/ && ./occ app:enable ocDashboard
fi
if [ ! -d "/var/www/html/owncloudsite/apps/ocusagecharts" ]; then
        echo "Downloading OC Charts"
	wget https://apps.owncloud.com/CONTENT/content-files/166746-ocusagecharts.zip -O /tmp/ocusagecharts.zip
	echo "Uncompressing OC Charts"
	/usr/bin/unzip /tmp/ocusagecharts.zip "ocusagecharts/*" -d /var/www/html/owncloudsite/apps
	cd /var/www/html/owncloudsite/ && ./occ app:enable ocusagecharts
fi

