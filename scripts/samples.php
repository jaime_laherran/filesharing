<?php

#############################################################################################################################################
##   This class is in charge to handle the samples that provider creates in order to assign to users (service users)
##   As comparation with APS 1.x this type implements a subservice depending from the context, the context is the globals type defined
##   in globals.php
##   We requiere the APS PHP runtime
#############################################################################################################################################

require_once "aps/2/runtime.php";

#############################################################################################################################################
## Since this class implements a type that is not the top level (as comparation as APS 1.x, this is a subservice
## we specify that we implement the resource type from the types catalog, an application may have only 1 type that implements application
## but may have N types that implements resource type
#############################################################################################################################################

/**
* Class samples
* @type("SAMPLES_TYPE")
* @implements("http://aps-standard.org/types/core/resource/1.0")
*/

class samples extends \APS\ResourceBase
{
     #############################################################################################################################################
     ##   In order to make this type exist, we have an strong connection with the application type
     ##   We may remember that the application type, that in our example is called owncloud-globals and is implemented in globals.php contains
     ##   the settings that allows us to operate the service, as comparation with APS 1.x, the global settings
     ##   here we see that we have such strong requirement, the apsc controller will provide us always the related application instance in this
     ##   case, and due this we will be able to address the global settings that will be operated in this class inside the owncloudglobals
     ##   in order to read any of them we just need to address them as $this->ATTRIBUTENAME as defined in the globals
     ##   As APS 2.0 we may say that this is a way to read the attributes of a concrete type
     #############################################################################################################################################

     /**
     * @link("GLOBALS_TYPE")
     * @required
     */
     public $owncloudglobals;

     #############################################################################################################################################
     ## This type will be used by users (in POA Service Users) who will be attached to a concrete sample in order to be configured
     ## in this application, the idea is that a user receives the quota limits based on a sample (or user profile)
     ## a concrete sample may be attached to multiple users at same time, due this we define that has a link to such type and that this is a collection
     #############################################################################################################################################

     /**
     * @link("USERS_TYPE[]")
     */

     public $users;

     #############################################################################################################################################
     ##   We define the attributes of this type, as comparation with APS 1.x this are settings of this service
     ##   As we can see by comparation with the globals, this is a usecase, in globals we mentioned that are the "global settings" because
     ##   was implementing the application core type, since this implements a resource type and has a dependency we may see it as the settings
     ##   of the concrete subservice.
     #############################################################################################################################################

     /**
     * @type(number)
     * @title("Storage Quota in GB")
     * @required
     */

     public $quota;

     /**
     * @type(string)
     * @title("Sample Name")
     */
     public $name;

     private $checkDebug;

     #############################################################################################################################################
     ## This is a support function to return some default values, when custom UI is used is better to have a json file on custom ui that contains
     ## such data unless we want to put some programatic logic behind this function
     #############################################################################################################################################

     public function _getDefault(){
          \APS\LoggerRegistry::get()->debug("GetDefaults function has been called, we return a hardcoded value for Quota of 10GB");
          return array('quota'=>'10','name'=>'New Sample');
     }

     #############################################################################################################################################
     ##   Below we implement the different functions for the crud operations, as we may see the retrive function is not implemented
     ##   in this case is not done due the aps controller knows already the settings and this class is only used to create a type
     ##   that can be read to see the settings
     #############################################################################################################################################

     public function provision()
    {
          $this->checkDebug();
          \APS\LoggerRegistry::get()->debug("Provision function has been called with values\n\tSample Name:".$this->name."\n\tQuota:".$this->quota);
    }

    public function configure($new)
    {
          $this->checkDebug();
          \APS\LoggerRegistry::get()->info("Configure function has been called with values\n\tSample Name:".$this->name."\n\tQuota:".$this->quota);
          $this->_copy($new);
          $apsc = clone \APS\Request::getController();
          $users = $this->users;
          foreach($users as $user){
               $myuser = $apsc->getResource($user->aps->id);
               \APS\LoggerRegistry::get()->debug("Updating User: ".$myuser->owncloudusername);
               $myuser->quota = $this->quota;
               $myuser->assignedprofilename = $this->name;
               $apsc->updateResource($myuser);
          }
    }

     public function unprovision()
     {
          $this->checkDebug();
          \APS\LoggerRegistry::get()->debug("Unprovision function has been called with values\n\tSample Name:".$this->name."\n\tQuota:".$this->quota);
     }

     function checkDebug()
     {
        $inifile = parse_ini_file('./config/config.ini',true);
        if($inifile['GLOBAL']['debug'] == 'true' || $inifile['GLOBAL']['debug'] == '1'){
            define("APS_DEVELOPMENT_MODE", true);
        }
        return;
     }


}
?>
