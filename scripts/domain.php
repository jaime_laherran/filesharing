<?php

require_once "aps/2/runtime.php";

/**
* Class domain
* @type("DOMAIN_TYPE")
* @implements("http://aps-standard.org/types/core/resource/1.0")
* @implements("http://www.parallels.com/pa/pa-core-services/domain-management/DomainService/1.0")
*/

class domain extends \APS\ResourceBase
{
    /**
    * @link("TENANT_TYPE")
    * @required
    */
    public $tenant;

    /**
    * @type(string)
    * @title("Prefix where to listen for owncloud")
    * @required
    **/

    public $accessdomainprefix;
    private $checkDebug;

    public function provision()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Provision of Domain started");
        $this->name = $this->domain->name;
        $apsc = clone \APS\Request::getController();
        $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
        $apsc->resetSession();
        $apsc->setResourceId($this->tenant->aps->id);
        $tenant = $apsc->getResource($this->tenant->aps->id);
        $account = $apsc->getResource($tenant->account->aps->id);
        if(isset($notificator[0]->aps->id) && isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
            $json_notification = array(
                "message" => array(
                    "message" => 'File Sharing Linked to Domain'
                ),
                "details" => array(
                    "message" => 'File Sharing was linked to domain __domainName__.',
                    "keys" => array("domainName" => $this->name)
                ),
                "type" => "activity",
                "accountId" => $account->aps->id,
                "userId" => $_SERVER['HTTP_APS_IDENTITY_ID'],
                "initiatorId" => $_SERVER['HTTP_APS_IDENTITY_ID'],
                "service" => "File Sharing",
                "status" => "ready",
                "packageId" => $this->aps->package->id
            );
            try{
                \APS\LoggerRegistry::get()->debug("Sending notification for domain creation ".$this->name);
                json_decode($apsc->getIo()->sendRequest(\APS\Proto::POST, "/aps/2/resources/".$notificator[0]->aps->id."/notifications", json_encode($json_notification)));
            } catch (Exception $e) {
                \APS\LoggerRegistry::get()->debug("Error when sending notification for creation of domain ".$this->name);
            }
        }
    }
    public function unprovision()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Unprovision of Domain started");
        $apsc = clone \APS\Request::getController();
        $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
        $apsc->resetSession();
        $apsc->setResourceId($this->tenant->aps->id);
        $tenant = $apsc->getResource($this->tenant->aps->id);
        $account = $apsc->getResource($tenant->account->aps->id);
        if(!isset($tenant->diskusage)){
            $tenant->diskusage = new stdClass();  
            $tenant->diskusage->usage = 0;
        }
        $inifile=parse_ini_file('./config/config.ini',true);
        $destcename = $inifile['GLOBAL']['owncloudhttp'];
        $destcename = str_replace("https://", "", $destcename);
        $destcename = str_replace("http://", "", $destcename);
        $hostname = substr($destcename,0, strpos($destcename, '/'));
        $tenant->accessdomain = substr($destcename,0, strpos($destcename, '/'));
        if(isset($notificator[0]->aps->id) && isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
          //"details": { "message": "'.$this->name.' for File Sharing service was successfully disabled"},
            $json_notification = array( 
                "message" => array(
                    "message" => 'File Sharing Unlinked from Domain'
                ),
                "details" => array(
                    "message" => 'File Sharing was unlinked from domain __domainName__.',
                    "keys" => array("domainName" => $this->name)
                ),
                "type" => "activity",
                "accountId" => $account->aps->id,
                "userId" => $_SERVER['HTTP_APS_IDENTITY_ID'],
                "status" => "ready",
                "packageId" => $this->aps->package->id,
                "initiatorId" => $_SERVER['HTTP_APS_IDENTITY_ID'],
                "service" => "File Sharing",
            );
            try{
                \APS\LoggerRegistry::get()->debug("Sending Screen notification to ".$this->name);
                json_decode($apsc->getIo()->sendRequest(\APS\Proto::POST, "/aps/2/resources/".$notificator[0]->aps->id."/notifications", json_encode($json_notification)));
            } catch (Exception $e) {
                \APS\LoggerRegistry::get()->debug("Error when sending notification for creation of domain ".$this->name);
            }
        }
        $apsc->updateResource($tenant);
        try{
            \APS\LoggerRegistry::get()->debug("Trying to find records on DNS zone that matched legacy data");
            $oldrecord = $apsc->getResources('implementing(http://aps-standard.org/types/dns/record/cname/1.0),source=eq='.$this->accessdomainprefix.'.'.$this->name.'.');
            if(count($oldrecord) > 0){
                $operation_result = json_decode($apsc->getIo()->sendRequest(\APS\Proto::DELETE, "/aps/2/resources/".$oldrecord[0]->aps->id));
                \APS\LoggerRegistry::get()->debug("Result of Delete operation of DNS record".print_r($operation_result,true));
            }
        } catch (Exception $e){
            \APS\LoggerRegistry::get()->debug("Error while deleting old record: ".$e->getMessage());
        }
    }
    public function configure($new=null)
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Reconfiguration of Domain started");
        $apsc = clone \APS\Request::getController();
        $apsc->resetSession();
        $apsc->setResourceId($new->tenant->aps->id);
        $oldDomain = $apsc->getResource($this->aps->id);
        $tenant = $apsc->getResource($new->tenant->aps->id);
        if(!isset($tenant->diskusage)){
            $tenant->diskusage = new stdClass();
            $tenant->diskusage->usage = 0;
        }
        $tenant->accessdomain = $new->accessdomainprefix.".".$new->name;
        $apsc->updateResource($tenant);
        $this->_copy($new);
        $account = $apsc->getResource($tenant->account->aps->id);
        $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
        if(isset($notificator[0]->aps->id) && isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
            //"details": { "message": "'.$tenant->accessdomain.' for File Sharing service was successfully enabled"},
            $json_notification = array(
                "message"=> array(
                    "message" => 'File Sharing Domain Updated'
                ),
                "details" => array(
                    "message" => 'File Sharing domain __domainName__ was updated.',
                    "keys" => array("domainName" => $this->name)
                ),
                "type" => "activity",
                "accountId" => $account->aps->id,
                "userId" => $_SERVER['HTTP_APS_IDENTITY_ID'],
                "status" => "ready",
                "packageId" => $this->aps->package->id,
                "initiatorId" => $_SERVER['HTTP_APS_IDENTITY_ID'],
                "service" => "File Sharing",
            );
            try{
                \APS\LoggerRegistry::get()->debug("Sending screen notification to ".$this->name);
                json_decode($apsc->getIo()->sendRequest(\APS\Proto::POST, "/aps/2/resources/".$notificator[0]->aps->id."/notifications", json_encode($json_notification)));
            } catch (Exception $e) {
                \APS\LoggerRegistry::get()->debug("Error when sending notification for creation of domain ".$this->name);
            }
        }
        try{
            \APS\LoggerRegistry::get()->debug("Trying to find records on DNS zone that matched legacy data");
            $oldrecord = $apsc->getResources('implementing(http://aps-standard.org/types/dns/record/cname/1.0),source=eq='.$oldDomain->accessdomainprefix.'.'.$oldDomain->name.'.');
            if(count($oldrecord) > 0){
                $operation_result = json_decode($apsc->getIo()->sendRequest(\APS\Proto::DELETE, "/aps/2/resources/".$oldrecord[0]->aps->id));
                \APS\LoggerRegistry::get()->debug("Result of Delete operation of DNS record".print_r($operation_result,true));
            }
            else{
                \APS\LoggerRegistry::get()->debug("Domain ".$new->name." had no record pointing to OWC installation");
            }
            $zone = $apsc->getResource($this->domain->aps->id);
            $apsc2 = clone \APS\Request::getController();
            $apsc2->resetSession();
            $globals = $apsc2->getResource($tenant->owncloudglobals->aps->id);
            $destcename = $globals->owncloudhttp;
            $destcename = str_replace("https://", "", $destcename);
            $destcename = str_replace("http://", "", $destcename);
            $hostname = substr($destcename,0, strpos($destcename, '/'));
            $json_record = '{
                "aps": { "type": "http://parallels.com/aps/types/pa/dns/record/cname/1.0" },
                "source": "'.$this->accessdomainprefix.'",
                "RRState":"active",
                "recordId" : "0",
                "TTL": "300",
                "cname": "'.$hostname.'.",
                "zone": {
                    "aps": {
                        "id": "'.$zone->aps->id.'"
                    }
                }
            }';
            $createrecord = $apsc->getIo()->sendRequest(\APS\Proto::POST, "aps/2/resources/", $json_record);
        } catch (Exception $e) {
            \APS\LoggerRegistry::get()->debug("Error on domain operation: ");
        }
    }

    /**
    * @verb(GET)
    * @path("/getprovideraccesspoint")
    * @param
    */
    public function getprovideraccesspoint()
    {
        $this->checkDebug();
        $inifile=parse_ini_file('./config/config.ini',true);
        $destcename = $inifile['GLOBAL']['owncloudhttp'];
        $destcename = str_replace("https://", "", $destcename);
        $destcename = str_replace("http://", "", $destcename);
        $hostname = substr($destcename,0, strpos($destcename, '/'));
        return array("accessdomain" => $hostname);
    }

    function checkDebug()
    {
        $inifile = parse_ini_file('./config/config.ini',true);
        if($inifile['GLOBAL']['debug'] == 'true' || $inifile['GLOBAL']['debug'] == '1'){
            define("APS_DEVELOPMENT_MODE", true);
        }
        return;
    }
}
?>
