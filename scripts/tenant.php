<?php
#############################################################################################################################################
## This Class is in charge to generate the Tenant. A tenant is the entity that is created once per subscription.
## As comparation with APS 1.x it is the context of the subscription but in APS 2.0, since the context is owned by provider, this service
## is instantiated by a resource from Application Service resource class.
## In this service. we may acquire some information relative to the Account itself and probably also to Subscription
#############################################################################################################################################
require_once "aps/2/runtime.php";

#############################################################################################################################################
## Since this service is not top level service it must implement the core type resource, is important to remark that as a "syndicated" service
## the main difference from aps resource modeling is that in APS 2.0 we may have applications that with only one instance we may serve several
## customers and subscriptions
#############################################################################################################################################

/**
* Class tenant
* @type("TENANT_TYPE")
* @implements("http://aps-standard.org/types/core/subscription/service/1.0")
* @implements("http://aps-standard.org/types/core/resource/1.0")
* @implements("http://aps-standard.org/types/core/suspendable/1.0")
*/

class tenant extends \APS\ResourceBase
{
    #############################################################################################################################################
    ##   In order to generate a tenant, we requiere the information contained in the global settings, in same way as in APS 1.x
    ##   The global settings are owned by the instance that is deployed on provider level, and we can access them if the subscription
    ##   has a resource of the class "service reference" that points to it
    ##   since below we define an strong requierement to such APS type, and subscription will have such resource, the APS controller will
    ##   serve the globals (the attributes of the APS type owncloudglobal-ldap
    #############################################################################################################################################

    /**
    * @link("GLOBALS_TYPE")
    * @required
    */
    public $owncloudglobals;

    #############################################################################################################################################
    ## Below we define also that we have an strong requierement with Subscription, this is done in this service for 2 reasons:
    ##   a) We want to access the subscription id in order to have a unique identifier while creating the tenant on the applicaion, in APS 1.x
    ##      we can do that by defining a setting and specify that this setting is from class subscription_id, in APS 2.0 we may link to the
    ##      core type subscription, since we define it as requiered, the aps controller will serve us such information. In case we don't define
    ##      it as requiered, our application will need to query the controller in order to discover such information, at any case, if we may
    ##      read (or maybe write) information that comes from an APS type that is not the one created by this service, we may specify it's
    ##      link
    ##   b) Our application has a service that is attached to service users, this service will be limited by a set of resources, by defining
    ##      link to subscription we may access to the operation resources defined in subscription type, this allows us to read all the
    ##      limits and usage of the resources defined in the subscription
    #############################################################################################################################################

    /**
    * @link("http://aps-standard.org/types/core/subscription/1.0")
    * @required
    */

    public $subscription;

    #############################################################################################################################################
    ##   We define link to account type, we define this in order to access the attributes of it for reading information.
    ##   concretely with that we can access the account name, and all it's other data. In APS 1.x we can do that by using several classess
    ##   assigned to settings, in aps 2.0 with this link we can access to all such information
    #############################################################################################################################################

    /**
    * @link("http://aps-standard.org/types/core/account/1.0")
    * @required
    */
    public $account;

    #############################################################################################################################################
    ## We define the fact that users will "live" or depend from the tenant, is important to remark that is not an string requierement from
    ## tenant => users, but it will be from user => tenant, in other words: A tenant may not have users, for example just after creation, but
    ## a user requieres always a tenant
    #############################################################################################################################################

    /**
    * @link("DOMAIN_TYPE")
    **/
    public $domain;

    /**
    * @link("USERS_TYPE[]")
    */
    public $users;

    #############################################################################################################################################
    ## Below we define a subset of attributes, this attributes are marked as readonly in order to be this service tenant who populates them.
    ## As comparation with APS 1.x this attributes are settings "hidden" and "protected" and are populated after execution of one of the operations
    ## doing an structured-output
    #############################################################################################################################################

    /**
    * @type(string)
    * @title("Tenant ID")
    * @readonly
    */

    public $TENANTID;

    /**
    * @type(string)
    * @title("Group Password")
    * @readonly
    * @encrypted
    */

    public $GROUPPASS;

    /**
    * @type(string)
    * @title("Personal access domain")
    * @readonly
    **/

    public $accessdomain;

    #############################################################################################################################################
    ## We will define a counter, this counter will only implement usage for usage billing for the tenant, in other words, shall be capable to
    ## count all diskspace used by all users in case that provider decides to do usage billing
    #############################################################################################################################################

    /**
    * @type("http://aps-standard.org/types/core/resource/1.0#Usage")
    * @description("Diskspace - Usage Only")
    */
    public $diskusage;

    /**
    * @type(number)
    * @readonly
    * @description("We add a flag that will be set to 1 if PBA is available for the subscription, by default, for compatibility reasons, we set to 0")
    **/
    public $billing=0;

    #############################################################################################################################################
    ## Definition of the functions that will respond to the different CRUD operations
    #############################################################################################################################################

    public function provision()
    {
        $this->checkDebug();
        #############################################################################################################################################
        ## Just for example, we will put on log file all the information contained inside the different types that are requiered, with that we
        ## can see that controller "serves" us this information automatically
        #############################################################################################################################################

        ## Dump of account information
        \APS\LoggerRegistry::get()->debug("Provision for tenant has been called, it received as input account information:\n\t".print_r($this->account,true));
        ## Dump of subscription information
        \APS\LoggerRegistry::get()->info("Provision for tenant has been called, it received as input subscription information:\n\t".print_r($this->subscription,true));
        ## Dump of globals
        \APS\LoggerRegistry::get()->debug("Provision for tenant has been called, it received as input the globals information:\n\t".print_r($this->owncloudglobals,true));   

        #############################################################################################################################################
        ## Below we are doing the operations that are native from the application itself, for example connecting to a remote API, etc...In our case
        ## we create objects inside an ldap. In this simple case we presume that the main attribute in the ldap tree will be unique, since is related
        ## with the subscription id, allowing us to simplify all error handling
        #############################################################################################################################################
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $this->TENANTID = $this->subscription->subscriptionId;
            $this->GROUPPASS = \APS\generatePassword(12);
            $add["objectClass"][0]="posixGroup";
            $add["objectClass"][1]="top";
            $add["cn"]=$this->subscription->subscriptionId;
            $add["userPassword"]=$this->make_ssha_password($this->GROUPPASS);
            $add["gidNumber"]=$this->subscription->subscriptionId;
            $add["description"]="Subscription in poa ".$this->subscription->subscriptionId;
            $adding=@ldap_add($ldap_conn,"cn=".$this->subscription->subscriptionId.",ou=Group,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2,$add);
            if(ldap_error($ldap_conn) != "Success"){
                ### Seams somebody restarted task, since the identifier is the subscriptionId, this is unique per installation, and only use case is that failed due taskmanager was restarted, we will try to remove it and add back
                $adding=@ldap_delete($ldap_conn,"cn=".$this->subscription->subscriptionId.",ou=Group,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2);
                $adding=@ldap_add($ldap_conn,"cn=".$this->subscription->subscriptionId.",ou=Group,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2,$add);
            }
            if(ldap_error($ldap_conn) == "Success"){
                ### This is a new code, we will check if PBA exists or not and set a flag called billing if so, that will enable upsells from our UI
                ## First we get connection to the controller
                $apsc=\APS\Request::getController();
                $globals=$apsc->getResource($this->owncloudglobals->aps->id);
                if($globals->billing == true){
                    $this->billing = 1;
                }
                #### Since we maybe modified the resource from original input (maybe we touched the billing flag, we must store the changes into the controller before it since we want to do it
                #### as same transaction as before, we reset the impersonation to get to the previous transaction
                \APS\Request::getController()->setResourceId(null);
                $apsc->updateResource($this);
                $onDomainAvailable = new \APS\EventSubscription(\APS\EventSubscription::Available, "onDomainAvailable");
                $onDomainAvailable->source = (object)array('type' => "DOMAIN_TYPE");
                $apsc->subscribe($this,$onDomainAvailable);
                $destcename = $this->owncloudglobals->owncloudhttp;
                $destcename = str_replace("https://", "", $destcename);
                $destcename = str_replace("http://", "", $destcename);
                $hostname = substr($destcename,0, strpos($destcename, '/'));
                $destcename = $this->owncloudglobals->owncloudhttp;
                $destcename = str_replace("https://", "", $destcename);
                $destcename = str_replace("http://", "", $destcename);
                $this->accessdomain = substr($destcename,0, strpos($destcename, '/'));
            }
            else{
                throw new Exception("Error adding in ldap", true);
            }
        }
    }

    public function configure($new){
        $this->checkDebug();
        ## in this concrete example, configure does nothing
        \APS\LoggerRegistry::get()->debug("Configure for tenant has been called, it received as input account information:\n\t".print_r($this->account));
        \APS\LoggerRegistry::get()->debug("Configure for tenant has been called, it received as input subscription information:\n\t".print_r($this->subscription));
        \APS\LoggerRegistry::get()->debug("Configure for tenant has been called, it received as input the globals information:\n\t".print_r($this->owncloudglobals));
        $this->_copy($new);
    }

    public function unprovision(){
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Tenant has been called to unprovision tenant related with subscription:".$this->TENANTID);
        $ldap_conn = $this->ldap_binda();
        try{
            $adding=ldap_delete($ldap_conn,"cn=".$this->subscription->subscriptionId.",ou=Group,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2);
        }
        catch(Exception $e){
            \APS\LoggerRegistry::get()->debug("it was already deleted the tenant");
        }
    }

    #############################################################################################################################################
    ## A tenant may be suspended, we may define de operations related with core type suspendable
    ## in that case will be enable and disable
    #############################################################################################################################################

    /**
    * We define operation for enable
    * @verb(PUT)
    * @path("/enable")
    * @param()
    */
    function enable(){
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("We are requiered to enable the tenant:".$this->TENANTID);
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $fields=array("memberUid");
            $ldapSearch=ldap_search($ldap_conn,"ou=Group,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2,"cn=".$this->subscription->subscriptionId,$fields);
            $entries = ldap_get_entries($ldap_conn,$ldapSearch);
            if(isset($entries[0]["memberuid"])){
                for($i=0;$i < $entries[0]["memberuid"]["count"]; $i++){
                    $old="uid=disabled_".$entries[0]["memberuid"][$i].",ou=users,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2;
                    $newname=str_replace("disabled_","",$entries[0]["memberuid"][$i]);
                    $new="uid=".$newname;
                    $scope="ou=users,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2;
                    $adding=ldap_rename($ldap_conn,$old,$new,$scope,TRUE);
                    if($adding==false){
                        throw new Exception("Error enabling users in subscription ".$this->TENANTID);
                    }
                }
            }
        }
        else{
            throw new Exception("Error connecting to Ldap when enabling users");
        }
    }

    /**
    * We define operation for disable
    * @verb(PUT)
    * @path("/disable")
    * @param()
    */
    function suspend(){
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("We are requiered to disable the tenant:".$this->TENANTID);
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $fields=array("memberUid");
            $ldapSearch=ldap_search($ldap_conn,"ou=Group,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2,"cn=".$this->subscription->subscriptionId,$fields);
            $entries = ldap_get_entries($ldap_conn,$ldapSearch);
                if(isset($entries[0]["memberuid"])){
                    for($i=0;$i < $entries[0]["memberuid"]["count"]; $i++){
                        $old="uid=".$entries[0]["memberuid"][$i].",ou=users,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2;
                        $new="uid=disabled_".$entries[0]["memberuid"][$i];
                        $scope="ou=users,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2;
                        $adding=ldap_rename($ldap_conn,$old,$new,$scope,TRUE);
                        if($adding==false){
                            throw new Exception("Error disabling users");
                        }
                    }
                }
        }
        else{
            throw new Exception("Error connecting to Ldap when disabling users");
        }
    }

    #############################################################################################################################################
    ## Let's define the classes for our diskspace usage counter, currently not implemented and will return just 0
    #############################################################################################################################################

    public function __construct(){
        $this->diskusage=new \org\standard\aps\types\core\resource\Usage();
    }

    public function log($what)
    {
        echo $what.": ".", diskusage: ".$this->diskusage->usage."\n";
    }

    public function count(){
        ### TO BE IMPLEMENTED IF WE WANT THAT CONTROLLER CAN ASK US WHEN IT WANTS, we don't return nothing since we push the values for updatequotas
        ### but if we will like, we will check it in our application and return it to controller in a way like below
        #$this->diskusage->usage=0;
    }

    public function retrieve(){
        $this->checkDebug();
        $this->count();
    }

    #############################################################################################################################################
    ## Support functions for this class
    #############################################################################################################################################
    private $ldap_binda;
    private $make_ssha_password;
    private $checkDebug;

    function ldap_binda()
    {
        $ldap_addr = 'ldaps://'.$this->owncloudglobals->LDAPIP;
        $ldap_conn = ldap_connect($ldap_addr) or die("Couldn't connect!");
        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap_rdn = $this->owncloudglobals->LDAPUSER;
        $ldap_pass = $this->owncloudglobals->LDAPPASS;
        $flag_ldap = ldap_bind($ldap_conn,"cn=$ldap_rdn,dc=".$this->owncloudglobals->dc1.",dc=".$this->owncloudglobals->dc2,$ldap_pass);
        return $ldap_conn;
    }

    function make_ssha_password($password){
        mt_srand((double)microtime()*1000000);
        $salt = pack("CCCC", mt_rand(), mt_rand(), mt_rand(), mt_rand());
        $hash = "{SSHA}" . base64_encode(pack("H*", sha1($password . $salt)) . $salt);
        return $hash;
    }
    #############################################################################################################################################
    ## We define a custom operation in order to receive notifications that somebody did an in app purchase
    #############################################################################################################################################

    /**
    * @verb(POST)
    * @path("/emptyupsell")
     * @param(object,body)
    */

    public function emptyupsell($body=null){
        return;
    }

    /**
    * @verb(POST)
    * @path("/upselladdsite")
    * @param(object,body)
    */

    public function upselladdsite($body){
        try{
            $apsc = clone \APS\Request::getController();
            $apsc->setResourceId($this->aps->id);
            \APS\LoggerRegistry::get()->info("Data for upsell:".print_r($body,true));
            if(isset($body[0])){
                $array = json_decode(json_encode($body), true);
                $tosend = $array[0];
                $createsiteresponse = $apsc->getIo()->sendRequest(\APS\Proto::POST,  "/aps/2/resources/", json_encode($tosend));
                \APS\LoggerRegistry::get()->info("Result of SiteCreation ".print_r($createsiteresponse,true));
            }
        } catch (Exception $e){
                \APS\LoggerRegistry::get()->info("Error while creating site in upsell operation ".print_r($e,true));
        }
    }

    /**
    * @verb(POST)
    * @path("/upsell")
    * @param(object,body)
    */

    public function upsell($body){
        $this->checkDebug();
        \APS\LoggerRegistry::get()->debug("The subscription ".$this->subscription->subscriptionId." did an in app purchase");
        $apsc = clone \APS\Request::getController();
        $apsc->setResourceId($this->aps->id);
        foreach($body as $newuser){
            try{
                if($newuser->aps->type == "USERS_TYPE") {
                        \APS\LoggerRegistry::get()->debug("Creating User: ".print_r(json_encode((array)$newuser),true));
                        $apsc->getIo()->sendRequest(\APS\Proto::POST,  "/aps/2/resources/", json_encode((array)$newuser));
                }
            } catch (Exception $e){
               \APS\LoggerRegistry::get()->debug("Error while creating user");
            }
        }
        \APS\LoggerRegistry::get()->info("MARC".print_r($body,true));
    }

    /**
    * @verb(POST)
    * @path("/onDomainAvailable")
    * @param("http://aps-standard.org/types/core/resource/1.0#Notification",body)
    */
    public function onDomainAvailable($event)
    {
        $apsc = clone \APS\Request::getController();
        $apsc->resetSession();
        $apsc->setResourceId($this->aps->id);
        $domain = $apsc->getResource($event->source->id);
        $zone = $apsc->getResource($domain->domain->aps->id);
        $destcename = $this->owncloudglobals->owncloudhttp;
        $destcename = str_replace("https://", "", $destcename);
        $destcename = str_replace("http://", "", $destcename);
        $hostname = substr($destcename,0, strpos($destcename, '/'));
        $this->accessdomain = $domain->accessdomainprefix.".".$domain->name;
        $record = \APS\TypeLibrary::newResourceByTypeId("http://aps-standard.org/types/dns/record/cname/1.0");
        $record->cname = $hostname.".";
        $record->RRState = 'active';
        $record->TTL = 300;
        $record->zone = $zone;
        $record->source = "filesharing"; #.$domain->name.".";
        \APS\LoggerRegistry::get()->debug(print_r($record,true));
        ## Let's wait core team to fix that records creation works using zone_UUID_/records
        $json_record = '{ 
            "aps": { "type": "http://parallels.com/aps/types/pa/dns/record/cname/1.0" },
            "source": "'.$domain->accessdomainprefix.'",
            "RRState":"active",
            "recordId" : "0",
            "TTL": "300",
            "cname": "'.$hostname.'.",
            "zone": {
                "aps": {
                    "id": "'.$zone->aps->id.'"
                }
            }
        }';
        #$apsc->linkResource($zone, 'records', $record);
        try{
            $createrecord = $apsc->getIo()->sendRequest(\APS\Proto::POST, "aps/2/resources/", $json_record);
        } catch (Exception $e) {
            \APS\LoggerRegistry::get()->info("Seams still not implemented correct dns behaivour!");
        }
        $apsc->updateResource($this);
    }

    function checkDebug()
    {
        $inifile = parse_ini_file('./config/config.ini',true);
        if($inifile['GLOBAL']['debug'] == 'true' || $inifile['GLOBAL']['debug'] == '1'){
            define("APS_DEVELOPMENT_MODE", true);
        }
        return;
    }

}
?>
