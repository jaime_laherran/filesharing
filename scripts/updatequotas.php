<?php
     #############################################################################################################################################
     #### This script is intented to be run by cron, for example. Script will contact APSC to list all tenants, for each tenant, all users
     #### with this we will be able to return to controller for each user, it's current usage and for each tenant, the accumulated of all users
     #############################################################################################################################################

define('APS_PHP_RUNTIME_ROOT', '/usr/share/aps/php/');
require_once 'aps/2/aps.php';


foreach(\APS\ControllerProxy::listInstances() as $instanceId) {
     $apsc = \APS\Request::getController($instanceId);
     $resList = $apsc->getResources();
     ###we will store all tenants that we want to update in a array
     $tenants=array();
     foreach($resList as $resource){
          ### we start our itineration by choosing tenants
          if($resource->aps->type=="USERS_TYPE"){
               try{
               ## we must do a disk usage directly since owncloud calculates that on each user session
               $current_usage=system("du -bb --max-depth=0 $resource->homedir | awk '{print $1}'", $retval);
               if(is_numeric($current_usage)){
                    ## we query the concrete resource to see all relations
                    $concreteres=$apsc->getResource($resource->aps->id);
                    ## We only care about GB....due this we shall convert the usage to it:
                    $current_usage=round(($current_usage/(1024*1024*1024)),2);
                    ## In case that output is nothing, means that is even not needed to report back...in case that is "something...we shall return it to controller
                    if($current_usage > 0){
                         $concreteres->userusage=$current_usage;
                         $apsc->updateResource($concreteres);
                         $tenant=$concreteres->tenant->link->id;
                         if(!in_array($tenant, $tenants)){
                              ## Is first time that we find such tenant, let's put it on the array
                              $tenants[$tenant] = $current_usage;
                         }
                         else{
                              ## we must update the array since we have more users already counted:
                              $tenants[$tenant] = $tenants[$tenant] + $current_usage;
                         }
                    }
                    else{
                         #Just in case we will reset the counters...maybe last times wasn't updated properly
                         $resource->userusage=0;
                         $apsc->updateResource($resource);
                    }
               }
            } catch (Exception $e) {
               print_r($e,true);
            }
          }
     }
     ## Now is time to update ALL the tenants back to controller
     foreach($tenants as $tenantid=> $tenantvalue){
          $tenantobject=$apsc->getResource($tenantid);
          $tenantvalue=round($tenantvalue,0);
          $tenantobject->diskusage->usage=$tenantvalue;
          $apsc->updateResource($tenantobject);
          ## If you want to see that the tenant object is updated, just comment below line, this it asks for the tenant object just after update
          #print(print_r($apsc->getResource($tenantid),true));
     }
}
?>
