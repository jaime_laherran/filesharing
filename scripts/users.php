<?php

require_once "aps/2/runtime.php";


/**
* Class users
*
* @type("USERS_TYPE")
* @implements("http://aps-standard.org/types/core/resource/1.0")
* @implements("http://aps-standard.org/types/core/user/service/1.0")
*/


class users extends \APS\ResourceBase
{
    /**
    * @link("TENANT_TYPE")
    * @required
    */
    public $tenant;

    /**
    * @link("http://aps-standard.org/types/core/user/1.0")
    * @required
    */

    public $user;

    /**
    * @link("SAMPLES_TYPE")
    * @required
    */
    public $samples;

    /**
    * @type(string)
    * @title("User Name")
    * @description("OwnCloud Username")
    * @readonly
    */
    public $owncloudusername;

    /**
    * @type(string)
    * @title("Owncloud Password")
    * @description("user password")
    * @encrypted
    **/

    public $owncloudpassword="";

    /**
    * @type(string)
    * @title("Home Directory")
    * @Description("Users Home Directory")
    * @readonly
    */

    public $homedir;

    /**
    * @type(string)
    * @title("Status")
    * @Description("User Status")
    * @readonly
    */

    public $userstatus;

    /**
    * @type(number)
    * @title("Quota Assigned")
    * @readonly
    */
    public $quota;

    /**
    * @type(number)
    * @title("Current Usage")
    * @readonly
    */

    public $userusage;
    /**
    * @type(boolean)
    * @title("Is Admin")
    * @readonly
    */

    public $isadmin = false;
    /**
    * @type(string)
    * @readonly
    * @title("creation id")
    * @readonly
    */

    public $creationid;
    /**
    * @type(string)
    * @title("Service User ID")
    * @readonly
    **/
    public $serviceUserId;

    # We shall define the methods of the class that are private
    /**
    * @type(string)
    * @title("Assigned profile UUID")
    * @readonly
    **/
    public $assignedprofile;

    /**
    * @type(string)
    * @title("Assigned Profile Name")
    * @readonly
    **/
    public $assignedprofilename;

    private $ldap_binda;
    private $make_ssha_password;
    private $returnbytesquota;
    private $randomPassword;
    private $checkDebug;

    public function provision()
    {
        $this->checkDebug();
        /**
        * Loading of own configuration
        **/
        $inifile=parse_ini_file('./config/config.ini', true);
        /**
        * Fulfill resource representation first
        **/
        $this->owncloudusername=$this->user->login;
        $this->homedir=$inifile['GLOBAL']['storagelocation'].$this->user->login;
        $this->quota=$this->samples->quota;
        $this->userusage=0;
        if($this->user->isAccountAdmin == true){
            $this->isadmin = true;
        }
        else{
            $this->isadmin = false;
        }
        $this->serviceUserId = $this->user->aps->id;
        $this->assignedprofile = $this->samples->aps->id;
        $this->assignedprofilename = $this->samples->name;
        \APS\LoggerRegistry::get()->info("Provisioning of New User with login ".$this->owncloudusername. " on subscription ".$this->tenant->TENANTID." started");
        \APS\LoggerRegistry::get()->debug("Trying to get notification-manager from bus");
        $apsc = clone \APS\Request::getController();
        $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
        $apsc->impersonate(null);
        $tenant=$apsc->getResource($this->tenant->aps->id);
        $account = $apsc->getResource($tenant->account->aps->id);
        if(isset($notificator[0]->aps->id)){
            \APS\LoggerRegistry::get()->debug("Notification Manager found");
            $json_notification = array(
                "message" => array(
                    "message" => "Assigning File Sharing"
                ),
                "details" => array(
                    "message"=> 'Assigning "__profileName__" profile to __userName__',
                    "keys" => array("profileName" => $this->assignedprofilename, "userName" => $this->user->displayName)
                ),
                "type" => "activity",
                "userId" => $this->user->aps->id,
                "status" => "inProgress",
                "service"=> "File Sharing",
                "packageId" => $this->aps->package->id,
                "linkMore" => "/v/pa/ccp-users/viewUser/r/".$this->user->aps->id,
                "linkMoreText" => array(
                    "message"=> "View User",
                    "keys" => array(
                        "View User" => ""
                    )
                )
            );
            if(isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
                $json_notification['initiatorId'] = $_SERVER['HTTP_APS_IDENTITY_ID'];
            }
            try{
                \APS\LoggerRegistry::get()->debug("Creating Notification");
                $notification=json_decode(
                    $apsc->getIo()->sendRequest(
                        \APS\Proto::POST,
                        "/aps/2/resources/".$notificator[0]->aps->id."/notifications",
                        json_encode($json_notification)
                    )
                );
                \APS\LoggerRegistry::get()->debug("Notification Created with id: ".$notification->id);
                $this->creationid = $notification->id;
            } catch (Exception $e){
                \APS\LoggerRegistry::get()->debug("Creating notification failed, please check pau log for more information");
            }
        }
        \APS\LoggerRegistry::get()->debug("Creation of user accepted and returned to controller request of Async phase");
        throw new \Rest\Accepted($this, "Creating user ".$this->user->login." on OwnCloud", 1);
    }

    public function provisionAsync()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Async Phase for creating user ".$this->owncloudusername." started");
        $inifile=parse_ini_file('./config/config.ini', true);
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $add["uid"]=$this->user->login;
            $add["cn"]=$this->user->login." ".$this->user->login;
            $add["objectClass"][0]="account";
            $add["objectClass"][1]="posixAccount";
            $add["objectClass"][2]="top";
            if(empty($this->owncloudpassword)){
                \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." is being provisioned with random password, means must be activated later");
                $this->aps->status="aps:activating";
                $add["userPassword"]=$this->make_ssha_password($this->randomPassword());
            }
            else{
                \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." Has password requested over API, let's use it");
                $add["userPassword"]=$this->make_ssha_password($this->owncloudpassword);
                $this->aps->status = "aps:ready";
            }
            $add["homeDirectory"]=$inifile['GLOBAL']['storagelocation'].$this->user->login;
            $add["description"]=$this->user->displayName;
            if(!$this->user->email || strlen($this->user->email) < 3){
                $add["emailAddress"]=$this->user->login;
            }
            else{
                $add["emailAddress"]=$this->user->email;
            }
            $add["ownCloudQuota"] = $this->returnbytesquota($this->samples->quota);
            $adding=@ldap_add($ldap_conn, "uid=".$this->user->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add);
            if(ldap_error($ldap_conn) == "Success"){
                $add2["memberUid"]=$this->user->login;
                $adding2=@ldap_mod_add($ldap_conn, "cn=".$this->tenant->TENANTID.",ou=Group,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add2);
                if(ldap_error($ldap_conn) == "Success"){
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." has been provisioned inside owncloud service");
                    $sub = new \APS\EventSubscription(\APS\EventSubscription::Changed, "onUserChange");
                    $sub->source->id=$this->user->aps->id;
                    $apsc = clone \APS\Request::getController();
                    $subscriptionnotifications = $apsc->subscribe($this, $sub);
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." subscribed to notifications of it's linked service user");
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Checking Presence of Notification Manager");
                    $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
                    $apsc->resetSession();
                    $tenant=$apsc->getResource($this->tenant->aps->id);
                    $account = $apsc->getResource($tenant->account->aps->id);
                    if(isset($notificator[0]->aps->id)){
                        \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Found Notification Manager");
                        \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Deleting Old Notification generated on Sync Provision task with id: ".$this->creationid);
                        \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning,  Old notification deletion succeded");
                        $json_notification = array(
                            "message" => array(
                                "message" => "File Sharing Profile Assigned"
                            ),
                            "details"=> array(
                                "message"=> 'Assigned "__profileName__" profile to __userName__',
                                "keys" => array("profileName" => $this->assignedprofilename, "userName" => $this->user->displayName)
                            ),
                            "type" => "activity",
                            "service"=> "File Sharing",
                            "userId" => $this->user->aps->id,
                            "status" => "ready",
                            "packageId" => $this->aps->package->id,
                            "linkMore" => "/v/pa/ccp-users/viewUser/r/".$this->user->aps->id,
                            "linkMoreText" => array(
                                "message"=> "View User",
                                "keys" => array(
                                    "View User"=> ""
                                )
                            )
                        );
                        \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Sending notification that User has been Created");
                        try{
                            \APS\LoggerRegistry::get()->debug("Sending Screen notification");
                            $notification=json_decode(
                                $apsc->getIo()->sendRequest(
                                    \APS\Proto::PUT,
                                    "/aps/2/resources/".$notificator[0]->aps->id."/notifications/".$this->creationid,
                                    json_encode($json_notification)
                                )
                            );
                            \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Notification of async provision completed successfully Done, notification generated ".$notification->id);
                        } catch (Exception $e){
                            \APS\LoggerRegistry::get()->debug("Sending Notification generated error...pau down?");
                        }
                        $this->creationid = "";
                    }
                    if($inifile['GLOBAL']['usernotification'] != ""){
                        $json_notification_email = array(
                            "msgTypeId" => $inifile['GLOBAL']['usernotification'],
                            "userId" => $this->serviceUserId,
                            "params" => array(
                                "displayName" => $this->user->displayName,
                                "login" => $this->owncloudusername
                                )
                            );
                        try{
                            \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Sending Email notification that User has been Created");
                            $notification=json_decode(
                                $apsc->getIo()->sendRequest(
                                    \APS\Proto::POST,
                                    "/aps/2/resources/".$inifile['GLOBAL']['notificator']."/messenger/sendToUser",
                                    json_encode($json_notification_email)
                                )
                            );
                            \APS\LoggerRegistry::get()->info(print_r($notification, true));
                        } catch (Exception $e) {
                            \APS\LoggerRegistry::get()->debug("Notification sending failed in PAU");
                        }
                    }
                    \APS\LoggerRegistry::get()->info("User ".$this->owncloudusername." async provisioning, task completed successfully");
                    return;
                }
                else{
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, problem trying to add the user".$this->user->login." to it's group ".$this->tenant->TENANTID);
                }
            }
            else{
                \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Trying to Rollback user creation just in case of something");
                $adding=@ldap_delete($ldap_conn, "uid=".$this->user->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2']);
                if(ldap_error($ldap_conn) == "Success"){
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, User existed in LDAP");
                }
                else{
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, User did not existed in LDAP");
                }
                \APS\LoggerRegistry::get()->error("User ".$this->owncloudusername." async provisioning, Problem adding user retrying");
                throw new \Rest\Accepted($this, "Creating user ".$this->user->login." on OwnCloud");
            }
        }
        else{
            \APS\LoggerRegistry::get()->error("ProvisionAsync function had issue connecting to ldap while provisioning user ".$this->owncloudusername);
            throw new Exception("Error Connecting to LDAP");
        }

    }

    public function unprovision()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Unprovision of user ".$this->owncloudusername." started");

        ################################################################################################################################
        ## We must remember that we have async provision, and may happen that customer wants to delete a user that has not been       ##
        ## really provisioned yet, in that case, let's throw an exception to avoid an unwanted status                                 ##
        ################################################################################################################################

        if ($this->aps->status == "aps:provisioning"){
            \APS\LoggerRegistry::get()->debug("Unprovision of user ".$this->owncloudusername." can't be completed due is still in provisioning status");
            throw new Exception ("User has not been provisioned yet");
        }
        $inifile=parse_ini_file('./config/config.ini', true);
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $wasdeleted=0;
            $adding=@ldap_delete($ldap_conn, "uid=".$this->user->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2']);
            if(ldap_error($ldap_conn) != "Success"){
                \APS\LoggerRegistry::get()->debug("Seams user was already deleted");
                $wasdeleted=1;
            }
            $allok=0;
            ################################################################################################################################
            ## Due user maybe is suspended, and we have approach to rename suspended users, it may happen that delete failed but is just  ##
            ## due that, let's retry in case of failure for deleted                                                                       ##
            ################################################################################################################################
            if(ldap_error($ldap_conn) == "Success"){
                $allok=1;
            }
            else{
                $adding=@ldap_delete($ldap_conn, "uid=suspended_".$this->user->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2']);
                if(ldap_error($ldap_conn) != "Success"){
                    \APS\LoggerRegistry::get()->debug("Seams user was already deleted");
                    $allok=1;
                }
            }
            if($allok==1){
                $add2["memberUid"]=$this->user->login;
                $adding2=@ldap_mod_del($ldap_conn, "cn=".$this->tenant->TENANTID.",ou=Group,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add2);
                if(ldap_error($ldap_conn) != "Success"){
                    if($wasdeleted=0){
                        \APS\LoggerRegistry::get()->debug("For some reason the user was deleted and was not existing in group");
                    }
                    else{
                        \APS\LoggerRegistry::get()->debug("User was already deleted");
                    }
                }
                ##Move user data to trash
                \APS\LoggerRegistry::get()->debug("Moving data of user ".$this->owncloudusername." to trash space");
                try{
                    $trash=system("mv -f ".$this->homedir." /var/www/ownclouddata_deletedusers", $retval);
                } catch (Exception $e) {
                    \APS\LoggerRegistry::get()->debug("move data while deleting user ".$this->owncloudusername." thrown Exception, but is controlled". $e->getMessage());
                }
                ## Sending Deletion notification
                $apsc = clone \APS\Request::getController();
                $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
                $apsc->resetSession();
                $tenant=$apsc->getResource($this->tenant->aps->id);
                $account = $apsc->getResource($tenant->account->aps->id);
                if(isset($notificator[0]->aps->id)){
                    \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." unprovision, Found Notification Manager");
                    $json_notification = array(
                        "message" => array(
                            "message" => "File Sharing Removed"
                        ),
                        "details" => array(
                            "message" => "File Sharing service has been removed from __userName__",
                            "keys" => array("userName" => $this->user->displayName)
                        ),
                        "type" => "activity",
                        "service"=> "File Sharing",
                        "userId" => $this->user->aps->id,
                        "status" => "ready",
                        "packageId" => $this->aps->package->id
                    );
                    if(isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
                        $json_notification['initiatorId'] = $_SERVER['HTTP_APS_IDENTITY_ID'];
                    }
                    try{
                        \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." unprovision, Sending notification that User has been Created");
                        $notification=json_decode(
                            $apsc->getIo()->sendRequest(
                                \APS\Proto::POST,
                                "/aps/2/resources/".$notificator[0]->aps->id."/notifications",
                                json_encode($json_notification)
                            )
                        );
                        \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." unprovision, Notification successfully Done, notification generated ".$notification->id);
                    } catch (Exception $e){
                        \APS\LoggerRegistry::get()->debug("Error when sending notification");
                    }
                }
            }
            else{
                \APS\LoggerRegistry::get()->error("Error deleting user ".$this->user->login);
            }
        }
        else{
            \APS\LoggerRegistry::get()->debug("Unprovision had issue to connect to LDAP");
            throw new Exception("Error while deleting user, please contact your provider");
        }
    }

    public function configure($new=null)
    {
        $this->checkDebug();
        ###############################################################################################################################################
        ## Here we have a function that allows us to reconfigure the user (as it was in APS 1.x), see that we get the old values over the $new param ##
        ## old ones can still be retrived since we still didn't done nothing over the initial param                                                  ##
        ## WARNING: ONLY changes in properties will thrown this method, not changes in links that will go over link() or unlink() methods.           ##
        ## In this application all properties are controled by application, due this only changes in link are valid as changes                       ##
        ## (change profile X for Y), due this this method will not do nothing in this application.                                                   ##
        ## As important remark: In case that is required to get the old values, it's not possible to obtain them over $this->something, in case we   ##
        ## need old values something like this must be done:                                                                                         ##
        ## $apsc = clone \APS\Request::getController();                                                                                              ##
        ## $apsc->resetSession();                                                                                                                    ##
        ## $old_values = $apsc->getResource($this->aps->id);                                                                                         ##
        ## Now we have the new values on $new and old ones in $old_values                                                                            ##
        ###############################################################################################################################################

        \APS\LoggerRegistry::get()->debug("Has been called the configure API even that does nothing");
    }

    public function samplesLink()
    {
        $this->checkDebug();
        $inifile=parse_ini_file('./config/config.ini', true);
        \APS\LoggerRegistry::get()->info("Requested link change for user ".$this->owncloudusername);
        if($this->samples->aps->id == $this->assignedprofile){
            \APS\LoggerRegistry::get()->info("Requested link change for user ".$this->owncloudusername." has nothing to do, requested profile is same as assigned");
        }
        if($this->userusage > $this->samples->quota){
            \APS\LoggerRegistry::get()->error("Is not possible to change profile, user is consuming more than new profile allows");
            throw new Exception("Update of user ".$this->owncloudusername." not possible, requested profile change not possible due new one has less quota than current usage of user");
        }
        \APS\LoggerRegistry::get()->debug("Retriving Old sample just in case");
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $add["uid"]=$this->owncloudusername;
            $add["cn"]=$this->owncloudusername." ".$this->owncloudusername;
            $add["objectClass"][0]="account";
            $add["objectClass"][1]="posixAccount";
            $add["objectClass"][2]="top";
            if(empty($this->owncloudpassword)){
                \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." is being provisioned with random password, means must be activated later");
                $this->aps->status="aps:activating";
                $add["userPassword"]=$this->make_ssha_password($this->randomPassword());
            }
            else{
                \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." Has password, let's use it");
                $add["userPassword"]=$this->make_ssha_password($this->owncloudpassword);
            }
            $add["description"]=$this->user->displayName;
            if(!$this->user->email || strlen($this->user->email) < 3 ){
                $add["emailAddress"]=$this->user->login;
            }
            else{
                $add["emailAddress"]=$this->user->email;
            }
            $add["ownCloudQuota"] = $this->returnbytesquota($this->samples->quota);
            if($this->aps->status =="aps:ready" || $this->aps->status=="aps:activating"){
                $modificated_ldap = @ldap_modify(
                    $ldap_conn,
                    "uid=".$this->owncloudusername.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'],
                    $add
                );
            }
            else{
                $modificated_ldap = @ldap_modify(
                    $ldap_conn,
                    "uid=suspended_".$this->owncloudusername.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'],
                    $add
                );
            }
            if(ldap_error($ldap_conn) != "Success"){
                \APS\LoggerRegistry::get()->error("Error while modifiying user ".$this->owncloudusername);
                throw new Exception("Error while updating user ".$this->owncloudusername.ldap_error($ldap_conn));
            }
        }
        else{
            \APS\LoggerRegistry::get()->error("Error connecting to ldap server");
            throw new Exception("Error while updating user ".$this->owncloudusername);
        }
        $apsc = clone \APS\Request::getController();
        $this->quota = $this->samples->quota;
        $this->assignedprofile = $this->samples->aps->id;
        $this->assignedprofilename = $this->samples->name;
        $apsc->updateResource($this);
        $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
        $apsc->resetSession();
        $apsc->setResourceId($this->tenant->aps->id);
        $tenant=$apsc->getResource($this->tenant->aps->id);
        $account = $apsc->getResource($tenant->account->aps->id);
        if(isset($notificator[0]->aps->id)){
            \APS\LoggerRegistry::get()->debug("Notification Manager found");
            $json_notification = array(
                "message" => array(
                    "message" => "File Sharing Profile Assigned"
                ),
                "details" => array(
                    "message"=> 'Assigned "__profileName__" profile to __userName__',
                    "keys" => array("profileName" => $this->assignedprofilename, "userName" => $this->user->displayName)
                ),
                "type" => "activity",
                "userId" => $this->user->aps->id,
                "status" => "ready",
                "service"=> "File Sharing",
                "packageId" => $this->aps->package->id,
                "linkMore" => "/v/pa/ccp-users/viewUser/r/".$this->user->aps->id,
                "linkMoreText" => array(
                    "message" => "View User",
                    "keys" => array(
                        "View User" => ""
                        )
                    )
                );
            if(isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
                $json_notification['initiatorId'] = $_SERVER['HTTP_APS_IDENTITY_ID'];
            }
            try{
                \APS\LoggerRegistry::get()->debug("Creating Notification");
                $notification=json_decode(
                    $apsc->getIo()->sendRequest(
                        \APS\Proto::POST,
                        "/aps/2/resources/".$notificator[0]->aps->id."/notifications",
                        json_encode($json_notification)
                    )
                );
                APS\LoggerRegistry::get()->debug("Notification Created with id: ".$notification->id);
            } catch (Exception $e){
                \APS\LoggerRegistry::get()->debug("Failed to send notification");
            }
        }
        if($inifile['GLOBAL']['usernotification'] != ""){
            $json_notification_email = array(
                "msgTypeId" => $inifile['GLOBAL']['usernotification'],
                "userId" => $this->serviceUserId,
                "params" => array(
                    "displayName" => $this->user->displayName,
                    "login" => $this->owncloudusername,
                    "newspace" => $this->samples->quota." GB"
                    )
                );
            try{
                \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." async provisioning, Sending Email notification that User has been Created");
                $notification=json_decode(
                    $apsc->getIo()->sendRequest(
                        \APS\Proto::POST,
                        "/aps/2/resources/".$inifile['GLOBAL']['notificator']."/messenger/sendToUser",
                        json_encode($json_notification_email)
                    )
                );
                \APS\LoggerRegistry::get()->info(print_r($notification, true));
            } catch (Exception $e) {
                \APS\LoggerRegistry::get()->debug("Notification sending failed in PAU");
            }
        }
    }

    /**
    * @verb(PUT)
    * @path("/changepassword")
    * @param(object,body)
    * @access(admin, true)
    * @access(owner, true)
    * @access(referrer, true)
    */

    function changepassword($body)
    {
        $this->checkDebug();
        $inifile=parse_ini_file('./config/config.ini', true);
        \APS\LoggerRegistry::get()->info("Request of change for user ".$this->owncloudusername);
        $apsc = clone \APS\Request::getController();
        $apsc->setResourceId($this->tenant->aps->id);
        $tenant=$apsc->getResource($this->tenant->aps->id);
        $account = $apsc->getResource($tenant->account->aps->id);
        $this->owncloudpassword = $body->owncloudpassword;
        $activation = false;
        if($this->aps->status=="aps:activating"){
            \APS\LoggerRegistry::get()->info("Request of change for user originates activation");
            $this->aps->status="aps:ready";
            $activation = true;
        }
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $add["uid"]=$this->owncloudusername;
            $add["cn"]=$this->owncloudusername." ".$this->owncloudusername;
            $add["objectClass"][0]="account";
            $add["objectClass"][1]="posixAccount";
            $add["objectClass"][2]="top";
            $add["userPassword"]=$this->make_ssha_password($this->owncloudpassword);
            $add["description"]=$this->user->displayName;
            if(!$this->user->email || strlen($this->user->email) < 3 ){
                $add["emailAddress"]=$userinfo->login;
            }
            else{
                $add["emailAddress"]=$this->user->email;
            }
            $add["ownCloudQuota"] = $this->returnbytesquota($this->samples->quota);
            if($this->aps->status=="aps:ready"){
                $modificated_ldap = @ldap_modify($ldap_conn, "uid=".$this->owncloudusername.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add);
            }
            else{
                $modificated_ldap = @ldap_modify(
                    $ldap_conn,
                    "uid=suspended_".$this->owncloudusername.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'],
                    $add
                );
            }
            if(ldap_error($ldap_conn) !== "Success"){
                \APS\LoggerRegistry::get()->debug("Update procedure on LDAP server returned:".ldap_error($ldap_conn));
                \APS\LoggerRegistry::get()->error("Error while Changing password for ".$this->owncloudusername);
                throw new Exception("Error while updating password ".$this->owncloudusername);
            }
            else{
                \APS\LoggerRegistry::get()->info("Request of change for user ".$this->owncloudusername." completed successfully");
                $apsc->updateResource($this);
                $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/notification-manager/1.0)');
                if(isset($notificator[0]->aps->id)){
                    \APS\LoggerRegistry::get()->debug("Notification Manager found");
                    if($activation == false){
                        $json_notification = array(
                            "message" => array(
                                "message" => "File Sharing Password Changed"
                            ),
                            "details" => array(
                                "message" => "File Sharing user password was changed by __userName__",
                                "keys" => array("userName" => $this->user->displayName) // by app logic only himself can activate
                            ),
                            "type" => "activity",
                            "accountId" => $account->aps->id,
                            "status" => "ready",
                            "service"=> "File Sharing",
                            "packageId" => $this->aps->package->id,
                            "linkMore" => "/v/pa/ccp-users/viewUser/r/".$this->user->aps->id,
                            "linkMoreText" => array(
                                "message" => "View User",
                                "keys" => array(
                                    "View User" => ""
                                    )
                                )
                            );
                    }
                    else{
                        $json_notification = array(
                            "message" => array(
                                "message" => "File Sharing Activated"
                            ),
                            "details" => array(
                                "message" => "File Sharing service has been activated by __userName__",
                                "keys" => array("userName" => $this->user->displayName)
                            ),
                            "type" => "activity",
                            "accountId" => $account->aps->id,
                            "status" => "ready",
                            "service"=> "File Sharing",
                            "packageId" => $this->aps->package->id,
                            "linkMore" => "/v/pa/ccp-users/viewUser/r/".$this->user->aps->id,
                            "linkMoreText" => array(
                                "message" => "View User",
                                "keys" => array(
                                    "View User" => ""
                                    )
                                )
                            );
                    }
                    if(isset($_SERVER['HTTP_APS_IDENTITY_ID'])){
                        $json_notification['userId'] = $_SERVER['HTTP_APS_IDENTITY_ID'];
                    }
                    try{
                        \APS\LoggerRegistry::get()->debug("Creating Notification");
                        $notification=json_decode(
                            $apsc->getIo()->sendRequest(
                                \APS\Proto::POST,
                                "/aps/2/resources/".$notificator[0]->aps->id."/notifications",
                                json_encode($json_notification)
                            )
                        );
                        APS\LoggerRegistry::get()->debug("Notification Created with id: ".$notification->id);
                    } catch (Exception $e){
                        \APS\LoggerRegistry::get()->debug("Failed to send notification");
                    }
                }
            }
        }
        else{
            \APS\LoggerRegistry::get()->debug("Error updating password for user ".$this->owncloudusername." due ldap connection error");
        }
        return $this;
    }

    /**
    * @verb(GET)
    * @path("/getssoparam")
    * @param
    */
    public function getssoparam()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Has been requested SSO params for user ".$this->owncloudusername);
        if($_SERVER['HTTP_APS_IDENTITY_ID'] != $this->serviceUserId){
            \APS\LoggerRegistry::get()->debug($_SERVER['HTTP_APS_IDENTITY_ID']." tried to get SSO params for user ".$this->serviceUserId);
            throw new Exception("You are not authorized to access File Sharing of this user");
        }
        if($this->aps->status != "aps:ready"){
            \APS\LoggerRegistry::get()->debug("We can't provide SSO credentials since user is not ready");
            if($this->aps->status == "aps:activating"){
                throw new Exception("Please activate your service first");
            }
            else{
                throw new Exception("Your user is not ready, please try again later or contact your service administrator");
            }
        }
        $inifile=parse_ini_file('./config/config.ini', true);
        $apsc = clone \APS\Request::getController();
        $apsc->resetSession();
        $tenant = $apsc->getResource($this->tenant->aps->id);
        $accessdomain = $tenant->accessdomain;
        return array(
            "accessdomain" => $this->tenant->accessdomain,
            "url" =>  $inifile['GLOBAL']['owncloudhttp'],
            "pass" => base64_encode($this->owncloudpassword)
        );
    }

    #############################################################################################################################################
    ## We will define 2 new operations out of the crud ones, in fact, such operations will not be usable out of the ui                         ##
    #############################################################################################################################################

    /**
    * We define operation for enable
    *
    * @verb(POST)
    * @path("/disableuser")
    * @param()
    */

    #############################################################################################################################################
    ## To disable a user we will do a simple operation, rename the login to avoid user to be able to login
    #############################################################################################################################################

    public function disableuser()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("Requesting to disable the user".$this->user->login);
        #############################################################################################################################################
        ## This version of Owncloud-ldap has the capability of Async Provisioning, this means that we must control the use case where provision    ##
        ## didn't happened and somebody presses the button on ui (for example)                                                                     ##
        #############################################################################################################################################
        if ($this->aps->status == "aps:provisioning"){
            return ("User has not been provisioned yet");
        }
        $inifile=parse_ini_file('./config/config.ini', true);
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $old="uid=".$this->user->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
            $new="uid=suspended_".$this->user->login;
            $scope="ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
            $adding=ldap_rename($ldap_conn, $old, $new, $scope, true);
            if(ldap_error($ldap_conn) != "Success"){
                ##########################################################################################################################################
                ## Here it can be seen a really interesting case, now is Application who wants to callback the controller in order to update something, ##
                ## concretely we are going to update the status, for that we must call directly the runtime. Is important to remember that now          ##
                ## application will authenticate to the controller using the certificate it received on installation time                               ##
                ## in this case, we even that didn't updated the ldap we return the correct status                                                      ##
                ##########################################################################################################################################
                $this->aps->status = "Suspended";
                $apsc = \APS\Request::getController();
                $apsc->updateResource($this);
                return("User is already suspended");
            }
            else{
                #############################################################################################################################################
                ## Here it can be seen a really interesting case, now is Application who wants to callback the controller in order to update something,     #
                ## concretely we are going to update the status, for that we must call directly the runtime. Is important to remember that now              #
                ## application will authenticate to the controller using the certificate it received on installation time                                   #
                #############################################################################################################################################

                $this->aps->status = "Suspended";
                $apsc = \APS\Request::getController();
                $apsc->updateResource($this);
                return("User ".$this->user->login." has been Suspended");
            }
        }
        else{
            \APS\LoggerRegistry::get()->error("Error connecting to LDAP when disabling user ".$this->user->login);
            throw new Exception("Error connecting to Ldap when disabling user");
        }
    }

    /**
    * We define operation for enable
    *
    * @verb(POST)
    * @path("/enableuser")
    * @param()
    */

    public function enableuser()
    {
        $this->checkDebug();
        if ($this->aps->status == "aps:provisioning"){
            return ("User has not been provisioned yet");
        }
        \APS\LoggerRegistry::get()->info("A Customer is requesting to enable the user".$this->user->login);
        $inifile=parse_ini_file('./config/config.ini', true);
        $ldap_conn = $this->ldap_binda();
        if($ldap_conn){
            $old="uid=suspended_".$this->user->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
            $new="uid=".$this->user->login;
            $scope="ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
            $adding=ldap_rename($ldap_conn, $old, $new, $scope, true);
            if(ldap_error($ldap_conn) != "Success"){
                $this->aps->status = "aps:ready";
                $apsc = \APS\Request::getController();
                $apsc->updateResource($this);
                return("User is Active");
            }
            else{
                $this->aps->status = "aps:ready";
                $apsc = \APS\Request::getController();
                $apsc->updateResource($this);
                return("User ".$this->user->login." has been Activated");
            }
        }
        else{
            throw new Exception("Error connecting to Ldap when disabling user");
        }
    }

    /**
    * We define operation for getting the clients, login url
    *
    * @verb(GET)
    * @path("/getclient")
    * @param()
    */

    public function getclient()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("A Customer is requesting clients and url to access service");
        $inifile=parse_ini_file('./config/config.ini', true);
        $accessdomain = $this->tenant->accessdomain;
        return array(
            "accessdomain" => $accessdomain,
            "url" =>  $inifile['GLOBAL']['owncloudhttp'],
            "winurl" => $inifile['GLOBAL']['winurl'],
            "macurl"=>$inifile['GLOBAL']['macurl'],
            "androidurl" => $inifile['GLOBAL']['androidurl'],
            "iosurl" => $inifile['GLOBAL']['iosurl'],
            "linuxurl"=>$inifile['GLOBAL']['linuxurl']
        );
    }

    #############################################################################################################################################
    ## Now we must define an operation in case that the service user changes, in other words, in case that is reconfigured like changing password
    ## Display name...or any of it's attributes. This happens becouse in provision() function we subscribed our application to it
    #############################################################################################################################################

    /**
    * We define the operation on post event
    *
    * @verb(POST)
    * @path("/onUsrChange")
    * @param("http://aps-standard.org/types/core/resource/1.0#Notification",body)
    */
    public function onUserChange($event)
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->info("onUserChange event has been triggered, even variable has following data: ".print_r($event, true));
        #############################################################################################################################################
        ### First thing we shall do is retrive the object that has changed
        #############################################################################################################################################
        $apsc = \APS\Request::getController();
        $serviceuser = $apsc->getResource($event->source->id);
        ### Here we aren't using $this->user becouse will only contain the link data, not the concrete info form attributes, that's why we do a request
        \APS\LoggerRegistry::get()->debug("onUserChange now knows that what's actual for user is: ".print_r($serviceuser, true));
        \APS\LoggerRegistry::get()->debug("we must update the login of this object:".$this->aps->id);
        #############################################################################################################################################
        ### One of the things that may have happened is that the user has been renamed, currently we shall be carefull with such a use case since
        ### our application must have enought data to know what was old....
        ### in our case...we can do everything since we have a duplicate of the username inside $this->owncloudusername
        #############################################################################################################################################
        $tenantinfo = $apsc->getResource($this->tenant->aps->id);
        \APS\LoggerRegistry::get()->debug("Information retrived by onUserChange relative to tenant of changed user".print_r($tenantinfo, true));
        $ldap_conn = $this->ldap_binda();
        $inifile=parse_ini_file('./config/config.ini', true);
        if(!$ldap_conn){
            throw new Exception("Error connecting to ldap");
        }
        if($this->owncloudusername != $serviceuser->login){
            $oldname="uid=".$this->owncloudusername.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
            $newname="uid=".$serviceuser->login;
            $scope="ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
            $adding=ldap_rename($ldap_conn, $oldname, $newname, $scope, true);
            if(ldap_error($ldap_conn) != "Success"){
                ### we wasn't able to rename, looks really that such user was disabled, let's try
                $oldname="uid=suspended_".$this->owncloudusername.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'];
                #### we shall leave it suspended...
                $newname="uid=suspended_".$serviceuser->login;
                $adding=ldap_rename($ldap_conn, $oldname, $newname, $scope, true);
                if(ldap_error($ldap_conn) != "Success"){
                    \APS\LoggerRegistry::get()->error("Seams we was't able to found the user ".$this->owncloudusername." while performing onUserChange due changes performed by Configure function");
                }
                else{
                    $add2["memberUid"]=$this->owncloudusername;
                    $adding2=ldap_mod_del($ldap_conn, "cn=".$tenantinfo->TENANTID.",ou=Group,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add2);
                    $add2["memberUid"]=$serviceuser->login;
                    ldap_mod_add($ldap_conn, "cn=".$tenantinfo->TENANTID.",ou=Group,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add2);
                    \APS\LoggerRegistry::get()->debug(print_r(ldap_error($ldap_conn), true));
                }
            }
            else{
                ### let's rename user in group
                $add2["memberUid"]=$this->owncloudusername;
                $adding2=ldap_mod_del($ldap_conn, "cn=".$tenantinfo->TENANTID.",ou=Group,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add2);
                $add2["memberUid"]=$serviceuser->login;
                ldap_mod_add($ldap_conn, "cn=".$tenantinfo->TENANTID.",ou=Group,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add2);
            }
        }
        $add["uid"]=$serviceuser->login;
        $add["cn"]=$serviceuser->login." ".$serviceuser->login;
        $add["objectClass"][0]="account";
        $add["objectClass"][1]="posixAccount";
        $add["objectClass"][2]="top";
        if(empty($this->owncloudpassword)){
            \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." is being provisioned with random password, means must be activated later");
            $this->aps->status="Activating";
            $add["userPassword"]=$this->make_ssha_password($this->randomPassword());
        }
        else{
            \APS\LoggerRegistry::get()->debug("User ".$this->owncloudusername." Has password requested over API, let's use it");
            $add["userPassword"]=$this->make_ssha_password($this->owncloudpassword);
        }
        $add["description"]=$serviceuser->displayName;
        if(!$serviceuser->email || strlen($serviceuser->email) < 3 ){
            $add["emailAddress"]=$serviceuser->login;
        }
        else{
            $add["emailAddress"]=$serviceuser->email;
        }
        $add["ownCloudQuota"] = $this->returnbytesquota($this->quota);
        if($this->aps->status =="aps:ready"){
            try{
                ldap_modify($ldap_conn, "uid=".$serviceuser->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add);
            }
            catch(Exception $e){
                \APS\LoggerRegistry::get()->debug("Seams we tried to add the same as it was do action enable/disable $e");
            }
        }
        else{
            try{
                ldap_modify($ldap_conn, "uid=suspended_".$serviceuser->login.",ou=users,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $add);
            }
            catch(Exception $e){
                \APS\LoggerRegistry::get()->debug("Seams we tried to add the same as it was do action enable/disable");
            }
        }
        ##Let's return to controller the new information
        \APS\LoggerRegistry::get()->info("we must update the login of this object:".$this->aps->id);
        $ownclouduser=$apsc->getResource($this->aps->id);
        $ownclouduser->owncloudusername=$serviceuser->login;
        $apsc->updateResource($ownclouduser);
        return;
    }

    #############################################################################################################################################
    ## Support functions for this class
    #############################################################################################################################################

    function ldap_binda()
    {
        putenv('LDAPTLS_REQCERT=never');
        $inifile=parse_ini_file('./config/config.ini', true);
        $ldap_addr = 'ldaps://'.$inifile['GLOBAL']['LDAPIP'];
        $ldap_conn = ldap_connect($ldap_addr) or die("Couldn't connect!");
        ldap_set_option($ldap_conn, LDAP_OPT_PROTOCOL_VERSION, 3);
        $ldap_rdn = $inifile['GLOBAL']['LDAPUSER'];
        $ldap_pass = $inifile['GLOBAL']['LDAPPASS'];
        $flag_ldap = ldap_bind($ldap_conn, "cn=$ldap_rdn,dc=".$inifile['GLOBAL']['dc1'].",dc=".$inifile['GLOBAL']['dc2'], $ldap_pass);
        return $ldap_conn;
    }

    function make_ssha_password($password)
    {
        mt_srand((double)microtime()*1000000);
        $salt = pack("CCCC", mt_rand(), mt_rand(), mt_rand(), mt_rand());
        $hash = "{SSHA}" . base64_encode(pack("H*", sha1($password . $salt)) . $salt);
        return $hash;
    }

    function returnbytesquota($gigas)
    {
        $output=$gigas*1024*1024*1024;
        return $output;
    }

    function randomPassword()
    {
        $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
        $pass = array();
        $alphaLength = strlen($alphabet) - 1;
        for ($i = 0; $i < 8; $i++) {
            $n = rand(0, $alphaLength);
            $pass[] = $alphabet[$n];
        }
        return implode($pass);
    }

    function checkDebug()
    {
        $inifile = parse_ini_file('./config/config.ini', true);
        if($inifile['GLOBAL']['debug'] == 'true' || $inifile['GLOBAL']['debug'] == '1'){
            define("APS_DEVELOPMENT_MODE", true);
        }
        return;
    }
}
?>
