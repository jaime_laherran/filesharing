<?php
#############################################################################################################################################
## This Class is in charge to define the top service
## as Comparation of APS 1.2 this one is in charge of:
##   * Define the global settings
##   * Define the Settings for the context Service: Is important to note that in general terms only 1 instance of this application will
##     exist per platform
##
## We may add all the requierements on top
## In this application we use 2
## Additionally we requiere the APS runtime, please see that the runtime must be installed where the endpoint executes
## is possible to add the runtime as part of the endpoint installation
## or deploy it globally on the server, in this case is expected that runtime runs on a Linux Machine and the rpm has been deployed on it
## and the php includes dir allows to reach it
## The spcial constant APS_DEVELOPMENT_MODE allows us to see all rquests on Webserver Logs
#############################################################################################################################################

require_once "aps/2/runtime.php";

#############################################################################################################################################
## This class must implement the instance, since we will use it to store the global settings we call it owncloud_globals
## As comparation with APS 1.x, this is the context, or top level service, due this, it must implement the application core type
#############################################################################################################################################

/**
* Class owncloud
* @type("GLOBALS_TYPE")
* @implements("http://aps-standard.org/types/core/application/1.0")
*/
class owncloud_globals extends \APS\ResourceBase
{

    #############################################################################################################################################
    ##   As Exposed before, we will have only 1 instance of this service in normal cases, provider customers will use this instance in order
    ##   to use the owncloud service, due this, we will have possibility to have a collection (multiple) customers attached to it, but this is 
    ##   not an strong requierement, an instance may exist without any customer subscription attached to it
    ##   We define below this, additional we refer to the service that connects by using the type definition of it
    #############################################################################################################################################

    /**
    * @link("TENANT_TYPE[]")
    **/
    public $tenant;

    #############################################################################################################################################
    ##   Service users from POA, or Users in general since we are implementing APS types, will be configured using a user profile
    ##   This User profile is defined by Provider and controlled by him, the application context that we are defining here may have several 
    ##   user profiles attached to it
    #############################################################################################################################################

    /**
    * @link("SAMPLES_TYPE[]")
    **/
    public $samples;

    #############################################################################################################################################
    ##   As comparation with APS 1.x, here we define the "Global Settings", anyway is important to remark that this is a usecase since:
    ##   * From APS 2.0 we are defining below attributes of this type
    ##   * We may say that are "Global Settings" since here is implemented the Application type, or in other words "the top level"
    #############################################################################################################################################

    /**
    * @type(string)
    * @title("LDAPIP")
    * @description("Ldap server IP")
    * @access(referrer, false)
    */
    public $LDAPIP;

    /**
    * @type(string)
    * @title("LDAPUSER")
    * @access(referrer, false)
    * @description("Ldap connection user")
    */
    public $LDAPUSER;

    /**
    * @type(string)
    * @title("LDAPPASS")
    * @description("Ldap Password")
    * @access(referrer, false)
    * @encrypted
    */
    public $LDAPPASS;

    /**
    * @type(string)
    * @title("dc1")
    * @description("DC1 string")
    * @access(referrer, false)
    */
    public $dc1;

    /**
    * @type(string)
    * @title("dc2")
    * @description("DC2 string")
    * @access(referrer, false)
    */
    public $dc2;

    /**
    * @type(string)
    * @title("owncloudhttp")
    * @description("OwnCloud UI Url")
    * @access(referrer, false)
    */
    public $owncloudhttp;

    /**
    * @type(string)
    * @title("storagelocation")
    * @access(referrer, false)
    */
    public $storagelocation;

    /**
    * @type(string)
    * @title("linuxurl")
    */
    public $linuxurl;

    /**
    * @type(string)
    * @title("macurl")
    */
    public $macurl;

    /**
    * @type(string)
    * @title("winurl")
    */
    public $winurl;

    /**
    * @type(string)
    * @title("Android Url")
    */
    public $androidurl;

    /**
    * @type(string)
    * @title("IOS Url")
    */
    public $iosurl;

    /**
    * @type(string)
    * @title("Notifications GUID")
    * @description("leave BLANK")
    * @readonly
    **/
    public $notifications;

    /**
    * @type(string)
    * @title("User Notification GUID")
    * @description("leave BLANK")
    * @readonly
    **/
    public $usernotification;

    /**
    * @type(string)
    * @title("User update Notification GUID")
    * @description("leave BLANK")
    * @readonly
    **/
    public $userupdatenotification;

    /**
    * @type(boolean)
    * @title("Billing")
    * @description("Is Billing Present")
    **/
    public $billing = false;

    /**
    * @type(boolean)
    * @title("Debug Mode")
    * @description("Enable Debug Mode for Endpoint")
    **/
    public $debug = false;

    private $checkDebug;

    #############################################################################################################################################
    ##   We must define also function for all the CRUD operations on this aps type
    ##   * configure will be called in case that an existing object is modified
    ##   * provision will be called in case that a new object must be created
    ##   * unprovision will be called in case that a delete operation is requiered on existing object
    ##   * retrive will be called in case that is requiered to read existing object
    #############################################################################################################################################

    public function configure($new=null)
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->debug("Configure has been called, we will overright all the information contained on ini file");
        $apsc = clone \APS\Request::getController();
        $apsc->resetSession();
        $old = $apsc->getResource($this->aps->id);
        if($new->LDAPPASS == 'default'){
            $new->LDAPPASS = $old->LDAPPASS;
        }
        if($new->debug == true || $new->debug == 1){
            $debug=1;
        }
        else{
            $debug=0;
        }
        $inifile=array(
            'GLOBAL' => array(
            ## We can capture the values parsed over $this->name of the variable that was defined on definition part for each attribute
                'LDAPIP' => $new->LDAPIP,
                'LDAPUSER' => $new->LDAPUSER,
                'LDAPPASS' => $new->LDAPPASS,
                'dc1' => $new->dc1,
                'dc2' => $new->dc2,
                'owncloudhttp' => $new->owncloudhttp,
                'storagelocation' => $new->storagelocation,
                'linuxurl' => $new->linuxurl,
                'macurl' => $new->macurl,
                'winurl' => $new->winurl,
                'androidurl' => $new->androidurl,
                'iosurl' => $new->iosurl,
                'notificator' => $old->notifications,
                'usernotification' => $old->usernotification,
                'userupdatenotification' => $old->userupdatenotification,
                'debug' => $debug
            ),
        );
        ## write_ini_file is a support function defined in the class
        $this->write_ini_file($inifile,'./config/config.ini',true);
        \APS\LoggerRegistry::get()->debug("Ini File has been written on configure, new data stored is\n\t".print_r($inifile,true));
        if($new->billing != $old->billing){
            if($new->billing == "" || $new->billing== false){
                $billing=0;
                $oldbilling=1;
                $new->billing = false;
            }
            else{
                $billing=1;
                $oldbilling=0;
                $new->billing = true;
            }
            #Here we introduce a limitation that on each "try" we will update 100000 tenants
            #This flag exists till this bug is not solved: APS-20471
            $tenants = $apsc->getResources('implementing(TENANT_TYPE),billing=eq='.$oldbilling.',limit(0,1000000)');
            foreach($tenants as $tenant){
                $tenantupdate = $apsc->getResource($tenant->aps->id);
                $tenantupdate->billing = $billing;
                if(!isset($tenantupdate->diskusage)){
                           $tenantupdate->diskusage = new stdClass();
                           $tenantupdate->diskusage->usage=0;
                }
                $apsc->updateResource($tenantupdate);
            }
        }
        $this->_copy($new);
    }

    public function provision()
    {
        if($this->debug == true){
            define('APS_DEVELOPMENT_MODE',true);
        }
        \APS\LoggerRegistry::get()->debug("Provision function has been called to deploy a new instance");
        $apsc = clone \APS\Request::getController();
        $this->notifications = "";
        $this->usernotification = "";
        $this->userupdatenotification = "";
        $notificator = $apsc->getResources('implementing(http://www.parallels.com/pa/pa-core-services/messenger-manager/1.0)');
        if(isset($notificator[0]->aps->id)){
            $this->notifications = $notificator[0]->aps->id;
            $json_notification = array(
                "description" => "File Sharing New User Creation Notification",
                "templates" => array(
                    array(
                        "locale" => "en",
                        "subject" => "File Sharing service was enabled for you",
                        "body"=> 'Welcome ${displayName}!
Your new File Sharing service was enabled.
Log into ${VENDOR_COMPANY_NAME} Customer Control Panel to get access.
Your Control Panel Access URL: ${VENDOR_CCP_URL}
Your login: ${login}',
                        "contentType"=> "text/plain")
                    )
            );
            $json_notification2 = array(
                "description" => "File Sharing User Update Notification",
                "templates" => array(
                    array(
                        "locale" => "en",
                        "subject" => "File Sharing service has been updated for you",
                        "body"=> 'Dear ${displayName}!
Your File Sharing service has been updated.
Now you can store up to ${newspace} in your File Sharing storage.
Log into ${VENDOR_COMPANY_NAME} Customer Control Panel to get access.
Your Control Panel Access URL: ${VENDOR_CCP_URL}
Your login: ${login}',
                        "contentType"=> "text/plain")
                    )
            );
            try{
                \APS\LoggerRegistry::get()->debug("Creating Notification");
                $notification=json_decode($apsc->getIo()->sendRequest(\APS\Proto::POST, "/aps/2/resources/".$notificator[0]->aps->id."/messenger/messagetypes", json_encode($json_notification)));
                $this->usernotification = $notification->id;
                $notification2 = json_decode($apsc->getIo()->sendRequest(\APS\Proto::POST, "/aps/2/resources/".$notificator[0]->aps->id."/messenger/messagetypes", json_encode($json_notification2)));
                $this->userupdatenotification = $notification2->id;

            } catch (Exception $e){
                \APS\LoggerRegistry::get()->debug("Error creating notification, pau exception");
            }
        }
        $inifile=array(
            'GLOBAL' => array(
                'LDAPIP' => $this->LDAPIP,
                'LDAPUSER' => $this->LDAPUSER,
                'LDAPPASS' => $this->LDAPPASS,
                'dc1' => $this->dc1,
                'dc2' => $this->dc2,
                'owncloudhttp' => $this->owncloudhttp,
                'storagelocation' => $this->storagelocation,
                'linuxurl' => $this->linuxurl,
                'macurl' => $this->macurl,
                'winurl' => $this->winurl,
                'androidurl' => $this->androidurl,
                'iosurl' => $this->iosurl,
                'notificator' => $this->notifications,
                'usernotification' => $this->usernotification,
                'userupdatenotification' => $this->userupdatenotification,
                'debug' => $this->debug
            )
        );
        $this->write_ini_file($inifile,'./config/config.ini',true);
        \APS\LoggerRegistry::get()->debug("Ini File has been written using the input params, data contained is: ".print_r($inifile,true));
    }

    public function _getDefault()
    {

        #############################################################################################################################################
        ## This is a support function to return some default values, when custom UI is used is better to have a json file on custom ui that contains
        ## such data unless we want to put some programatic logic behind this function
        #############################################################################################################################################

        \APS\LoggerRegistry::get()->debug("Get Defaults has been called");
        $inifile=array(
            'GLOBAL' => array(
                'LDAPIP' => '127.0.0.1',
                'LDAPUSER' => 'admin',
                'LDAPPASS' => 'password',
                'dc1' => 'owncloud',
                'dc2' => 'com',
                'storagelocation' => '/var/www/ownclouddata',
                'owncloudhttp' => 'endpoint.something.com/owncloudsite',
                'linuxurl' => 'https://software.opensuse.org/download/package?project=isv:ownCloud:desktop&package=owncloud-client',
                'winurl' =>'https://download.owncloud.com/desktop/stable/ownCloud-1.7.1.4382-setup.exe',
                'macurl' => 'https://download.owncloud.com/desktop/stable/ownCloud-1.7.1.1655.pkg',
                'androidurl' => 'https://play.google.com/store/apps/details?id=com.owncloud.android&hl=en',
                'iosurl' => 'https://itunes.apple.com/en/app/owncloud/id543672169?mt=8',
                'debug' => 'false'
            )
        );
    return $inifile['GLOBAL'];
    }

    public function retrieve()
    {
        $this->checkDebug();
        \APS\LoggerRegistry::get()->debug("It has been called retrive function");
        $inifile=parse_ini_file('./config/config.ini',true);
        \APS\LoggerRegistry::get()->debug("Retrive function is returning\n\t".print_r($inifile,true));
        return $inifile['GLOBAL'];
    }

    #############################################################################################################################################
    ##   We may define additional functions that will not be public, this means that doesn't represent an APS operation
    ##   in this application on this type we have 1:
    ##   * one to be able to write ini files in same format as redable by php function parse_ini_file
    #############################################################################################################################################

    private $write_ini_file;

    function write_ini_file($assoc_arr, $path, $has_sections=FALSE)
    {
        $content = "";
        if ($has_sections) {
            foreach ($assoc_arr as $key=>$elem) {
                $content .= "[".$key."]\n";
                    foreach ($elem as $key2=>$elem2) {
                        if(is_array($elem2)){
                            for($i=0;$i<count($elem2);$i++){
                                $content .= $key2."[] = \"".$elem2[$i]."\"\n";
                            }
                        }
                        else if($elem2=="") $content .= $key2." = \n";
                        else $content .= $key2." = \"".$elem2."\"\n";
                    }
            }
        } else {
            foreach ($assoc_arr as $key=>$elem) {
                if(is_array($elem)) {
                    for($i=0;$i<count($elem);$i++) {
                        $content .= $key2."[] = \"".$elem[$i]."\"\n";
                    }
                }
                else if($elem=="") $content .= $key2." = \n";
                else $content .= $key2." = \"".$elem."\"\n";
            }
        }
        if (!$handle = fopen($path, 'w')) {
            return false;
        }
        if (!fwrite($handle, $content)) {
            return false;
        }
        fclose($handle);
        return true;
    }

    function checkDebug()
    {
        $inifile = parse_ini_file('./config/config.ini',true);
        if($inifile['GLOBAL']['debug'] == 'true' || $inifile['GLOBAL']['debug'] == '1'){
            define("APS_DEVELOPMENT_MODE", true);
        }
        return;
    }
} ## Close of the Class
?>
