��    j      l  �   �      	     	     !	     1	     D	     Y	     k	  $   |	     �	  
   �	  
   �	     �	     �	     �	     �	     
     
      
     &
     -
     5
     E
     U
     d
     s
     |
     �
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
               %  #   @     d     q     v  
   z     �     �     �     �     �     �     �          !  ,   0  Q   ]  	   �     �     �     �     �     �     
            *   .     Y     e     q     �     �     �  %   �     �     �     �     �                     "     9     G     Y  	   a  I   k     �     �  	   �  	   �     �     �       5         V  <   d     �     �     �     �     �     �     �     �     �          !  �  (     �     �     �          $     B  $   U  
   z  
   �  
   �     �      �     �     �     �                          $     ;     M     b     x     �     �     �     �     �     �     �     �     �     �     
     %     +     :  )   V     �     �     �     �     �     �     �     �     �          +  !   A     c  3   u  v   �  	      #   *     N     U     i     x     �     �     �  (   �     �     �     �               +  2   ;     n     �     �     �     �     �     �     �     �       	           ^   1     �     �     �     �  %   �            >   .     m  I   y     �     �     �     �     �     �                    3     C                 P   `                     3   b   2   >             %   J   .   _   
   ?               I   [       8             4   5          *   ,   h      (   Y   T   O               A   9   =               f   #   @             X   i           Q   ^   &   )       \   E               S   :      M      R   N   K   1   B   L   F   7              C   D                H   +                  e         '                   6       G      0   a   <   /       "       $   ;   !   W                   j   U   	   ]           Z   g   V   c                       d   -    API Credentials Access OwnCloud Assigned Quota(GB) Back to Samples list Back to User list Confirm Settings Correcting minor bug on usercp2.html Current Usage DC1 string DC2 string Delete Diskspace - Usage Only Diskspace in GB Download Client for your OS Edit Edit Sample Email Enable General Getting Started Global Settings Group Password Home Directory LDAP dc1 LDAP dc2 LDAPIP LDAPPASS LDAPUSER Ldap IP Address Ldap Password Ldap connection user Ldap server IP Ldap user Linux Client Linux Client Download URL Login MacOSX Client MacOSX Client Download URL Modify user {user.owncloudusername} More Clients Name New New Sample OwnCloud OwnCloud Application website OwnCloud Clients OwnCloud Endpoint RPM OwnCloud Global Settings OwnCloud Tenant OwnCloud UI Url OwnCloud User Samples OwnCloud Users OwnCloud for user {enduser.owncloudusername} OwnCloud is a file sharing solution, with multi device support including mobiles. Parallels Please specify your settings Profile Profile Select Profile Type Providers Guide Quota Quota Assigned Resource Model Resource with the same name already exists Sample Name Sample edit Sample {sample.name} Samples Samples List Save changes Screenshot of Parallels Control Panel Select User Selected Profile Selected User Status Step 1 Step 2 Storage Quota in GB Storage assigned in GB Store Changes Subscribers guide Suspend Tenant ID There is no users to attach to OwnCloud, create user first from Users tab Used Storage(GB) User User Info User Name User Storage Location Windows Client Windows Client Download URL You don't have enought resources to create a new user Your OwnCloud Your service is disabled, contact your Company administrator dc1 dc2 gradient gradientradial http://www.parallels.com linuxurl macurl owncloudhttp sample-aps2-owncloud-ldap storagelocation winurl Project-Id-Version: sample-aps2-owncloud-ldap-1-1
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2013-07-15 22:38+0200
PO-Revision-Date: 2013-07-15 23:06+0100
Last-Translator: Marc Serrat <mserrat@parallels.com>
Language-Team: Spanish <EMAIL@ADDRESS>
Language: es_ES
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n != 1);
X-Generator: Poedit 1.5.7
 Credenciales API Acceder a OwnCloud Quota Assignada(GB) Volver al listado de Perfiles Volver al listado de usuarios Confirmar Opciones Correcting minor bug on usercp2.html Uso Actual Cadena DC1 Cadena DC2 Eliminar Espacio en disco - Solamente Uso Espacio en disco en GB Descargar Cliente para su SO Editar Editar Perfil Email Activar General Guia de inicio rápido Opciones Globales Contraseña de grupo Directorio particular LDAP dc1 LDAP dc2 LDAPIP LDAPPASS LDAPUSER Dirección IP Ldap Contraseña Ldap Usuario Conexión Ldap IP Servidor LDAP Usuario LDAP Cliente Linux URL Descarga Cliente Linux Login Cliente MacOSX URL Descarga Cliente MacOSX Modificar usuario {user.owncloudusername} Más Clientes Nombre Nuevo Nuevo Perfil OwnCloud Sitio Web Applicación OwnCloud Clientes OwnCloud RPM Endpoint OwnCloud Opciones Globales OwnCloud Organización OwnCloud URL Interfaz OwnCloud Perfiles de usuario para OwnCloud Usuarios OwnCloud OwnCloud para el usuario {enduser.owncloudusername} OwnCloud is una solución para compartir archivos, dispone de soporte para múltiples dispositivos incluyendo mobiles. Parallels Por favor, especifique sus opciones Perfil Perfil Seleccionado Tipo de Perfil Guia del Proveedor Quota Quota assignada Modelo de Recursos Un recurso con el mismo nombre ya existe Nombre Perfil Editar perfil Perfil {sample.name} Perfiles Listado de Perfiles Guardar cambios Captura de Pantalla del Panel de Control Parallels Seleccione Usuario Perfil Seleccionado Usuario Seleccionado Estado Paso 1 Paso 2 Almacenamiento en GB Almacenamiento asignado en GB Guardar Cambios Guia del suscriptor Suspender ID Organización No hay usuarios a los que activar OwnCloud, cree un usuario primero desde la pestaña usuarios Almacenamiento Usado(GB) Usuario Información de Usuario Nombre de Usuario Localización almacenamiento Usuarios Cliente Windows URL Descarga Cliente Windows No dispone de suficientes recursos para crear un nuevo usuario Su OwnCloud Su servicio esta desactivado, contacte con el administrador de su empresa dc1 dc2 gradient gradientradial http://www.parallels.com linuxurl macurl owncloudhttp sample-aps2-owncloud-ldap storagelocation winurl 