define([
    "dojo/_base/declare",
    "dijit/registry",
    "aps/_View"
], function(declare, registry, _View) {

    var USERNAME = "owc-activate-userName",
        MY_USERNAME_PANEL = "owc-activate-userName-panel",
        MY_PROFILE_PANEL = "owc-activate-profile-panel",
        MY_PROFILE = "owc-activate-profile",
        PWD_FIELD  = "owc-activate-password",
        PWD_MIN = 7,
        PWD_MAX = 64;

    var userData = {user:{}};

    return declare(_View, {
        init: function() {
            return [
                ["aps/Panel", { id: MY_PROFILE_PANEL, title: _("My File Sharing Profile")}, [
                   ["aps/Output", {
                       id: MY_PROFILE,
                       label: _('My Sharing Profile')
                   }]
                ]],
                ["aps/Panel", { id: MY_USERNAME_PANEL, title: _("My File Sharing Login")}, [
                    ["aps/FieldSet", {
                        gridSize: "md-12 xs-12"
                    },[
                        ["aps/Output", {
                            id: USERNAME,
                            label: _('Username')
                        }],
                        ["aps/Password",{
                            id: PWD_FIELD,
                            autoSize: false,
                            label: _('Password'),
                            showStrengthIndicator: true,
                            required: true,
                            gridSize: "md-3 xs-12",
                            customValidator: function(value) {
                                if (value === null || value.length < PWD_MIN || value.length > PWD_MAX) return false;
                                return true;
                            }
                        }]
                    ]]
                ]]
            ];
        },

        onContext: function () {
            function drawLogin(ownCloudUserLogin){
                userData.id = ownCloudUserLogin.aps.id;
                registry.byId(MY_PROFILE).set(
                    "value",
                    _('__name__ (__quota__ GB)', {
                        name: ownCloudUserLogin.assignedprofilename,
                        quota: ownCloudUserLogin.quota
                    })
                );
                registry.byId(USERNAME).set("value", ownCloudUserLogin.owncloudusername);
            }

            function updateData() {
                for (var i=0; i<aps.context.vars.endusers.length; i++){
                    var userObj = aps.context.vars.endusers[i];
                    if(userObj.user.userId === aps.context.user.userId){
                        drawLogin(userObj);
                        break;
                    }
                }
            }

            updateData();
            aps.apsc.hideLoading();
        },

        onNext: function () {
            var pwdField = registry.byId(PWD_FIELD);
            if(!pwdField.validate()){
                aps.apsc.cancelProcessing();
                return;
            }
            userData.user.owncloudpassword = pwdField.value;
            var savedXhr = {
                url: '/aps/2/resources/' + userData.id + '/changepassword',
                method: 'PUT',
                headers: {
                    "Content-Type": 'application/json'
                },
                data: JSON.stringify(userData.user)
            };
            aps.apsc.next({objects: [{xhr: savedXhr}]});
        }

    });// Close of return declare
}); // Close of Main function
