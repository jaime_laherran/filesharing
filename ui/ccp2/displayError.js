define(["dijit/registry", "aps/Message"],
    function (registry, Message) {
        return function(err, messagetype) {
            aps.apsc.cancelProcessing();
            var page = registry.byId("apsPageContainer");
            if(!page){
                return;
            }
            var messages = page.get("messageList");
            messages.removeAll();
            messages.addChild(new Message({description: err, type: messagetype||'error'}));
        };
    }
);
