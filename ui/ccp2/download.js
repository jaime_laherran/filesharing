define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojo/when",
    "dojox/mvc/getStateful",
    "aps/_View",
    "aps/ResourceStore"
], function (declare, Deferred, when, getStateful, View, Store) {

    var globalSettings;

    return declare(View, {
        init: function () {
            function open_url(url) {
                window.open(url, "_blank");
            }

            return [
                ["aps/Tiles", {
                    id: 'owc_dn_desktop_tiles',
                    title: _("Desktop")
                }, [
                    ["aps/Tile", {
                        id: 'owc_dn_macos_tile',
                        title: _("Mac OSX App"),
                        gridSize: 'xs-12 md-4',
                        buttons: [
                            {
                                title: _('Download'),
                                iconClass: "fa-download",
                                autoBusy: false,
                                onClick: function (){
                                    open_url(globalSettings.macurl);
                                }
                            }
                        ]
                    }, [
                        ["aps/Output", {
                            id: 'owc_dn_macos_tile_out',
                            "class": "lead",
                            content: "<span class='fa fa-apple'></span> " + _("OSX 10.7+, 64 bit")
                        }]
                    ]],

                    ["aps/Tile", {
                        id: 'owc_dn_win_tile',
                        title: _("Windows App"),
                        gridSize: 'xs-12 md-4',
                        buttons: [
                            {
                                title: _('Download'),
                                iconClass: "fa-download",
                                autoBusy: false,
                                onClick: function (){
                                    open_url(globalSettings.winurl);
                                }
                            }
                        ]
                    }, [
                        ["aps/Output", {
                            id: 'owc_dn_win_tile_out',
                            "class": "lead",
                            content: "<span class='fa fa-windows'></span> " + _("XP, Vista, 7 and 8")
                        }]
                    ]],

                    ["aps/Tile", {
                        id: 'owc_dn_lin_tile',
                        title: _("Linux App"),
                        gridSize: 'xs-12 md-4',
                        buttons: [
                            {
                                title: _('Download'),
                                iconClass: "fa-download",
                                autoBusy: false,
                                onClick: function (){
                                    open_url(globalSettings.linuxurl);
                                }
                            }
                        ]
                    }, [
                        ["aps/Output", {
                            id: 'owc_dn_lin_tilei_out',
                            "class": "lead",
                            content: "<span class='fa fa-linux'></span> " + _("Multiple distributions")
                        }]
                    ]]
                ]],

                ["aps/Tiles", {
                    id: 'owc_dn_mobile_tiles',
                    title: _("Mobile")
                }, [
                    ["aps/Tile", {
                        id: 'owc_dn_android_tile',
                        title: _("Android App"),
                        gridSize: 'xs-12 md-4',
                        buttons: [
                            {
                                title: _('Download'),
                                iconClass: "fa-download",
                                autoBusy: false,
                                onClick: function (){
                                    open_url(globalSettings.androidurl);
                                }
                            }
                        ]
                    }, [
                        ["aps/Button", {
                            id: 'owc_dn_android_btn',
                            size: 3,
                            type: "block",
                            title: _("Google Play") + "<small>" + _("ANDROID APP ON") + "</small>",
                            "class": "btn-lg btn-default promo",
                            autoBusy: false,
                            onClick: function () { open_url(globalSettings.androidurl); },
                            iconClass: "fa fa-android pull-left"
                        }]
                    ]],
                    ["aps/Tile", {
                        id: 'owc_dn_ios_tile',
                        title: _("iPhone App"),
                        gridSize: 'xs-12 md-4',
                        buttons: [
                            {
                                title: _('Download'),
                                iconClass: "fa-download",
                                autoBusy: false,
                                onClick: function (){
                                    open_url(globalSettings.iosurl);
                                }
                            }
                        ]
                    }, [
                        ["aps/Button", {
                            id: 'owc_dn_ios_btn',
                            size: 3,
                            type: "block",
                            title: ("App Store") + "<small>" + ("Available on the iPhone") + "</small>",
                            "class": "btn-lg btn-default promo",
                            autoBusy: false,
                            onClick: function () { open_url(globalSettings.iosurl); },
                            iconClass: "fa fa-apple pull-left"
                        }]
                    ]]
                ]]
            ];
        },
        onContext: function() {
            var deferred;

            if ('enduser' in aps.context.vars) { // mycp
                if('myglobals' in aps.app.model) {
                    globalSettings = aps.app.model.myglobals;
                } else {
                    var store = new Store ({
                        target: "/aps/2/resources/" + aps.context.vars.enduser.aps.id
                    });
                    deferred = new Deferred();
                    when(store.get('getclient'), function(client) {
                        aps.app.model.myglobals = getStateful(client);
                        globalSettings = aps.app.model.myglobals;
                        deferred.resolve();
                    });
                }
            } else { // for ccp use always from navigation vars
                globalSettings = aps.context.vars.globals;
            }

            if (deferred) deferred.then(aps.apsc.hideLoading); else aps.apsc.hideLoading();

            return deferred;
        }
    });
});
