define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "aps/_PopupView",
    "aps/ResourceStore",
    "./displayError",
    "../common/TYPES",
    "aps/ready!"
], function (declare, Deferred, _PopupView, Store, displayError, TYPES) {

    var POPUP_PANEL = 'owc-bm-panel',
        POPUP_FIELDSET = 'owc-bm-FieldSet',
        PROFILES_COUNT = 'owc-bm-add-profiles-value';

    return declare(_PopupView, {
        size: "sm",
        init: function () {
            return [
                "aps/Panel", {id: this.genId(POPUP_PANEL)}, [
                    ["aps/FieldSet", {
                        id: this.genId(POPUP_FIELDSET),
                        gridSize: "xs-12 md-12"
                    }, [
                        ["aps/Spinner", {id: this.genId(PROFILES_COUNT), label: _('Total Profiles'), minimum: 0, maximum: 99, value: 0}]
                    ]]
                ]
            ];
        },
        onContext: function () {
            var deferred = new Deferred(),
                profile = aps.context.vars.profile;

            var popupTitle = _('__name__ (__quota__ GB)', {name: profile.name, quota: profile.quota});
            this.set("label", popupTitle);
            aps.apsc.buttonState("submit", {disabled: true});

            aps.biz.getResourcesAmount(
                [{apsType: TYPES.SAMPLES_TYPE, apsId: profile.aps.id}],
                aps.context.vars.tenant.aps.subscription
            ).then(function (resources) {
                var profileInfo = resources.find(function(resource) { return resource.aps.id == profile.aps.id; }); // XXX
                if (profileInfo) {
                    var spinner = this.byId(PROFILES_COUNT);
                    spinner.set({
                        maximum: (profileInfo.max >= 0) ? profileInfo.max : null,
                        minimum: Math.max(profileInfo.min, profileInfo.usage), // we cannot force user to use all included resources
                        value: profileInfo.amount
                    });
                    spinner.focus();
                    aps.apsc.buttonState("submit", {disabled: false});
                }
                deferred.resolve();
            }.bind(this), function(err) {
                if (err && err.response && err.response.data) {
                    var data = err.response.data instanceof Object ? err.response.data : JSON.parse(err.response.data);
                    displayError(_(data.message, data.details || {}, data.pkgId), 'error');
                } else {
                    displayError(_("Failed to get resource info with error \"__msg__\"", {msg: err.response.data.message}), 'error');
                }
                deferred.resolve();
            });

            return deferred.then(aps.apsc.hideLoading);
        },
        onCancel: function () {
            this.cancel();
        },
        onSubmit: function() {
            if (!this.byId(PROFILES_COUNT).validate()) {
                aps.apsc.cancelProcessing();
            } else {
                var amount = this.byId(PROFILES_COUNT).value,
                    profile = aps.context.vars.profile,
                    subscription = aps.context.vars.tenant.aps.subscription;

                aps.biz.getResourcesAmount(
                    [{apsType: TYPES.SAMPLES_TYPE, apsId: profile.aps.id}],
                    subscription
                ).then(function (resources) {
                    var profileInfo = resources.find(function(resource) { return resource.aps.id == profile.aps.id; }); // XXX
                    if (profileInfo) {
                        profileInfo.amount = amount;
                    }
                    aps.biz.changeResourcesAmount(resources, subscription);
                    return aps.biz.operate({}, subscription);
                }, displayError)
                .then(function () {
                    this.submit();
                }.bind(this), displayError);
            }
        },
        onHide: function() {
            this.byId(PROFILES_COUNT).set({minimum: 0, maximum: 99, value: 0});
            aps.apsc.buttonState("submit", {disabled: true});
        }
    });
});
