define([
    "dojo/_base/declare",
    "aps/_View",
    "dojo/store/Observable",
    "aps/Memory",
    "dojo/Deferred",
    "aps/Output",
    "aps/tiles/UsageInfoTile",
    "./loadProfiles",
    "aps/ready!"
], function(declare, _View, Observable, Memory, Deferred, Output, UsageInfoTile, loadProfiles) {

    var PROFILES_TILES = 'owc-pl-profilestiles',
        PROFILE_TILE_PREFIX = 'owc-pl-profile-tile-',

        BUY_PROFILES_POPUP = 'http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp#buymoreprofiles';

    var billingEnabled = false,
        canBuyAdditional = false;

    return declare(_View, {
        init: function () {
            return ["aps/Container", {
                collapsible: false
            },
                [
                    ["aps/Tiles", {id: this.genId(PROFILES_TILES)}, []]
                ]
            ];

        },
        onContext: function () {
            function updateBillingStatus() {
                var def = new Deferred();

                if ('tenant' in aps.context.vars) {
                    if ('billing' in aps.context.vars.tenant) {
                        billingEnabled = (aps.context.vars.tenant.billing !== 0);
                    }
                }

                if (!billingEnabled) {
                    def.resolve([false]);
                } else {
                    loadProfiles.getBillingSubscription(aps.context.vars.tenant, aps.context.vars.billing).then(function (bssSubscription) {
                          // exists, is not Trial and Active Status XXX APS-22306
                        var canBuy = (bssSubscription &&
                                      !bssSubscription.trial &&
                                      bssSubscription.status == 30);

                        def.resolve([canBuy, bssSubscription]);
                    }, function(err) {
                        def.reject(err);
                    });
                }
                return def;
            }

            function updateProfile(profile, removedFrom, insertedInto) {
                var visibilitywidget = true,
                    notPurchased = false,
                    titleText = _('__name__ (__quota__ GB)', {name: profile._profile.name, quota: profile._profile.quota});

                if (profile.amount === 0 && !canBuyAdditional) {
                    visibilitywidget = false;
                }

                var profileTile = this.byId(PROFILE_TILE_PREFIX + profile.aps.id);
                if (!profileTile) {
                    profileTile = new UsageInfoTile({
                        visible: visibilitywidget,
                        title: titleText,
                        id: this.genId(PROFILE_TILE_PREFIX + profile.aps.id),
                        gridSize: 'xs-12 md-6'
                    });
                    this.byId(PROFILES_TILES).addChild(profileTile, insertedInto);
                }

                var hintText;
                if (profile.usage < profile.amount) {
                    hintText = _('__available__ Available', {available: (profile.amount - profile.usage)});
                } else {
                    hintText = _('No Available');
                }
                  // will show Pie
                if (profile.amount !== undefined && profile.amount > 0) {
                    profileTile.set({
                        visible: visibilitywidget,
                        showPie: true,
                        maximum: profile.amount,
                        value: profile.usage,
                        textFirstNumber: '${value}',
                        textSecondNumber: '${maximum}',
                        description: 'Profiles Used',
                        usageHint: hintText
                    });
                } else { // will show Resource Usage
                    if (profile.amount === 0 && profile.usage === 0) {
                        notPurchased = true;
                        profileTile.set({
                            visible: visibilitywidget,
                            showPie: false,
                            textFirstNumber: '',
                            textSecondNumber: '',
                            description: _('Not Purchased Yet'),
                            usageHint: ''
                        });
                    } else {
                        if (profile.amount === undefined) {
                            hintText = _('Unlimited Available');
                        }

                        profileTile.set({
                            visible: visibilitywidget,
                            showPie: false,
                            textSecondNumber: '',
                            maximum: undefined,
                            value: profile.usage,
                            textFirstNumber: '${value}',
                            description: _('Profiles Used'),
                            usageHint: hintText
                        });
                    }
                }

                if (!canBuyAdditional || profile.limit === undefined) {
                    profileTile.set({
                        buttons: []
                    });
                } else {
                    var buyMoreFunc = function () {
                        this.nav.showPopup({
                            viewId: BUY_PROFILES_POPUP,
                            resourceId: profile.aps.id,
                            modal: false
                        });
                    }.bind(this);
                    profileTile.set({
                        buttons: [
                            {
                                'title': notPurchased ? _('Buy') : _('Buy More'),
                                iconClass: "fa-plus",
                                autoBusy: false,
                                onClick: buyMoreFunc
                            }
                        ]
                    });
                }
            }

            var deferred = new Deferred(),
                profilesMemory = new Observable(new Memory({
                    idProperty: "aps.id"
                }), true),
                profListResults = profilesMemory.query("ne(limit,0)");

            profListResults.observe(updateProfile.bind(this), true);

            updateBillingStatus().then(function (results){
                canBuyAdditional = results[0];
                return results[1];
            })
            .then(function() {
                return loadProfiles.getProfilesAmount(
                    aps.context.vars.tenant,
                    {
                        memory: profilesMemory
                    }
                );
            })
            .then(function () {
                deferred.resolve();
            });

            return deferred.then(aps.apsc.hideLoading);
        },
        onHide: function() {
        }
    });
});
