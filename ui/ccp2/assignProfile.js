define([
    "dojo/_base/declare",
    "./configureProfile"
], function (declare, configureProfileView) {
    "use strict";
    return declare(configureProfileView);
});
