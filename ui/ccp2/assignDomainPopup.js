define([
    "dojo/_base/declare",
    "dojo/promise/all",
    "dojo/Deferred",
    "aps/_PopupView",
    "aps/ResourceStore",
    "aps/xhr",
    "./displayError",
    "../common/TYPES",
    "dojo/text!../json/ownclouddomain.json"
], function (declare, all, Deferred, _PopupView, ResourceStore, xhr, displayError, TYPES, owcDomainJson) {

    var DESCRIPTION = 'owc-output-description',
        INFO_PANEL = 'owc-dom-panel',
        DOMAIN_DESC = 'owc-dom-desc',
        DOMAIN_PROTO = 'owc-dom-proto',
        DOMAIN_PREFIX = 'owc-dom-prefix',
        DOMAIN_POSTFIX = 'owc-dom-postfix',
        DEFAULT_ZONE = 'files';

    var originalOwcZone,
        isReplace,
        owcDomainService,
        owcPreviousDomain;

    return declare(_PopupView, {
        size: 'md',
        optionSelected: 'none',
        init: function () {
            return [
                ["aps/Container", [
                    ["aps/Output", {
                        id: this.genId(DESCRIPTION),
                        value: _('You and your employees will have access to File Sharing sevice by selected domain.'),
                        'class': 'lead'
                    }]
                ]],
                ["aps/Panel", {id: this.genId(INFO_PANEL), title: _('Select Sub-domain')}, [
                    ["aps/Output", {
                        id: this.genId(DOMAIN_DESC),
                        value: _('For save all current domain zone preference your access will be available by default on subdomain "__zoneName__". You always can change the subdomain to your own.', {zoneName: DEFAULT_ZONE})
                    }],
                    ["aps/Container", [
                        ["aps/Output", {
                            id: this.genId(DOMAIN_PROTO),
                            value: "https://"
                        }],
                        ["aps/TextBox", {
                            id: this.genId(DOMAIN_PREFIX),
                            required: true,
                            pattern: "^(([a-zA-Z0-9]|[a-zA-Z0-9][a-zA-Z0-9-]*[a-zA-Z0-9]).)*([A-Za-z0-9]|[A-Za-z0-9][A-Za-z0-9-]*[A-Za-z0-9])",
                            invalidMessage: _("Sorry, subdomain must be an string and contain only letters and digits"),
                            missingMessage: _("Sorry, please input a value")
                        }],
                        ["aps/Output", {
                            id: this.genId(DOMAIN_POSTFIX)
                        }]
                    ]]
                ]]
            ];
        },
        onContext: function() {

            var deferred = new Deferred(),
                store = new ResourceStore({
                    target: '/aps/2/resources'
                }),
                tenant = aps.context.vars.tenant,
                selectedDomain = aps.context.vars.selectedDomain;

            this.byId(DOMAIN_POSTFIX).set('value', _('.__domainName__', {domainName: selectedDomain.name}));

            all([store.query('implementing(' + TYPES.DOMAIN_TYPE + '),select(domain),eq(domain.aps.id,' + selectedDomain.aps.id + ')'),
                 store.query('implementing(' + TYPES.DOMAIN_TYPE + ')')]).then(function(allData)
            {
                owcDomainService = allData[0][0];
                var owcDomains = allData[1];

                originalOwcZone = owcDomainService ? owcDomainService.accessdomainprefix : null;

                if (!tenant) {
                    this.byId(DESCRIPTION).set('value', '');
                    displayError(_('Has not been found File Sharing Activated in your account'));
                    this.byId(INFO_PANEL).set('visible', false);
                    aps.apsc.buttonState('submit', {visible: false});
                } else {
                    this.byId(INFO_PANEL).set('visible', true);
                    aps.apsc.buttonState('submit', {visible: true});

                    this.byId(DOMAIN_PREFIX).set('value', originalOwcZone ? originalOwcZone : DEFAULT_ZONE);

                    if (owcDomains && owcDomains[0]) {
                        if (!owcDomainService ||
                            owcDomainService.aps.subscription != tenant.aps.subscription) // another MSS subscription
                        {
                            isReplace = true;
                            owcPreviousDomain = owcDomains[0];
                            if (!owcDomainService) {
                                displayError(_('File Sharing will be enabled only for the selected domain. Previous domain "__domainName__" will be disabled', {domainName: owcPreviousDomain.name}), 'warning');
                            }
                        }
                    }
                    this.byId(DESCRIPTION).set('value', _('You and your employees will have access to File Sharing sevice by selected domain.'));
                }
                deferred.resolve();
            }.bind(this), displayError);

            return deferred.then(aps.apsc.hideLoading);
        },
        onCancel: function() {
            this.cancel();
        },
        onSubmit: function() {
            var submitFunc = function() {this.submit();}.bind(this);

            function changeOwcZone(domainService, newZone) {
                domainService.accessdomainprefix = newZone;
                xhr('/aps/2/resources/' + domainService.aps.id, {
                    method: 'PUT',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    data: JSON.stringify(domainService)
                }).then(submitFunc, displayError);
            }

            function assingOwcDomain(owcZone, selectedDomain) {
                var tenant = aps.context.vars.tenant;
                var newDomain = JSON.parse(owcDomainJson);
                newDomain.aps.subscription = tenant.aps.subscription;
                newDomain.tenant.aps.id = tenant.aps.id;
                newDomain.accessdomainprefix = owcZone;
                newDomain.domain.aps.id = selectedDomain.aps.id;
                xhr('/aps/2/resources/', {
                    method: 'POST',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    data: JSON.stringify(newDomain)
                }).then(submitFunc, displayError);
            }

            function changeOwcDomain(owcZone, selectedDomain) {
                xhr('/aps/2/resources/' + owcPreviousDomain.aps.id, {
                    method: 'DELETE'
                }).then(assingOwcDomain.bind(this, owcZone, selectedDomain));
            }

            if (!this.byId(DOMAIN_PREFIX).validate()) {
                aps.apsc.cancelProcessing();
                return;
            }

            var selectedDomain = aps.context.vars.selectedDomain,
                owcZone = this.byId(DOMAIN_PREFIX).get('value');

            if (originalOwcZone && !isReplace) { // already assigned
                if (owcZone === originalOwcZone) { // nothing to do?
                    this.cancel();
                } else {
                    changeOwcZone(owcDomainService, owcZone);
                }
            } else {
                if (isReplace) { // replace another domain or subscription
                    changeOwcDomain(owcZone, selectedDomain);
                } else {
                    assingOwcDomain(owcZone, selectedDomain);
                }
            }
        },
        onHide: function() {
            this.byId(DOMAIN_PREFIX).reset();
        }
    });
});
