define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojo/when",
    "dojox/mvc/getStateful",
    "dijit/registry",
    "aps/_View",
    "aps/ResourceStore",
    "aps/xhr",
    "aps/tiles/UsageInfoTile",
    "../common/TYPES"
], function (declare, Deferred, when, getStateful, registry, _View, Store, xhr, UsageInfoTile, TYPES) {

    var PROFILES_LIST = 'owc-sign-profiles-list',
        PROFILE_TILE = 'owc-sign-profile-',
        USER_NAME = 'owc-sign-username',
        USER_PWD = 'owc-sign-password',
        PWD_MIN = 7,
        PWD_MAX = 64;

    var profileValue;

    function init() {
        var deferred = new Deferred(),
            wizardData=aps.context.wizardData['http://www.parallels.com/ccp-signup#signupview'],

            profilesList = registry.byId(PROFILES_LIST),
            firstProfile,
            store = new Store({
                target: "/aps/2/resources/"
            });

        profilesList.removeAll();

        when(store.query("implementing(" + TYPES.SAMPLES_TYPE + "),sort(-quota)"), function(samples) {
            for (var l=0; l < samples.length; l++) {
                var nextSample = samples[l];
                for (var i = 0; i < wizardData.planRates.length; i++) {
                    var nextRate = wizardData.planRates[i];
                    if (nextRate.apsType === TYPES.SAMPLES_TYPE &&
                        nextRate.apsId == nextSample.aps.id &&
                        nextRate.included > 0)
                    {
                        var nextButtonId = PROFILE_TILE + nextRate.apsId;
                        var nextButton = registry.byId(nextButtonId);
                        if (nextButton) {
                            nextButton.destroy();
                        }
                        nextButton = new UsageInfoTile({
                            id: nextButtonId,
                            title: nextRate.title,
                            owcProfileId: nextRate.apsId,
                            value: nextSample.quota,
                            textSuffix: _('GB Storage'),
                            usageHint: (
                                nextRate.included > 1 ?
                                    _('__included__ Profiles Available', {included: nextRate.included})
                                    : _('__included__ Profile Available', {included: nextRate.included})
                            ),
                            showPie: false
                        });
                        profilesList.addChild(nextButton);

                        if (!firstProfile) {
                              // first added profile will be selected if there
                              // is no selected profile
                            firstProfile = nextRate.apsId;
                        }
                    }
                }
            }
            profilesList.set('selectionArray', getStateful([PROFILE_TILE + (profileValue ? profileValue : firstProfile)]));

            var widget = registry.byId(USER_NAME);
            if (wizardData.objects.length > 0 && 'users' in wizardData.objects[0] && wizardData.objects[0].users.length > 0) {
                widget.set('value', wizardData.objects[0].users[0].login);
            }

            deferred.resolve();
        });

        return deferred;
    }

    return declare(_View, {
        init: function () {
            return [
                ["aps/Output", {id: "owc-sign-cfg-out", content: _('Configure File Sharing service.')}],
                ["aps/Tiles", {
                    id: PROFILES_LIST,
                    title: _('Select Your File Sharing Profile'),
                    selectionMode: 'single',
                    required: true,
                    onClick: function() {
                        var selectedTile = registry.byId(this.get('selectionArray')[0]);
                        profileValue = selectedTile.owcProfileId;
                    }
                }],
                ["aps/Container", {
                    id: "owc-sign-username-group",
                    title: _('Setup Your File Sharing User')
                }, [
                    ["aps/Panel", {
                        id: "owc-sign-username-panel"
                    }, [
                        ["aps/FieldSet", {
                            id: "owc-sign-username-container"
                        }, [
                            ["aps/Output", {
                                id: USER_NAME,
                                label: _("Username")
                            }],
                            ["aps/Password", {
                                id: USER_PWD,
                                autoSize: false,
                                gridSize: 'xs-6 md-3',
                                label: _("Password"),
                                showStrengthIndicator: true,
                                required: true,
                                customValidator: function(value) {
                                    if (value === null || value.length < PWD_MIN || value.length > PWD_MAX) return false;
                                    return true;
                                }
                            }]
                        ]]
                    ]]
                ]]
            ];
        },
        onContext: function() {
            return init().then(aps.apsc.hideLoading());
        },
        onPrev: function() {
            aps.apsc.prev();
        },
        onNext: function() {
            var pwdField = registry.byId(USER_PWD);
            if (!pwdField.validate()) {
                aps.apsc.cancelProcessing();
                return;
            }

            if (!profileValue) {
                aps.apsc.next();
            } else {
                var data = {
                    aps: {
                        type: TYPES.USERS_TYPE
                    },
                    samples:{
                        aps: {
                            id: profileValue
                        }
                    },
                    owncloudpassword: pwdField.get('value')
                };
                aps.apsc.next(data);
            }
        }
    });
});
