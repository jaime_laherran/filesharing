define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojox/mvc/getStateful",
    "aps/_View",
    "./loadProfiles",
    "./profilesTools",
    "dojo/text!../json/ownclouduser.json"
], function (
    declare, Deferred, getStateful, _View,
    loadProfiles, profilesTools,
    ownCloudUserJson
) {
    var DESCRIPTION = 'owc-au-description',
        PROFILES_LIST = 'owc-au-profiles-list',
        PROFILE_TILE = 'owc-au-profile-';

    function updateResourcesAmount() {
        var tilesList = this.byId(PROFILES_LIST);
        var selection = tilesList.get('selectionArray'),
            tiles = tilesList.getChildren(),
            resources = [],
            selectedTileId;

        if (selection.length > 0) selectedTileId = selection[0];

        tiles.forEach(function (tile) {
            var profile = tile._profile;
            if (profile) {
                profile.amount = profile.amountCopy; // XXX remove for new aps.biz
                if (tile.id === selectedTileId) profile.amount += profile.amountDue;
                resources.push(profile);
            }
        });
        aps.biz.changeResourcesAmount(resources, aps.context.vars.tenant.aps.subscription);
    }

    return declare(_View, {
        init: function () {
            return [
                ["aps/Output", {
                    id: this.genId(DESCRIPTION),
                    content: _('Select which File Sharing profile you want to assign to the new users.'),
                    'class': 'lead'
                }],
                ["aps/Tiles", {
                    id: this.genId(PROFILES_LIST),
                    title: _('Select Profile'),
                    selectionMode: 'single',
                    required: true,
                    onClick: updateResourcesAmount.bind(this)
                }]
            ];
        },
        onContext: function() {
            // in TAKT we expect to have ONLY 1 tenant of filesharing, but this must be changed in future
            var usersToBeCreated = 0,
                deferred = new Deferred();

            var usersList = aps.biz.getResourcesToBind().users;
            if (usersList) {
                usersToBeCreated = usersList.length;
            }

            loadProfiles.getProfilesAmount(aps.context.vars.tenant).then(function (results) {
                profilesTools.addProfiles(
                    this, this.byId(PROFILES_LIST), PROFILE_TILE, results,
                    {additionalProfiles: usersToBeCreated, upsellDisabled: true}
                );
                updateResourcesAmount.call(this);
                deferred.resolve();
            }.bind(this));

            return deferred.then(aps.apsc.hideLoading);
        },
        onPrev: function() {
            aps.apsc.prev();
        },
        onNext: function() {
            var profileValue,
                tenant = aps.context.vars.tenant,
                selection = this.byId(PROFILES_LIST).get('selectionArray');

            if (selection.length > 0) {
                var tile = this.byId(selection[0]);
                if (tile && tile._profile) profileValue = tile._profile.aps.id;
            }

            var forProvision = [];
            if (profileValue) {
                var users = aps.biz.getResourcesToBind().users;
                if (users) {
                    var userTemplate = JSON.parse(ownCloudUserJson);
                    users.forEach(function (user) {
                        var model = getStateful(userTemplate);
                        model.aps.subscription = tenant.aps.subscription;
                        model.samples.aps.id = profileValue;
                        model.tenant.aps.id = tenant.aps.id;
                        model.user.aps.id = user.aps.id;
                        forProvision.push(model);
                    });
                }
            }
            if (forProvision) {
                aps.biz.operate({provision: forProvision}, tenant.aps.subscription);
            } else {
                aps.apsc.next();
            }
        }
    });
});
