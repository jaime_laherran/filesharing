define([
    "dojo/_base/declare",
    "dojo/when",
    "dojo/query",
    "aps/xhr",
    "aps/nav/ViewPlugin",
    "aps/FieldSet",
    "aps/Gauge",
    "aps/Output",
    "aps/ResourceStore",
    "aps/Status",
    "aps/Tile",
    "aps/confirm",
    "../tools",
    "../../common/TYPES"
], function (
    declare, when, query, xhr, ViewPlugin, FieldSet, Gauge, Output, Store,
    Status, Tile, confirm, tools, TYPES
) {

    var TILE = 'owc-ui-tile',
        STATUS = 'owc-ui-status',
        OUT_CLICK = 'owc-ui-click',
        OUT_FIELDSET = 'owc-ui-fieldset',
        OUT_PROFILE = 'owc-ui-profile',
        OUT_USAGE = 'owc-ui-usage',

        BTN_ASSIGN = 'owc-ui-assign',
        BTN_ACTIONS = 'owc-ui-actions',
        BTN_ACTIVATE = 'owc-ui-activate',
        BTN_CONFIGURE = 'owc-ui-configure',
        BTN_CHANGE_PWD = 'owc-ui-changepwd',
        BTN_LOGIN = 'owc-ui-login',
        BTN_REMOVE = 'owc-ui-remove',

        URL_OPENER_OUT = 'owc-ui-urlopener-output',
        URL_OPENER = 'owc-ui-urlopener',

        OWC_APP_ID = 'http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp';

    var ASSIGN_VIEW = OWC_APP_ID + '#user-assign',
        CONFIGURE_VIEW = OWC_APP_ID + '#user-configure',
        ACTIVATE_POPUP = OWC_APP_ID + '#activate',
        CHANGE_PWD_POPUP = OWC_APP_ID + '#change-password';

    function refresh(tile, userId, service) {

        function login(serviceaccess, tile) {
            var loginstore = new Store({
                target: "/aps/2/resources/" + serviceaccess.aps.id + "/getssoparam"
            });
            when(loginstore.get(), function(sso) {
                var opener = new Output({
                    id: URL_OPENER_OUT,
                    content: '<form action="' + sso.url + '" id="' + URL_OPENER + '" method="post" target="_blank" style="display: none"><input name="user" type="hidden" value="' + serviceaccess.owncloudusername + '"/><input name="password" type="hidden" value="' + atob(sso.pass) + '"/></form>'
                });
                tile.addChild(opener);
                query('#' + URL_OPENER)[0].submit();
                opener.destroy();
            });
        }

        function removeService(tile, serviceId) {
            confirm({
                size: 'md',
                title: _('Do you want to remove File Sharing service for __userName__?', {userName: aps.context.vars.selectedUser.displayName}),
                description: _('All uploaded files will be lost.'),
                submitLabel: _('Remove'),
                submitType: 'danger'
            }).then(function (response) {
                if (response) {
                    tile.set("isBusy", true);
                    xhr('/aps/2/resources/' + serviceId, {
                        method: 'DELETE'
                    });
                }
            });
        }

        tile.removeAll();
        if (service) {
            var profileOutput = tools.getWidget(
                Output, this.genId(OUT_PROFILE),
                {
                    gridSize: "xs-12 md-12",
                    label: _('Profile'),
                    value: service.assignedprofilename
                }
            );
            var usageOutput = tools.getWidget(
                Gauge, this.genId(OUT_USAGE),
                {
                    gridSize: "xs-6 md-6",
                    label: _('Storage Usage'),
                    value: service.userusage,
                    legend: _('__usage__ GB of __quota__ GB', {usage: service.userusage, quota: service.quota}),
                    maximum: service.quota
                }
            );
            var modalOutput = tools.getWidget(FieldSet, this.genId(OUT_FIELDSET));
            tile.addChild(modalOutput);
            modalOutput.addChild(profileOutput);
            modalOutput.addChild(usageOutput);

            var changePwdFuncion,
                isNotActivated = (service.aps.status === 'aps:activating'),
                isReady = (service.aps.status === 'aps:ready'),
                isOwnService = (aps.context.user.aps.id === service.serviceUserId);

            if (isReady || isOwnService) {
                changePwdFuncion = function() {
                    var callParams = {userApsId: service.aps.id};
                    if (isNotActivated) callParams['userLogin'] = service.owncloudusername;
                    this.nav.showPopup({
                        viewId: (isNotActivated ? ACTIVATE_POPUP : CHANGE_PWD_POPUP),
                        resourceId: null,
                        params: callParams,
                        modal: false
                    });
                }.bind(this);
            }

            var naButtons = [],
                actionItems = [];

            if (isNotActivated) {
                var removeButton = {
                    id: this.genId(BTN_REMOVE),
                    title: _('Remove Service'),
                    iconClass: 'fa-times',
                    autoBusy: false,
                    onClick: removeService.bind(this, tile, service.aps.id)
                };
                if (!isOwnService) {
                    naButtons = [removeButton];
                } else {
                    actionItems = [
                        {
                            id: this.genId(BTN_ACTIVATE),
                            title: _('Activate'),
                            type: 'warning',
                            iconClass: 'fa-flash',
                            autoBusy: false,
                            onClick: changePwdFuncion
                        },
                        {
                            type: 'separator'
                        },
                        removeButton
                    ];
                }
            }else if (isReady) {
                var configureFunc = function () {
                    this.nav.gotoView(CONFIGURE_VIEW, userId, {owcUser: service});
                }.bind(this);

                if (!isOwnService) {
                    actionItems = [
                        {
                            id: this.genId(BTN_CONFIGURE),
                            title: _('Reassign Profile'),
                            iconClass: 'fa-pencil',
                            autoBusy: false,
                            onClick: configureFunc
                        },
                        {
                            type: 'separator'
                        },
                        {
                            id: this.genId(BTN_REMOVE),
                            title: _('Remove Service'),
                            iconClass: 'fa-times',
                            autoBusy: false,
                            onClick: removeService.bind(this, tile, service.aps.id)
                        }
                    ];
                } else {
                    actionItems = [
                        {
                            id: this.genId(BTN_CONFIGURE),
                            iconClass: "fa-pencil",
                            label: _('Reassign Profile'),
                            onClick: configureFunc
                        },
                        {
                            id: this.genId(BTN_CHANGE_PWD),
                            iconClass: "fa-key",
                            label: _('Change Password'),
                            onClick: changePwdFuncion
                        },
                        {
                            id: this.genId(BTN_LOGIN),
                            iconClass: "fa-external-link",
                            label: _('Log into OwnCloud'),
                            onClick: login.bind(this, service, tile)
                        },
                        {
                            type: 'separator'
                        },
                        {
                            id: this.genId(BTN_REMOVE),
                            iconClass: 'fa-times',
                            label: _('Remove Service'),
                            onClick: removeService.bind(this, tile, service.aps.id)
                        }
                    ];
                }
            }

            var infoStatus = 'ready';
            if (service.aps.status === "aps:activating") {
                infoStatus = 'notActivated';
            }
            else if (service.aps.status === "aps:proto") {
                infoStatus = 'activating';
            }
            else if (service.aps.status === "aps:provisioning") {
                infoStatus = 'assigning';
            }
            else if (service.aps.status === "aps:unprovisioning") {
                infoStatus = 'unprovisioning';
            }
            tile.info.set('status', infoStatus);
            if (naButtons.length === 0) {
                naButtons = [
                    {
                        id: this.genId(BTN_ACTIONS),
                        iconClass: "fa-gear",
                        label: _('Actions'),
                        disabled: (actionItems.length === 0),
                        items: actionItems
                    }
                ];
            }
            tile.set('buttons', naButtons);
        } else {
            tile.info.set('status', 'ready');
            tile.set(
                'buttons',
                [{
                    id: this.genId(BTN_ASSIGN),
                    title: _('Assign Service'),
                    iconClass: 'fa-plus',
                    autoBusy: false,
                    onClick: function () {
                        this.nav.gotoView(ASSIGN_VIEW, userId);
                    }.bind(this)
                }]
            );
            var outSet = new FieldSet();
            tile.addChild(outSet);
            outSet.addChild(new Output({'id': this.genId(OUT_CLICK), 'value': _('Service is not assigned yet')}));
        }
    }

    return declare(ViewPlugin, {
        setServiceData: function (data) {
            var userId = aps.app.model.aps.id;

            var tile = tools.getWidget(
                Tile, this.genId(TILE),
                {
                    gridSize: "md-4 xs-12",
                    title: _('File Sharing'),
                      // customization
                    iconName: this.buildStaticURL('./images/tile_logo.png'),
                    fontColor: "#fff",
                    backgroundColor: "#40a79c",
                      // info
                    'info': new Status({
                        id: this.genId(STATUS),
                        useIcon: false,
                        status: 'notAssigned',
                        statusInfo: {
                            'activating':     {'label': _('Activating'),     'type': 'default', 'isLoad': true},
                            'assigning':      {'label': _('Assigning'),      'type': 'default', 'isLoad': true},
                            'notActivated':   {'label': _('Not Activated'),  'type': 'warning'},
                            'ready':          {'label': '',                  'type': 'success', 'visible': false},
                            'unprovisioning': {'label': _('Unprovisioning'), 'type': 'default', 'isLoad': true}
                        }
                    })
                }
            );

            var services = data.services,
                myService;

            if (services)
                myService = services.find(function (service) {
                    return service.aps.type === TYPES.USERS_TYPE && service.aps.subscription == this.myTenant.aps.subscription;
                }.bind(this));

            refresh.call(this, tile, userId, myService);
            return tile;
        },
        onContext: function(context) {
            this.myTenant = context.vars.tenant;
        }
    });
});
