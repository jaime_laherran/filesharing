define([
    "dojo/_base/declare",
    "aps/nav/ViewPlugin",
    "../../common/TYPES"
], function (declare, ViewPlugin, TYPES)
{
    return declare(ViewPlugin, {
        apsType: TYPES.SAMPLES_TYPE,
        resourceName: _('Profiles'),
        totalSubtitle: _('Assigned to Users'),
        entryViewId: 'service'
    });
});

