define([
    "aps/nav/ViewPlugin",
    "dojo/_base/declare",
    "../../common/TYPES"
], function (ViewPlugin, declare, TYPES) {

    return declare(ViewPlugin, {
        updateState: function (domain, state) {
            state.status = _("Not Assigned");
            state.title = _("File Sharing");
            if (domain.services && domain.services.length > 0) {
                domain.services.forEach(function (service) {
                    if (service.aps.type === TYPES.DOMAIN_TYPE) {
                        state.status = _("Assigned");
                    }
                }, this);
            }
        }
    });
});
