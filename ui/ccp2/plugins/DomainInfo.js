define([
    "dojo/_base/declare",
    "dojo/when",
    "aps/nav/ViewPlugin",
    "aps/confirm",
    "aps/FieldSet",
    "aps/Output",
    "aps/ResourceStore",
    "aps/Status",
    "aps/Tile",
    "../displayError",
    "../../common/TYPES"
], function (declare, when, ViewPlugin, confirm, FieldSet, Output, ResourceStore, Status, Tile, displayError, TYPES) {

    var DOMAIN_TILE = 'owc-di-tile',
        DOMAIN_STATUS = 'owc-di-status',
        DOMAIN_FIELDS = 'owc-di-fieldset',
        DOMAIN_OUT = 'owc-di-output',

        BTN_ASSIGN = 'owc-di-assign',
        BTN_ACTIONS = 'owc-di-actions',
        BTN_CONFIGURE = 'owc-di-configure',
        BTN_DISABLE = 'owc-di-disable',

        OWC_ID = "http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp";

    var ASSIGN_POPUP = OWC_ID + '#domain-assign',
        CONFIGURE_POPUP = OWC_ID + '#domain-configure';

    return declare(ViewPlugin, {
        setServiceData: function (data) {
            var domainData = {
                    serviceName: _("File Sharing"),
                    domainId: data.aps.id,
                    zoneId: data.zoneId,
                    domainName: data.name,
                    zone: data.zone,
                    status: _("Not Assigned")
                };

            var changeDomainPopupFunction = function(POPUP_ID) {
                this.nav.showPopup({
                    viewId: POPUP_ID,
                    resourceId: data.aps.id,
                    modal: false
                });
            };

            var tile = new Tile({
                id: this.genId(DOMAIN_TILE),
                title: domainData.serviceName,
                gridSize: "md-4 xs-12",
                  // customization
                iconName: this.buildStaticURL('./images/tile_logo.png'),
                fontColor: "#fff",
                backgroundColor: "#40a79c"
            });

            var outputDesc = new FieldSet({id: this.genId(DOMAIN_FIELDS)});
            tile.addChild(outputDesc);
            var output = new Output({id: this.genId(DOMAIN_OUT)});

            var service;
            if (data.services)
                service = data.services.find(function(service) {
                    return (service.aps.type === TYPES.DOMAIN_TYPE &&
                        service.aps.subscription == this.myTenant.aps.subscription);
                }.bind(this));

            if (!service) {
                tile.set({
                    'buttons': [
                        {
                            id: this.genId(BTN_ASSIGN),
                            title: _('Link to File Sharing'),
                            iconClass: 'fa-plus',
                            autoBusy: false,
                            onClick: changeDomainPopupFunction.bind(this, ASSIGN_POPUP)
                        }
                    ]
                });
                output.set('value', _('Enable the domain for this service'));
                outputDesc.addChild(output);
            } else {
                domainData.status = _("Assigned");
                domainData.objectforremoval = service.aps.id;
                domainData.step = "remove";

                outputDesc.addChild(
                    new Status({
                        id: this.genId(DOMAIN_STATUS),
                        label: _('Domain Status'),
                        status: 'enabled',
                        statusInfo: {
                            'enabled': {'label': _('Enabled'), 'type': 'success'}
                        }
                    }),
                    'first'
                );

                output.set({
                    'label': _('Hostname'),
                    'value': service.accessdomainprefix + '.' + service.name
                });
                outputDesc.addChild(output);

                tile.set('buttons',
                    [{
                        id: this.genId(BTN_ACTIONS),
                        label: _('Actions'),
                        iconClass: "fa-gear",
                        items: [
                            {
                                id: this.genId(BTN_CONFIGURE),
                                iconClass: "fa-pencil",
                                label: _("Configure"),
                                onClick: changeDomainPopupFunction.bind(this, CONFIGURE_POPUP)
                            },
                            {
                                type: 'separator'
                            },
                            {
                                id: this.genId(BTN_DISABLE),
                                iconClass: "fa-times",
                                label: _("Unlink"),
                                onClick: function () {
                                    var store = new ResourceStore({
                                        target: "/aps/2/resources/" + service.aps.id
                                    });
                                    when(store.get('/getprovideraccesspoint'), function (providerDomain){
                                        confirm({
                                            title: _('Disable __domainName__ for File Sharing', {domainName: service.name}),
                                            description: _('You and your employees will have access to File Sharing service by default domain "__domainName__"', {domainName: providerDomain.accessdomain}),
                                            submitLabel: _('Disable'),
                                            submitType: 'danger'
                                        }).then(function(response) {
                                            if(response){
                                                when(store.remove(), function(){
                                                }, displayError);
                                            }
                                        });
                                    }, displayError);
                                }
                            }
                        ]
                    }]
                );
            }
            return tile;
        },
        onContext: function(context) {
            this.myTenant = context.vars.tenant;
        }
    });
});
