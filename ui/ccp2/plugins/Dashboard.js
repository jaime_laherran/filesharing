define([
    "aps/nav/ViewPlugin",
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojo/Stateful",
    "dojo/when",
    "dojox/mvc/getStateful",
    "aps/Container",
    "aps/ResourceStore",
    "aps/tiles/UsageInfoTile",
    "aps/Status",
    "../displayError",
    "../loadProfiles",
    "../../common/TYPES"
], function (ViewPlugin, declare, Deferred, Stateful, when, getStateful, Container, ResourceStore, UsageInfoTile, Status, displayError, loadProfiles, TYPES)
{
    var TILE_ID = 'owc-ds-usage',
        TILE_STATUS = 'owc-ds-usage-status',

        BTN_RENEW = 'owc-ds-btn-renew',
        BTN_UPGRADE = 'owc-ds-btn-upgrade';

    function refreshStatus(tenant, billing) {
        if (tenant.billing) {
            when(loadProfiles.getBillingSubscription(tenant, billing), function(bssSubscription) {
                var widget = this.byId(TILE_ID);
                  // XXX APS-22306 long provisioned subscription and other cases
                if (!bssSubscription) {
                    widget.set({
                        'info': undefined,
                        'buttons': []
                    });
                    return;
                }

                if (bssSubscription.trial === false) {
                    switch(bssSubscription.status) {
                    case 30: // Active
                        widget.set('info', undefined);
                        break;
                    case 40: // Graced
                    case 50: // Expired
                        widget.set({
                            'info': undefined,
                            'buttons': [
                                {
                                    id: this.genId(BTN_RENEW),
                                    title: _('Renew'),
                                    iconClass: "fa-shopping-cart",
                                    type: "danger",
                                    autoBusy: false,
                                    onClick: function () {
                                        displayError("Functionality not implemented yet", "warning");
                                    }
                                }
                            ]
                        });
                        break;
                    }
                } else {
                    var expDate = ((bssSubscription.status == 40) ? bssSubscription.shutdownDate : bssSubscription.expirationDate),
                        infoLabel;

                    if (expDate) {
                        var oneDay = 24*60*60*1000,
                            today = new Date(),
                            endDate = new Date(expDate);

                        var diffDays = Math.round(Math.abs((endDate.getTime() - today.getTime())/(oneDay)));
                        infoLabel = _('__days__ Days left', {days: diffDays});
                    }

                    var statusWidget = this.byId(TILE_STATUS);
                    if (statusWidget) statusWidget.destroy();

                    var warnType = (bssSubscription.status == 15) ? 'warning' : 'danger'; // Active or not (Graced)
                    widget.set({
                        'info': new Status({id: this.genId(TILE_STATUS), useIcon: false, status: 'trial', statusInfo: {'trial': {
                            'label': infoLabel,
                            'type': warnType
                        }}}),
                        'buttons': [
                            {
                                id: this.genId(BTN_UPGRADE),
                                title: _('Upgrade to Paid'),
                                iconClass: "fa-shopping-cart",
                                type: warnType,
                                autoBusy: false,
                                onClick: function() {
                                    this.nav.gotoView("http://www.parallels.com/ccp-billing-subscription-management#upgrade-wizard", null, {
                                        subscriptionId: bssSubscription.subscriptionId,
                                        appIds: ["http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp"]
                                    });
                                }
                            }
                        ]
                    });
                }
            }.bind(this));
        }
    }

    return declare(ViewPlugin, {
        init: function (mediator) {
            var self = this;

            var deferred = new Deferred(),
                widget = new UsageInfoTile({
                    id: this.genId(TILE_ID),
                    showPie: false,
                    title: _('File Sharing'),
                    textSuffix: _('Profiles'),
                    gridSize: "md-4 xs-12",
                    onClick: function () {
                        self.nav.gotoView("http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp#service");
                    },
                      // customization
                    iconName: self.buildStaticURL('./images/tile_logo.png'),
                    fontColor: "#fff",
                    backgroundColor: "#40a79c"
                });

            mediator.getWidget = function () {
                return widget;
            };
            mediator.watch("resourceUsage", function (name, oldValue, newValue) {
                if (newValue === undefined ||
                    !self.myTenant || !(self.myTenant.aps.subscription in newValue)) return;

                var resources = newValue[self.myTenant.aps.subscription],
                    totalProfiles = 0,
                    usedProfiles = 0,
                    unlimExists  = false;

                resources.forEach(function(profile) {
                    if (profile.apsType !== TYPES.SAMPLES_TYPE) return;

                    if (profile.limit === undefined) {
                        usedProfiles += profile.usage;
                        unlimExists = true;
                    } else {
                        totalProfiles += profile.limit;
                        usedProfiles += profile.usage;
                    }
                });

                if (unlimExists) {
                    this.byId(TILE_ID).set({
                        textSecondNumber: '',
                        value: usedProfiles,
                        maximum: undefined,
                        description: _('Assigned to Users')
                    });
                } else {
                    this.byId(TILE_ID).set({
                        value: usedProfiles,
                        maximum: totalProfiles,
                        textSecondNumber: '${maximum}',
                        description: _('Assigned to Users')
                    });
                }
            }.bind(this));
            deferred.resolve();
            return deferred;
        },
        onContext: function(context) {
            this.myTenant = context.vars.tenant;
            refreshStatus.call(this, this.myTenant, context.vars.billing);
        }
    });
});

