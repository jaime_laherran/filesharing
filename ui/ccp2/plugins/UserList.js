define([
    "aps/nav/ViewPlugin",
    "dojo/_base/declare",
    "dojo/query",
    "aps/Container",
    "aps/FieldSet",
    "aps/Gauge",
    "aps/Output",
    "aps/Status",
    "../tools",
    "../../common/TYPES"
], function(ViewPlugin, declare, query, Container, FieldSet, Gauge, Output, Status, tools, TYPES) {

    var TILES_WIDGET = 'owc-ul-tiles-widget-',
        LIST_WIDGET = 'owc-ul-list-widget-',
        TILES_PROFILE = 'owc-ul-tiles-profile-',
        LIST_PROFILE = 'owc-ul-list-profile-',
        TILES_STATUS = 'owc-ul-tiles-status-',
        LIST_STATUS = 'owc-ul-list-status-',
        TILES_USAGE = 'owc-ul-tiles-usage-',
        LIST_USAGE = 'owc-ul-list-usage-';

    return declare(ViewPlugin, {
        getServicesList: function () {
            return {'title': _('File Sharing')};
        },
        getState: function(users) {
            var result = {};
            users.forEach(function(user) {
                var state = {};
                this.updateState(user, state);
                result[user.aps.id] = state;
            }, this);
            return result;
        },
        updateState: function (user, state) {
            state.status = "Not Assigned";
            state.name = state.title = _('File Sharing');
            if (!user.services || user.services.length === 0) {
                return;
            }
            user.services.forEach(function (service) {
                if (service.aps.type == TYPES.USERS_TYPE) {
                    var tileStatusId = this.genId(TILES_STATUS + service.aps.id);

                    if (service.aps.status === "aps:provisioning") {
                        state.status = "Updating";
                        state.widget = tools.getWidget(
                            Status, tileStatusId,
                            {
                                useIcon: false, status: 'updating',
                                statusInfo: {'updating': {'label': _('Updating'), 'type': 'default', isLoad: true}}
                            }
                        );
                    } else {
                        if (service.aps.status === "aps:unprovisioning") {
                            state.status = "Deleting";
                            state.widget = tools.getWidget(
                                Status, tileStatusId,
                                {
                                    useIcon: false, status: 'deleting',
                                    statusInfo: {'deleting': {'label': _('Deleting'), 'type': 'danger', isLoad: true}}
                                }
                            );
                        } else {
                            if (service.aps.status === "aps:activating") {
                                state.status = "aps:activating";
                            } else {
                                state.status = service.assignedprofilename;
                            }
                            var usage = 0;
                            if (service.userusage > 0) {
                                usage = service.userusage;
                            }

                            var tilesWidget = tools.getWidget(Container, this.genId(TILES_WIDGET + service.aps.id)),
                                listWidget = tools.getWidget(FieldSet, this.genId(LIST_WIDGET + service.aps.id));

                            if (service.aps.status !== "aps:activating") {
                                var outputForTiles = new Output({
                                    id: this.genId(TILES_PROFILE + service.aps.id),
                                    content: service.assignedprofilename
                                });
                                tilesWidget.addChild(outputForTiles);

                                var outputForList = new Output({
                                    id: this.genId(LIST_PROFILE + service.aps.id),
                                    content: service.assignedprofilename
                                });
                                listWidget.addChild(outputForList);
                            }

                            if (service.aps.status === "aps:ready") {
                                var legendText = _('__usage__ GB of __quota__ GB used', {usage: '${value}', quota: '${maximum}'}),
                                    usageInfo = {
                                        gridSize: "xs-8 md-8",
                                        value: usage,
                                        maximum: service.quota,
                                        legend: legendText
                                    };

                                var tilesUsage = tools.getWidget(
                                    Gauge, this.genId(TILES_USAGE + service.aps.id), usageInfo
                                );
                                tilesWidget.addChild(tilesUsage);

                                var usageForList = tools.getWidget(
                                    Gauge, this.genId(LIST_USAGE + service.aps.id), usageInfo
                                );
                                listWidget.addChild(usageForList);
                            } else {
                                if (service.aps.status === "aps:activating") {
                                    var statusInfo = {
                                        useIcon: false, status: 'notActivated',
                                        statusInfo: {'notActivated': {'label': _('Not Activated'), 'type': 'warning'}}
                                    };
                                    tilesWidget.addChild(tools.getWidget(Status, tileStatusId, statusInfo));
                                    listWidget.addChild(tools.getWidget(Status, this.genId(LIST_STATUS + service.aps.id), statusInfo));
                                }
                            }
                            state.widget = tilesWidget;
                            state.gridWidget = listWidget;
                        }
                    }
                }
            }, this);
        }
    });
});
