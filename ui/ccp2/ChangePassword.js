define([
    "dojo/_base/declare",
    "aps/_PopupView",
    "aps/load",
    "aps/xhr",
    "./displayError"
], function (declare, _PopupView, load, xhr, displayError) {

    var USERINFO_PANEL = 'owc-chp-panel',
        USERINFO_CNT  = 'owc-chp-container',
        USER_FIELD = 'owc-chp-username',
        PWD_FIELD  = 'owc-chp-password',
        PWD_MIN = 7,
        PWD_MAX = 64;

    var loadPromise;

    return declare(_PopupView, {
        size: 'sm',
        optionSelected: 'none',
        init: function () {
            loadPromise = load([
                ["aps/Panel", {
                    id: this.genId(USERINFO_PANEL)
                }, [
                    ["aps/FieldSet", {id: this.genId(USERINFO_CNT)}, [
                        ["aps/Output", {
                            id: this.genId(USER_FIELD),
                            label: _("Username"),
                            visible: false
                        }],
                        ["aps/Password", {
                            id: this.genId(PWD_FIELD),
                            autoSize: false,
                            gridSize: 'xs-12 md-6',
                            label: _('New Password'),
                            showStrengthIndicator: true,
                            required: true,
                            customValidator: function(value) {
                                if (value === null || value.length > PWD_MAX) return false;
                                return true;
                            }
                        }]
                    ]]
                ]]
            ], this.domNode);

            loadPromise.then(function() {
                var widget = this.byId(PWD_FIELD);
                widget.watch('value', function () {
                    aps.apsc.buttonState("submit", {disabled: (!arguments[2]  || arguments[2].length === 0 || arguments[2].length < PWD_MIN)});
                });
            }.bind(this));
        },
        onContext: function() {
            var params = aps.context.params;

            loadPromise.then(function() {
                var pwdLabel = _('New Password'),
                    loginWidget = this.byId(USER_FIELD);

                if ('userLogin' in params && params.userLogin) {
                    loginWidget.set({
                        'value'  : params.userLogin,
                        'visible': true
                    });
                    pwdLabel = _('Password');
                } else {
                    loginWidget.set('visible', false);
                }

                var pwdWidget = this.byId(PWD_FIELD);
                pwdWidget.set({
                    'label': pwdLabel,
                    'value': ''
                });
                pwdWidget.focus();

                aps.apsc.buttonState("submit", {disabled: true});
                aps.apsc.hideLoading();
            }.bind(this));
        },
        onSubmit: function() {
            xhr('/aps/2/resources/' + aps.context.params.userApsId + '/changepassword', {
                method: 'PUT',
                headers: {
                    "Content-Type": 'application/json'
                },
                data: JSON.stringify({owncloudpassword: this.byId(PWD_FIELD).get('value')})
            }).then(function (data) {
                this.submit({service: data});
            }.bind(this), displayError);
        },
        onHide: function() {
            var loginWidget = this.byId(USER_FIELD);
            if (loginWidget) loginWidget.set('visible', false);
        }
    });
});
