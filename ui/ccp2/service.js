define([
    "dojo/_base/declare",
    "dojo/promise/all",
    "dojo/query",
    "dojo/when",
    "dijit/registry",
    "aps/_View",
    "aps/Button",
    "aps/DropDownButton",
    "aps/confirm",
    "aps/Gauge",
    "aps/Memory",
    "aps/Message",
    "aps/Output",
    "aps/ResourceStore",
    "aps/Status",
    "aps/xhr",
    "./loadProfiles",
    "./displayError",
    "../common/TYPES"
],
    function(
        declare,
        all, query, when, registry, _View, Button, DropDownButton, confirm, Gauge, Memory, Message, Output,
        Store, Status, xhr, loadProfiles, displayError,
        TYPES
    )
{

    var OWC_TILES_MYFS = 'owc-tilesOfFileSharing',
        OWC_MSG_ASSIGN = 'owc-svc-msg',
        OWC_MSG_OUT = 'owc-svc-msgout',
        OWC_MSG_BTN = 'owc-svc-msgbtn',
        OWC_TILE = 'owc-login-tile',
        OWC_TILE_FIELDSET = 'owc-login-fieldset',
        OWC_SERVER = 'owc-serverAddress',
        OWC_USERNAME = 'owc-myUserNameOutput',
        OWC_PROFILE = 'owc-myAssignedProfile',
        OWC_USAGE = 'owc-mystorageplaceholder',
        OWC_USAGE_GAUGE = 'owc-MyGaugeUsage',
        APPS_TILE = 'owc-desktopClient',
        PROFILES_TILE = 'owc-svc-profiles-tile',
        PROFILES_TILE_INFO = 'owc-svc-profiles-tile-info',
        DOMAIN_TILE = 'owc-svc-domains-tile',
        DOMAIN_OUT = 'owc-domains-output',

        BTN_DOWNLOAD = "owc-svc-btn-download",
        BTN_DOWNLOAD_CURRENT = "owc-svc-btn-download-current",
        BTN_DOWNLOAD_OTHER = "owc-svc-btn-download-other",
        BTN_ACTIVATE = "owc-svc-btn-activate",
        BTN_LOGIN = "owc-svc-btn-login",
        BTN_CHANGE_PWD = "owc-svc-btn-changepwd",
        BTN_NEW_DOMAIN = "owc-svc-btn-newdomain",
        BTN_MNG_DOMAIN = "owc-svc-btn-mngdomain",
        BTN_MNG_PROFILES = "owc-svc-btn-profiles",

        USERS_GRID_CONTAINER = 'owc-users-cont',
        USERS_GRID = 'owc-users-grid',
        USERS_GRID_TOOLBAR = 'owc-users-toolbar',
        USERS_NAME = 'owc-users-name-',
        USERS_STATUS = 'owc-users-status-',
        USERS_PROFILE = 'owc-users-profile-',
        USERS_USAGE = "owc-users-gauge-",

        USERS_ACTIONS = "owc-users-actions-",
        USERS_ASSIGN = "owc-users-assign-",
        USERS_ACTIVATE = "owc-users-activate-",
        USERS_EDIT = "owc-users-edit-",
        USERS_CHANGEPWD = "owc-users-changepwd-",
        USERS_LOGIN = "owc-users-login-",
        USERS_VIEW = "owc-users-view-",
        USERS_SEP = "owc-users-sep-",
        USERS_REMOVE = "owc-users-remove-",

        ASSIGN_VIEW = "user-assign",
        CONFIGURE_VIEW = "user-configure",
        ACTIVATE_POPUP = "activate",
        CHANGE_PWD_POPUP = "change-password",
        USERS_APP = "http://www.parallels.com/ccp-users#";

    var ADD_USERS_VIEW_ID = USERS_APP + "addUser",
        USER_VIEW_ID = USERS_APP + "viewUser",
        DOMAINS_LIST = "http://www.parallels.com/ccp-domains#domains",
        DOMAIN_VIEW = "http://www.parallels.com/ccp-domains#viewDomain";

    var userStatusInfo = {
            "aps:proto": {label: '', "type": 'default'},
            "aps:provisioning": {label: _('Assigning'), "type": 'default', isLoad: true},
            "aps:ready": {label: _('Ready'), "type": 'success'},
            "aps:unprovisioning": {label: '', "type": 'warning'},
            "aps:deleted": {label: '', "type": 'danger'},
            "none": {label: _('Not Assigned'), "type": "disabled" },
            "aps:activating": {label: _('Not Activated'), "type": "warning" }
        };


        return declare(_View, {
            init: function() {
                function deleteServices() {
                    var grid = registry.byId(USERS_GRID);
                    var selectedUsers = grid.get('selectionArray'),
                        store = grid.get('store');
                    confirm({
                        title:
                            selectedUsers.length > 1 ?
                                _('Do you want to remove File Sharing service for __count__ users?', {count: selectedUsers.length})
                            :
                                _("Do you want to remove File Sharing for this user?"),
                        description: _('All uploaded files will be lost.'),
                        submitType: 'danger',
                        submitLabel: _('Remove'),
                        submitIcon: 'fa-check'
                    }).then(function (response) {
                        if (!response) {
                            aps.apsc.cancelProcessing();
                            return;
                        }
                        aps.apsc.showLoading();

                        var deferreds = [];
                        selectedUsers.forEach(function (userId) {
                            when(store.get(userId), function(userData) {
                                if (!(userData && userData.owcUser && userData.owcUser.assignedprofile && userData.owcUser.aps.id)) return;
                                deferreds.push(
                                    xhr('/aps/2/resources/' + userData.owcUser.aps.id, {
                                        method: 'DELETE'
                                    }).then(
                                        function () {},
                                        function (err) {
                                            if (err && err.response && err.response.data) {
                                                var data = err.response.data instanceof Object ? err.response.data : JSON.parse(err.response.data);
                                                displayError(_(data.message, data.details || {}, data.pkgId));
                                            } else {
                                                displayError(_("Failed to delete File Sharing service for user with error \"__msg__\"", {msg: err}));
                                            }
                                            aps.apsc.hideLoading();
                                        }
                                    )
                                );
                            });
                        });

                        all(deferreds).then(function () {
                            grid.set('selectionArray', []);
                            aps.apsc.hideLoading();
                        });
                    });
                }
                return [
                        ["aps/Output", {
                            id: 'owc-Welcome',
                            content: _('Welcome to ownCloud, the service provides access to your data through web interface while providing a platform to easily view, sync and share accross devices.'),
                            'class': 'lead'
                        }], // Close of Welcome Output
                        ["aps/Tiles", {
                            id: OWC_TILES_MYFS,
                            title: _('My File Sharing')
                        }, [
                            ["aps/Tile", {
                                id: OWC_TILE,
                                gridSize: "md-8 xs-12",
                                title: _('File Sharing Account'),
                                'info': new Status({
                                    useIcon: false,
                                    id: "owc-status-user",
                                    status: 'notAssigned',
                                    statusInfo: {
                                        'assigning':    {'label': _('Assigning'),     'type': 'default', 'isLoad': true},
                                        'notActivated': {'label': _('Not Activated'), 'type': 'warning'},
                                        'notAssigned':  {'label': _('Not Assigned'),  'type': 'default'},
                                        'ready':        {'label': '',                 'type': 'success', 'visible': false}
                                    }
                                })
                            }, [
                                ["aps/FieldSet", {
                                    id: OWC_TILE_FIELDSET
                                }, [
                                    ["aps/Output",{
                                        id: OWC_SERVER,
                                        label: _("Server Address"),
                                        gridSize: "md-6 xs-12"
                                    }],
                                    ["aps/Output",{
                                        id: OWC_USERNAME,
                                        label: _('My User Name'),
                                        value: _('Service not Assigned'),
                                        gridSize: "md-6 xs-12"
                                    }],
                                    ["aps/Output",{
                                        id: OWC_PROFILE,
                                        label: _('My Profile'),
                                        value: _('Not Assigned'),
                                        gridSize: "md-6 xs-12"
                                    }],
                                    ["aps/Output",{
                                        id: OWC_USAGE,
                                        label: _('My Storage Usage'),
                                        value: _('None'),
                                        gridSize: "md-6 xs-12"
                                    }]
                                ]]
                            ]],
                            ["aps/Tile", {
                                    id: APPS_TILE,
                                    title: _('Apps'),
                                    gridSize: "md-4 xs-12",
                                    buttons: [{
                                        id: BTN_DOWNLOAD,
                                        label: _('Download App'),
                                        iconClass: 'fa-gear',
                                        items: [
                                            {
                                                id: BTN_DOWNLOAD_CURRENT,
                                                iconClass: 'fa-download',
                                                visible: false
                                            },
                                            {
                                                type: 'separator'
                                            },
                                            {
                                                id: BTN_DOWNLOAD_OTHER,
                                                label: _('Other Platforms'),
                                                iconClass: 'fa-download',
                                                onClick: function() {
                                                    aps.apsc.gotoView("download");
                                                }
                                            }
                                        ]
                                    }]
                                },[["aps/Container", [
                                    ["aps/Output", {
                                        id: "owc-output-download-apps",
                                        content: _('Download apps for Mac OSX,<br>Windows, iOS, Android and other<br>platforms')
                                    }]
                                ]]
                                ]
                            ]
                        ]],
                        ["aps/Tiles", {
                            id: "owc-tilesAdminOfFileSharing",
                            title: _('Administration')
                        }, [
                            ["aps/Tile", {
                                    id: PROFILES_TILE,
                                    gridSize: "md-4 xs-12",
                                    title: _('Profiles'),
                                    autoBusy: false,
                                    buttons: [
                                        {
                                            id: BTN_MNG_PROFILES,
                                            label: _('Manage Profiles'),
                                            iconClass: 'fa-pencil',
                                            visible: true,
                                            autoBusy: false,
                                            onClick: function() {
                                                aps.apsc.gotoView("profiles");
                                            }
                                        }
                                    ]
                                },[
                                    ["aps/UsageInfo",{
                                        id: PROFILES_TILE_INFO,
                                        gridSize: "xs-12 md-12"
                                    }]
                                ]
                            ],
                            ["aps/Tile", {
                                    id: DOMAIN_TILE,
                                    gridSize: "md-4 xs-12",
                                    title: _('Linked Domain'),
                                    'buttons': [
                                        {
                                            id: BTN_NEW_DOMAIN,
                                            title: _('Link Domain'),
                                            iconClass: "fa-plus",
                                            autoBusy: false,
                                            visible: false,
                                            onClick: function() {
                                                aps.apsc.gotoView(DOMAINS_LIST);
                                            }
                                        },
                                        {
                                            id: BTN_MNG_DOMAIN,
                                            title: _('Manage Domain'),
                                            iconClass: "fa-pencil",
                                            autoBusy: false,
                                            visible: false
                                        }
                                    ]
                                },[["aps/Output", {
                                    id: DOMAIN_OUT,
                                    gridSize: "md-12 xs-12",
                                    value: _('No Linked Domain')
                                }]]
                            ]
                        ]],// Close of Tiles
                          // Users list container
                        ["aps/Container", {id: USERS_GRID_CONTAINER, title: _('File Sharing Users'), gridSize: "md-12 xs-12"}, [
                            ["aps/Grid", {
                                id: USERS_GRID,
                                apsResourceViewId: USER_VIEW_ID,
                                selectionMode: "multiple"
                            }, [
                                ["aps/Toolbar", {id: USERS_GRID_TOOLBAR}, [
                                    ["aps/Button", {
                                        id: 'owc-add-new-users',
                                        autoBusy: false,
                                        label: _("Add New Users"),
                                        iconClass: "fa-plus",
                                        isDefault: true,
                                        onClick: function () {
                                            aps.apsc.gotoView(ADD_USERS_VIEW_ID);
                                        }
                                    }],
                                    ["aps/Button", {
                                        id: 'owc-del-users',
                                        autoBusy: false,
                                        label: _("Remove Service"),
                                        iconClass: "fa-times",
                                        isDefault: false,
                                        requireItems: true,
                                        type: "danger",
                                        onClick: function () {
                                            deleteServices();
                                        }
                                    }]
                                ]]
                            ]]
                        ]],
                        // Form is created in order to have SSO Experience
                        ["<form>", {
                                id: "owc-urlOpener",
                                action: "nourl",
                                method: "post",
                                target: "_blank",
                                "style": "display: none"
                            },
                            [
                                ["<input>", {
                                    name: "user",
                                    id: "user",
                                    type: "hidden"
                                }],
                                ["<input>", {
                                    name: "password",
                                    id: "password",
                                    type: "hidden"
                                }],
                                ["<input>", {
                                    name: "owncloudhttp",
                                    id: "owncloudhttp",
                                    type: "hidden"
                                }]
                            ]
                        ] // Close of hidden Form
                ];
            },

            onShow: function() {
                function detectOs() {
                    var myOS, myOSName;

                    if (navigator.platform.indexOf("Win") != -1) {
                        myOS = "win";
                        myOSName = _('Windows');
                    } else if (navigator.platform.indexOf("Mac") != -1) {
                        myOS = "mac";
                        myOSName = _('Mac OS X');
                    } else if (navigator.platform.indexOf("Linux") != -1) {
                        myOS = "linux";
                        myOSName = _('Linux');
                    } else if (navigator.platform.indexOf("iPhone") != -1 ||
                               navigator.platform.indexOf("iPod") != -1 ||
                               navigator.platform.indexOf("iPad") != -1) {
                        myOS = "ios";
                        myOSName = _('iPhoneApp');
                    } else if (navigator.platform.indexOf("android") != -1) {
                        myOS = "android";
                        myOSName = _('Android App');
                    }

                    return [myOS, myOSName];
                }

                var osInfo = detectOs();
                if (osInfo[1]) {
                    registry.byId(BTN_DOWNLOAD_CURRENT).set({
                        title: osInfo[1],
                        visible: true,
                        onClick: function() {
                            var url = aps.context.vars.globals[osInfo[0] + "url"];
                            window.open(url, "_blank");
                        }
                    });
                }
            },

            onContext: function() {
                aps.app.model.visibleProfiles = 0;
                var myUsers = new Memory({idProperty: "aps.id"}),
                    myCols = [],
                    store = new Store({
                        target: "/aps/2/resources/"
                    }),
                    domainsStore = new Store({
                        target: "/aps/2/resources/" + aps.context.vars.tenant.aps.id + "/domain"
                    });

                function recalculateProfilesTile(profiles) {
                    var used = 0,
                        limit = 0,
                        unlimited = false;

                    profiles.forEach(function (profile) {
                        used = used + profile.usage;
                        if (!unlimited) {
                            if (profile.limit === undefined) {
                                unlimited = true;
                            } else {
                                limit += profile.limit;
                            }
                        }
                    });
                    var profilesInfo = registry.byId(PROFILES_TILE_INFO);

                    if (unlimited) {
                        profilesInfo.set({
                            textSecondNumber: '',
                            description: _('Profiles Used'),
                            maximum: undefined,
                            value: used,
                            showPie: false
                        });
                   } else {
                        profilesInfo.set({
                            minimum: 0,
                            maximum: limit,
                            value: used,
                            textSecondNumber: '${maximum}',
                            description: _('Assigned To Users'),
                            showPie: true
                        });
                    }
                }

                function loginIntoOwc(owcUserId, owcLogin) {
                    var ssostore = new Store({
                        target: "/aps/2/resources/" + owcUserId + "/getssoparam"
                    });
                    ssostore.query().then(function (ssoData) {
                        query("#user")[0].value = owcLogin;
                        query("#password")[0].value = atob(ssoData.pass);
                        var urlOpener = query("#owc-urlOpener")[0];
                        urlOpener.action = ssoData.url;
                        urlOpener.value = ssoData.url;
                        urlOpener.submit();
                    });
                }

                function drawLogin(userId) {
                    if (aps.context.user.aps.id != userId) return;

                    var tileWidget = registry.byId(OWC_TILE);
                    when(myUsers.get(userId), function(userData) {
                        var owcUser = userData.owcUser;
                        if (owcUser) {
                            var oldMessage = registry.byId(OWC_MSG_ASSIGN);
                            if (oldMessage) oldMessage.destroy();

                            registry.byId(OWC_TILES_MYFS).set('visible', true);
                            registry.byId(OWC_PROFILE).set(
                                "value",
                                _('__name__ (__quota__ GB)', {
                                    name: owcUser.assignedprofilename,
                                    quota: owcUser.quota
                                })
                            );
                            registry.byId(OWC_USERNAME).set("value", owcUser.owncloudusername);
                            var usageGauge = registry.byId(OWC_USAGE_GAUGE);
                            if (usageGauge) {
                                usageGauge.set({
                                    maximum: owcUser.quota,
                                    value: owcUser.userusage
                                });
                            } else {
                                registry.byId(OWC_USAGE).destroy();
                                registry.byId(OWC_TILE_FIELDSET).addChild(new Gauge({
                                    label: _('My Storage Usage'),
                                    id: OWC_USAGE_GAUGE,
                                    legend: _('__used__ GB of __quota__ GB', {used: '${value}', quota: '${maximum}'}),
                                    value: owcUser.userusage,
                                    maximum: owcUser.quota,
                                    gridSize: "md-6 xs-12"
                                }));
                            }
                            if (owcUser.aps.status==="aps:ready") {

                                tileWidget.info.set('status', 'ready');
                                tileWidget.set(
                                    'buttons', [
                                        {
                                            id: BTN_LOGIN,
                                            title: _('Log into ownCloud'),
                                            iconClass: "fa-external-link",
                                            autoBusy: false,
                                            onClick: function () { loginIntoOwc(owcUser.aps.id, owcUser.owncloudusername); }
                                        },
                                        {
                                            id: BTN_CHANGE_PWD,
                                            title: _('Change Password'),
                                            iconClass: "fa-key",
                                            autoBusy: false,
                                            onClick: function () {
                                                aps.apsc.showPopup({
                                                    viewId: CHANGE_PWD_POPUP,
                                                    resourceId: null,
                                                    params: {
                                                        userApsId: owcUser.aps.id
                                                    },
                                                    modal: false
                                                });
                                            }
                                        }
                                    ]
                                );
                            } else if (owcUser.aps.status === "aps:provisioning") {
                                tileWidget.info.set('status', 'assigning');
                                tileWidget.set(
                                    'buttons', [
                                        {
                                            id: BTN_ACTIVATE,
                                            title: _('Activate'),
                                            type: 'warning',
                                            iconClass: "fa-flash",
                                            onClick: undefined,
                                            disabled: true
                                        }
                                    ]
                                );
                            } else if (owcUser.aps.status === "aps:activating") {
                                tileWidget.info.set('status', 'notActivated');
                                tileWidget.set(
                                    'buttons', [
                                        {
                                            id: BTN_ACTIVATE,
                                            title: _('Activate'),
                                            type: 'warning',
                                            iconClass: "fa-flash",
                                            autoBusy: false,
                                            onClick: function () {
                                                aps.apsc.showPopup({
                                                    viewId: ACTIVATE_POPUP,
                                                    resourceId: null,
                                                    params: {
                                                        userApsId: owcUser.aps.id,
                                                        userLogin: owcUser.owncloudusername
                                                    },
                                                    modal: false
                                                });
                                            }
                                        }
                                    ]
                                );
                            }
                        } else {
                              // we don't recreate existing message - it can
                              // be hidden by user
                            if (!registry.byId(OWC_MSG_ASSIGN)) {
                                var assignMessage = new Message({
                                    id: OWC_MSG_ASSIGN,
                                    type: 'warning'
                                });
                                assignMessage.addChild(new Output({
                                    id: OWC_MSG_OUT,
                                    value: _('Warning! Assign the File Sharing service to yourself to start using it.')
                                }));
                                assignMessage.addChild(new Button({
                                    id: OWC_MSG_BTN,
                                    label: _('Assign'),
                                    iconClass: 'fa-plus',
                                    type: 'warning',
                                    autoBusy: false,
                                    onClick: function () {
                                        this.nav.gotoView(ASSIGN_VIEW, userId);
                                    }.bind(this)
                                }));
                                registry.byId("apsPageContainer").get("messageList").addChild(assignMessage);
                            }

                            registry.byId(OWC_TILES_MYFS).set('visible', false);
                        }
                    }.bind(this));
                }

                function updateDomainInfo(domains) {
                    var outWidget = registry.byId(DOMAIN_OUT),
                        newDomainButton = registry.byId(BTN_NEW_DOMAIN),
                        mngDomainButton = registry.byId(BTN_MNG_DOMAIN);
                    if (domains.length === 0) {
                        outWidget.set("value", _('No Linked Domain'));
                        newDomainButton.set('visible', true);
                        mngDomainButton.set('visible', false);
                    } else {
                        var owcDomain = domains[0];
                        outWidget.set(
                            "value", owcDomain.accessdomainprefix + '.' + owcDomain.name
                        );
                        newDomainButton.set('visible', false);
                        mngDomainButton.set({
                            'visible': true,
                            'onClick': function() {
                                aps.apsc.gotoView("http://www.parallels.com/ccp-domains#domains");
                            }
                        });

                        store.get(owcDomain.aps.id).then(function (domainInfo) {
                            mngDomainButton.set(
                                'onClick',
                                function() {
                                    aps.apsc.gotoView(DOMAIN_VIEW, domainInfo.domain.aps.id);
                                }
                            );
                        });
                    }
                }

                function fillUsersList(usersList) {
                    var subscriptionId = aps.context.vars.tenant.aps.subscription;

                    usersList.forEach(function (nextUser) {
                        var owcUser = nextUser.services.find(function(service) { return service.aps.type == TYPES.USERS_TYPE; });
                        if (owcUser && owcUser.aps.subscription == subscriptionId) nextUser.owcUser = owcUser;
                        delete nextUser.services;
                        if (aps.context.user.aps.id == nextUser.aps.id) {
                            when(myUsers.put(nextUser), drawLogin.bind(this, nextUser.aps.id));
                        } else {
                            myUsers.put(nextUser);
                        }
                    }.bind(this));

                    myCols.push(
                        {
                            field: "displayName",
                            name: _("User"),
                            type: 'resourceName',
                            renderCell: function(row) {
                                var profileWidget = registry.byId(USERS_NAME + row.aps.id);
                                if (profileWidget) {
                                    return profileWidget;
                                } else {
                                    return new Output({
                                        id: USERS_NAME + row.aps.id,
                                        value: row.displayName
                                    });
                                }
                            }
                        },
                        {
                            field: "owcUser.aps.status",
                            name: _("File Sharing Status"),
                            sortable: 'true',
                            renderCell: function (row, data) {
                                var currentStatus = (row.owcUser && data) ? data : "none",
                                    serviceStatus = registry.byId(USERS_STATUS + row.aps.id);
                                if (serviceStatus) {
                                    serviceStatus.set("status", currentStatus);
                                } else {
                                    serviceStatus = new Status({
                                        id: USERS_STATUS + row.aps.id,
                                        status: currentStatus,
                                        statusInfo: userStatusInfo
                                    });
                                }
                                return serviceStatus;
                            }
                        }
                    );
                      // Behaivour of grid is to show NAME - Assigned Profile - Gauge
                    var assignedProfileForGrid = {
                        field: "owcUser.assignedprofilename",
                        id: "owc-assignedProfileToUser",
                        name: _('Assigned Profile'),
                        renderCell: function(row, data) {
                            var owcUser = row.owcUser,
                                profileText,
                                profileOut = registry.byId(USERS_PROFILE + row.aps.id);

                            if (owcUser && owcUser.quota > 0) {
                                profileText = _('__profile_name__ (__quota__ GB)', {profile_name: data, quota: owcUser.quota});
                            }

                            if (profileOut) {
                                profileOut.set("value", profileText);
                                return profileOut;
                            } else {
                                return new Output({
                                    id: USERS_PROFILE+ row.aps.id,
                                    value: profileText
                                });
                            }
                        }
                    };
                    myCols.push(assignedProfileForGrid);

                    var gaugeColumn = {
                        field: "owcUser.quota",
                        id: "owc-GaugeOnGrid",
                        name: _("Storage Usage"),
                        renderCell: function(row) {
                            var owcGauge = registry.byId(USERS_USAGE + row.aps.id),
                                owcUser = row.owcUser;

                            if (owcUser && owcUser.quota > 0) {
                                if (!owcGauge) {
                                    owcGauge = new Gauge({
                                        id: USERS_USAGE + row.aps.id,
                                        gridSize: "xs-12 md-12"
                                    });
                                }
                                owcGauge.set({
                                    legend: _('${value} GB used of ${maximum} GB'),
                                    minimum: 0,
                                    maximum: owcUser.quota,
                                    value: owcUser.userusage
                                });
                                return owcGauge;
                            } else {
                                if (owcGauge) owcGauge.destroy();
                                return;
                            }
                        }
                    };
                    myCols.push(gaugeColumn);

                    var actionsColumn = {
                        field: "owcUser.aps.id",
                        id: "owc-Actions",
                        renderCell: function(row) {
                            var owcUser = row.owcUser;

                            var actionsButton = registry.byId(USERS_ACTIONS + row.aps.id),
                                isAssigned = (owcUser && owcUser.assignedprofile),
                                isEnabled = (!owcUser || (owcUser.aps.status !== "aps:provisioning" && owcUser.aps.status !== "aps:unprovisioning")),
                                isOwnService = (aps.context.user.aps.id == row.aps.id);

                            var needActivate = (isAssigned && owcUser.aps.status == "aps:activating");

                            if (actionsButton) {
                                registry.byId(USERS_ASSIGN + row.aps.id).set(
                                    'visible', !isAssigned && isEnabled
                                );
                                registry.byId(USERS_ACTIVATE + row.aps.id).set(
                                    'visible', isOwnService && needActivate && isEnabled
                                );
                                registry.byId(USERS_EDIT + row.aps.id).set(
                                    'visible', isAssigned && !needActivate && isEnabled
                                );
                                registry.byId(USERS_CHANGEPWD + row.aps.id).set(
                                    'visible', isAssigned && !needActivate && isOwnService && isEnabled
                                );
                                if (isOwnService) {
                                    registry.byId(USERS_LOGIN + row.aps.id).set(
                                        'visible', isAssigned && !needActivate && isOwnService && isEnabled
                                    );
                                }
                                registry.byId(USERS_SEP + row.aps.id).set(
                                    'visible', isAssigned && isEnabled
                                );
                                registry.byId(USERS_REMOVE + row.aps.id).set(
                                    'visible', isAssigned && isEnabled
                                );
                                actionsButton.set('disabled', !isEnabled);
                            } else {
                                var actionItems = [
                                    {
                                        id: USERS_ASSIGN + row.aps.id,
                                        label: _("Assign Service"),
                                        iconClass: 'fa-plus',
                                        visible: !isAssigned && isEnabled,
                                        onClick: function () {
                                            this.nav.gotoView(ASSIGN_VIEW, row.aps.id);
                                        }.bind(this)
                                    },
                                    {
                                        id: USERS_ACTIVATE + row.aps.id,
                                        label: _("Activate"),
                                        type: 'warning',
                                        iconClass: "fa-flash",
                                        visible: isOwnService && needActivate && isEnabled,
                                        onClick: function () {
                                            aps.apsc.showPopup({
                                                viewId: ACTIVATE_POPUP,
                                                resourceId: null,
                                                params: {
                                                    userApsId: owcUser.aps.id,
                                                    userLogin: owcUser.owncloudusername
                                                },
                                                modal: false
                                            });
                                        }
                                    },
                                    {
                                        id: USERS_EDIT + row.aps.id,
                                        label: _("Reassign Profile"),
                                        iconClass: "fa-pencil",
                                        visible: isAssigned && !needActivate && isEnabled,
                                        onClick: function () {
                                            this.nav.gotoView(
                                                CONFIGURE_VIEW,
                                                row.aps.id,
                                                {
                                                    owcUser: {
                                                        aps: {id: owcUser.aps.id},
                                                        assignedprofile: owcUser.assignedprofile,
                                                        serviceUserId: row.aps.id,
                                                        owncloudusername: owcUser.owncloudusername
                                                    }
                                                }
                                            );
                                        }.bind(this)
                                    },
                                    {
                                        id: USERS_CHANGEPWD + row.aps.id,
                                        label: _("Change Password"),
                                        iconClass: "fa-key",
                                        visible: isAssigned && !needActivate && isOwnService && isEnabled,
                                        onClick: function () {
                                            aps.apsc.showPopup({
                                                viewId: CHANGE_PWD_POPUP,
                                                resourceId: null,
                                                params: {
                                                    userApsId: owcUser.aps.id
                                                },
                                                modal: false
                                            });
                                        }
                                    },
                                    {
                                        id: USERS_VIEW + row.aps.id,
                                        label: _("View User"),
                                        iconClass: "fa-user",
                                        onClick: function () {
                                            aps.apsc.gotoView(USER_VIEW_ID, row.aps.id);
                                        }
                                    },
                                    {
                                        id: USERS_SEP + row.aps.id,
                                        type: 'separator',
                                        visible: isAssigned
                                    },
                                    {
                                        id: USERS_REMOVE + row.aps.id,
                                        label: _("Remove Service"),
                                        iconClass: "fa-times",
                                        visible: isAssigned && isEnabled,
                                        onClick: function () {
                                            confirm({
                                                size: 'md',
                                                title: _('Do you want to remove File Sharing service for __userName__?', {userName: row.displayName}),
                                                description: _('All uploaded files will be lost.'),
                                                submitLabel: _('Remove'),
                                                submitType: 'danger'
                                            }).then(function(response) {
                                                if (response) {
                                                    xhr('/aps/2/resources/' + owcUser.aps.id, {
                                                        method: 'DELETE'
                                                    });
                                                }
                                            });
                                        }
                                    }
                                ];

                                if (isOwnService) {
                                    actionItems.splice(
                                        4, 0, {
                                            id: USERS_LOGIN + row.aps.id,
                                            label: _("Log into OwnCloud"),
                                            iconClass: "fa-external-link",
                                            visible: isAssigned && !needActivate && isOwnService,
                                            onClick: function () { loginIntoOwc(owcUser.aps.id, owcUser.owncloudusername); }
                                        }
                                    );
                                }

                                actionsButton = new DropDownButton({
                                    id: USERS_ACTIONS + row.aps.id,
                                    label: _("Actions"),
                                    iconClass: "fa-gear",
                                    disabled: !isEnabled,
                                    items: actionItems
                                });
                            }
                            return actionsButton;
                        }.bind(this)
                    };
                    myCols.push(actionsColumn);

                    var owcUsersGrid = registry.byId(USERS_GRID);
                    if (owcUsersGrid) {
                        owcUsersGrid.set({
                            columns: myCols,
                            store:   myUsers
                        });
                    }
                }

                registry.byId(OWC_SERVER).set("value", aps.context.vars.tenant.accessdomain);

                store.query('implementing(http://aps-standard.org/types/core/user/1.0),sort(displayName),select(services)')
                .then(function (users) {
                    fillUsersList.call(this, users);
                    aps.apsc.hideLoading();
                }.bind(this));

                loadProfiles.load(aps.context.vars.tenant).then(recalculateProfilesTile);

                domainsStore.query().then(updateDomainInfo);

            },

            onHide: function() {}

        });
});
