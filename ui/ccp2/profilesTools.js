define([
    "dojox/mvc/getStateful",
    "aps/Tile",
    "aps/tiles/UsageInfoTile"
], function (getStateful, Tile, UsageInfoTile) {

    return {
        addProfiles: function(self, profilesList, tilePrefix, profiles, options) {
            var profilesCount = 0,
                disabledProfiles = 0,
                selectedId,
                profileNeedUpsell = false;

            profilesList.removeAll();

            for (var i = 0; i < profiles.length; ++i) {
                var nextProfile = profiles[i],
                    needUpsell = false;

                if (!nextProfile || (nextProfile.max === 0 && nextProfile.usage === 0)) continue;

                ++profilesCount;

                var availableText,
                    noAvailable = false,
                    existingAmount = 0;

                if ('used' in options && nextProfile.aps.id in options.used) existingAmount = options.used[nextProfile.aps.id];

                if ((nextProfile.amount === -1) || nextProfile.amount === undefined) {
                    availableText = _('Unlimited Available', self);
                } else {
                    if ((nextProfile.usage - existingAmount) < nextProfile.amount) {
                        availableText = _('__available__ Profiles Available', {available: (nextProfile.amount - nextProfile.usage + existingAmount)}, {}, self);
                    } else {
                        availableText = _('No Profiles Available', self);
                        noAvailable = true;
                    }
                }

                var imDisabled = false,
                    dueAmount = false,
                    amountDue = 0,
                    priceString = '',
                    additionalProfiles = 1;

                if (options.additional) additionalProfiles = options.additional;

                if (nextProfile.amount !== undefined) {
                    var newAmount = nextProfile.usage + additionalProfiles - existingAmount;
                    if (newAmount > nextProfile.amount) {
                        if (options.upsellDisabled ||
                            (nextProfile.max >= 0 && newAmount > nextProfile.max))
                        {
                            imDisabled = true;
                            dueAmount = true;
                        } else {
                            amountDue = newAmount - nextProfile.amount;
                            priceString = _(
                                '$__price__/month for __amount__ Additional Profiles',
                                {amount: amountDue, price: (amountDue * nextProfile.price.amount)},
                                {}, self
                            );
                            needUpsell = true;
                        }
                    }
                }

                if (imDisabled) ++disabledProfiles;
                nextProfile.amountDue = amountDue;

                if (dueAmount === true && !noAvailable) {
                    availableText = _('__available__ / __comment__', {available: availableText, comment: _("can not be used for assigning it to the __count__ new users", {count: additionalProfiles})}, {}, self);
                }

                var nextButtonId = self.genId(tilePrefix + nextProfile.aps.id),
                    imChecked = false;

                if (!imDisabled && (!selectedId || (options.selectedId && nextProfile.aps.id == options.selectedId))) {
                    imChecked = true;
                    selectedId = nextButtonId;
                    profileNeedUpsell = needUpsell;
                }
                nextProfile.needUpsell = needUpsell;
                var nextButton = new UsageInfoTile({
                    id: nextButtonId,
                    title: nextProfile._profile.name,
                    _profile: nextProfile,
                    gridSize: 'md-4 xs-12',
                    disabled: imDisabled,
                    value: nextProfile._profile.quota,
                    textSuffix: _('GB Storage', self),
                    description: availableText,
                    usageHint: priceString,
                    showPie: false
                });
                profilesList.addChild(nextButton);
            }

            if (profilesCount === disabledProfiles) {
                var noneId = self.genId(tilePrefix + 'none');
                profilesList.addChild(new Tile({
                    id: noneId,
                    _profile: undefined,
                    title: _('None', self),
                    gridSize: 'md-4 xs-12',
                    disabled: false
                }));
                selectedId = noneId;
                profileNeedUpsell = false;
            }
            profilesList.set('selectionArray', getStateful([selectedId]));

            return profileNeedUpsell;
        }
    };
});
