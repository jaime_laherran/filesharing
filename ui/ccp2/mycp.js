define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "aps/_View",
    "aps/ResourceStore",
    "dojo/query",
    "dojox/mvc/getStateful",
    "aps/Status"
], function(declare, Deferred, _View, Store, query, getStateful, Status) {

    var MYCP_TILES = "owc-mycp-FileSharing",
        LOGIN_TILE = "owc-mycp-login",
        MY_STATUS = "owc-mycp-status-user",
        MY_SERVER = "owc-mycp-serverAddress",
        MY_USERNAME = "owc-mycp-userName",
        MY_PROFILE = "owc-mycp-profilelogin",
        MY_USAGE = "owc-mycp-usage",
        DOWNLOAD_TILE = "owc-mycp-download",
        DOWNLOAD_TEXT = "owc-mycp-output-download-apps",

        BTN_DOWNLOAD = "owc-mycp-btn-download",
        BTN_DOWNLOAD_CURRENT = "owc-mycp-btn-download-current",
        BTN_DOWNLOAD_OTHER = "owc-mycp-btn-download-other",
        BTN_ACTIVATE = "owc-mycp-btn-activate",
        BTN_LOGIN = "owc-mycp-btn-login",
        BTN_CHANGE_PWD = "owc-mycp-btn-changepwd",

        LOGIN_FORM = "owc-mycp-urlOpener",
        LOGIN_USER = "owc-mycp-uo-user",
        LOGIN_PWD = "owc-mycp-uo-pwd",

        CHANGE_PWD_POPUP = "change-my-password",
        ACTIVATE_POPUP = "activate-me";

    return declare(_View, {
        init: function() {
            return [
                ["aps/Tiles", { id: this.genId(MYCP_TILES) }, [
                    ["aps/Tile", {
                        gridSize: "md-8 xs-12",
                        title: _('ownCloud Portal'),
                        id: this.genId(LOGIN_TILE),
                        'info': new Status({
                            useIcon: false,
                            id: this.genId(MY_STATUS),
                            status: 'ready',
                            statusInfo: {
                                'notActivated': {'label': _('Not Activated'), 'type': 'warning'},
                                'ready':        {'label': '',                 'type': 'success', 'visible': false}
                            }
                        })
                    },[
                        ["aps/FieldSet", {
                            gridSize: "md-12 xs-12"
                        }, [
                            ["aps/Output", {
                                id: this.genId(MY_SERVER),
                                label: _("Server Address"),
                                gridSize: "md-6 xs-12"
                            }],
                            ["aps/Output", {
                                id: this.genId(MY_USERNAME),
                                label: _('My User Name'),
                                gridSize: "md-6 xs-12"
                            }],
                            ["aps/Output", {
                                id: this.genId(MY_PROFILE),
                                label: _('My Profile'),
                                gridSize: "md-6 xs-12"
                            }],
                            ["aps/Gauge", {
                                id: this.genId(MY_USAGE),
                                label: _('My Storage Usage'),
                                legend: _('__used__ GB of __quota__ GB', {used: '${value}', quota: '${maximum}'}),
                                gridSize: "md-6 xs-12"
                            }]
                        ]]
                    ]],
                    ["aps/Tile", {
                            id: this.genId(DOWNLOAD_TILE),
                            title: _('Apps'),
                            gridSize: "md-4 xs-12",
                            buttons: [{
                                id: this.genId(BTN_DOWNLOAD),
                                label: _('Download App'),
                                iconClass: 'fa-gear',
                                items: [
                                    {
                                        id: this.genId(BTN_DOWNLOAD_CURRENT),
                                        iconClass: 'fa-download',
                                        visible: false
                                    },
                                    {
                                        type: 'separator'
                                    },
                                    {
                                        id: this.genId(BTN_DOWNLOAD_OTHER),
                                        label: _('Other Platforms'),
                                        iconClass: 'fa-download',
                                        onClick: function() {
                                            aps.apsc.gotoView("downloadmycp");
                                        }
                                    }
                                ]
                            }]
                        },[["aps/Container", [
                            ["aps/Output", {
                                id: this.genId(DOWNLOAD_TEXT),
                                content: _('Download apps for Mac OSX,<br>Windows, iOS, Android and other<br>platforms')
                            }]
                        ]]]
                    ],
                    ["<form>", {
                        id: LOGIN_FORM,
                        action: "nourl",
                        method: "post",
                        target: "_blank"
                    },[
                        ["<input>", {
                                name: "user",
                                id: LOGIN_USER,
                                type: "hidden"
                        }],
                        ["<input>", {
                                name: "password",
                                id: LOGIN_PWD,
                                type: "hidden"
                        }]
                    ]]
                ]]
            ];
        }, // Close of Init function
        onContext: function () {
            function detectOs() {
                var myOS, myOSName;

                if (navigator.platform.indexOf("Win") != -1) {
                    myOS = "win";
                    myOSName = _('Windows');
                } else if (navigator.platform.indexOf("Mac") != -1) {
                    myOS = "mac";
                    myOSName = _('Mac OS X');
                } else if (navigator.platform.indexOf("Linux") != -1) {
                    myOS = "linux";
                    myOSName = _('Linux');
                } else if (navigator.platform.indexOf("iPhone") != -1 ||
                           navigator.platform.indexOf("iPod") != -1 ||
                           navigator.platform.indexOf("iPad") != -1) {
                    myOS = "ios";
                    myOSName = _('iPhoneApp');
                } else if (navigator.platform.indexOf("android") != -1) {
                    myOS = "android";
                    myOSName = _('Android App');
                }

                return [myOS, myOSName];
            }

            function drawLogin(ownCloudUser) {
                var deferred = new Deferred(),
                    store = new Store({
                        target: "/aps/2/resources"
                    });

                store.get(ownCloudUser).then(function (ownCloudUserLogin) {
                    var usagedraw = 0;
                    if (ownCloudUserLogin.userusage > 0) {
                        usagedraw = ownCloudUserLogin.userusage;
                    }
                    this.byId(MY_PROFILE).set(
                        "value",
                        _('__name__ (__quota__ GB)', {
                            name: ownCloudUserLogin.assignedprofilename,
                            quota: ownCloudUserLogin.quota
                        })
                    );
                    this.byId(MY_USERNAME).set("value", ownCloudUserLogin.owncloudusername);

                    this.byId(MY_USAGE).set({
                        value: usagedraw,
                        maximum: ownCloudUserLogin.quota
                    });

                    var widget = this.byId(LOGIN_TILE);
                    if (ownCloudUserLogin.aps.status === "aps:activating") {
                        widget.info.set('status', 'notActivated');
                        widget.set({
                            buttons: [
                                {
                                    id: BTN_ACTIVATE,
                                    title: _('Activate'),
                                    type: "warning",
                                    iconClass: "fa-flash",
                                    autoBusy: false,
                                    onClick: function () {
                                        aps.apsc.showPopup({
                                            viewId: ACTIVATE_POPUP,
                                            resourceId: null,
                                            params: {
                                                userApsId: ownCloudUserLogin.aps.id,
                                                userLogin: ownCloudUserLogin.owncloudusername
                                            },
                                            modal: false
                                        });
                                    }
                                }
                            ]
                        });
                    } else {
                        var loginFunc = function() {
                            var ssostore = new Store({
                                target: "/aps/2/resources/" + ownCloudUserLogin.aps.id + "/getssoparam"
                            });
                            ssostore.get().then(function(ssoData) {
                                query("#" + LOGIN_USER)[0].value = ownCloudUserLogin.owncloudusername;
                                query("#" + LOGIN_PWD)[0].value = atob(ssoData.pass);
                                var opener = query("#" + LOGIN_FORM)[0];
                                opener.action = ssoData.url;
                                opener.value = ssoData.url;
                                opener.submit();
                            });
                        };

                        widget.info.set('status', 'ready');
                        widget.set(
                            'buttons', [
                                {
                                    id: BTN_LOGIN,
                                    title: _('Log into ownCloud'),
                                    iconClass: "fa-external-link",
                                    autoBusy: false,
                                    onClick: loginFunc
                                },
                                {
                                    id: BTN_CHANGE_PWD,
                                    title: _('Change Password'),
                                    iconClass: "fa-key",
                                    autoBusy: false,
                                    onClick: function () {
                                        aps.apsc.showPopup({
                                            viewId: CHANGE_PWD_POPUP,
                                            resourceId: null,
                                            params: {
                                                userApsId: ownCloudUserLogin.aps.id
                                            },
                                            modal: false
                                        });
                                    }
                                }
                            ]
                        );
                    }
                    deferred.resolve();
                }.bind(this));
                return deferred;
            }

            function updateDownload() {
                var osInfo = detectOs(),
                    store = new Store ({
                        target: "/aps/2/resources/" + aps.context.vars.enduser.aps.id
                    });

                store.get('getclient').then(function (theclient) {
                    aps.app.model.myglobals = getStateful(theclient);
                    this.byId(MY_SERVER).set("value", aps.app.model.myglobals.accessdomain);
                    if (osInfo[1]) {
                        this.byId(BTN_DOWNLOAD_CURRENT).set({
                            title: osInfo[1],
                            visible: true,
                            onClick: function() {
                                window.open(aps.app.model.myglobals[osInfo[0] + "url"], "_blank");
                            }
                        });
                    }
                }.bind(this));
            }

            var deferred = drawLogin.call(this, aps.context.vars.enduser.aps.id);
            updateDownload.call(this);
            return deferred.then(aps.apsc.hideLoading);
        }
    });// Close of return declare
}); // Close of Main function
