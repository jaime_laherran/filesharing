define([
    "dojo/_base/declare",
    "dojo/Deferred",
    "dojox/mvc/getPlainValue",
    "aps/_View",
    "aps/xhr",
    "aps/Message",
    "./loadProfiles",
    "./profilesTools",
    "./displayError",
    "dojo/text!../json/ownclouduser.json"
], function (
    declare, Deferred, getPlainValue, _View, xhr, Message,
    loadProfiles, profilesTools, displayError, owcUserJson
) {

    var PROFILES_LIST = 'owc-sp-profiles',
        PROFILE_TILE = 'owc-sp-profile-',
        UPSELL_INFO = 'owc-sp-upsell-msg',
        ACCOUNT_CNT = 'owc-sp-acccnt',
        ACCOUNT_PANEL = 'owc-sp-accpanel',
        ACCOUNT_PROPS = 'owc-sp-account',
        USERNAME = 'owc-sp-username',
        PASSWORD = 'owc-sp-password',
        PWD_MIN = 7,
        PWD_MAX = 64;

    function updateInfoMessage(needUpsell) {
        var messageWidget = this.byId(UPSELL_INFO);
        if (!needUpsell) {
            if (messageWidget) messageWidget.destroy();
        } else {
            if (messageWidget) return;
            messageWidget = new Message({
                id: this.genId(UPSELL_INFO),
                type: 'info',
                description: _('Notice: To provide you with the additional profiles, the new order will be created right after you assign selected profile.')
            });
            this.byId("apsPageContainer").get("messageList").addChild(messageWidget);
        }
    }

    function updateUserProfile(user, profile) {
        var profilePromise;

        if (profile.aps.id === user.assignedprofile) {
            profilePromise = new Deferred();
            profilePromise.resolve();
        } else {
            profilePromise = xhr('/aps/2/resources/' + user.aps.id + '/samples', {
                method: 'POST',
                headers: {"Content-Type": 'application/json'},
                data: JSON.stringify({
                    aps: {
                        id: profile.aps.id
                    }
                })
            });
        }

        profilePromise.then(function () {
            var pwd = this.byId(PASSWORD).get('value');
            if (pwd.length === 0) {
                aps.apsc.prev();
            } else {
                user.owncloudpassword = pwd;
                xhr('/aps/2/resources/' + user.aps.id + '/changepassword', {
                    method: 'PUT',
                    headers: {
                        "Content-Type": 'application/json'
                    },
                    data: JSON.stringify(user)
                }).then(function () {
                    aps.apsc.prev();
                }, displayError);
            }
        }.bind(this), displayError);
    }

    function assignUserProfile(userId, profile) {
        var newUser = JSON.parse(owcUserJson);
        newUser.samples.aps.id  = profile.aps.id;
        newUser.user.aps.id = userId;
        newUser.tenant.aps.id = aps.context.vars.tenant.aps.id;
        newUser.owncloudpassword = this.byId(PASSWORD).get('value');
        xhr('/aps/2/resources/' + aps.context.vars.tenant.aps.id+ "/users", {
            method: 'POST',
            headers: {'Content-Type': 'application/json'},
            data: JSON.stringify(newUser)
        }).then(function () {
            aps.apsc.prev();
        }, displayError);
    }

    function buyMoreUserProfile(userId, profile) {
        var newUser = JSON.parse(owcUserJson);
        newUser.samples.aps.id  = profile.aps.id;
        newUser.user.aps.id = userId;
        newUser.tenant.aps.id = aps.context.vars.tenant.aps.id;
        newUser.owncloudpassword = this.byId(PASSWORD).get('value');
        var params = [getPlainValue(newUser)];
        aps.apsc.showPopup({
            viewId: "http://www.parallels.com/ccp-billing#order-confirmation-popup",
            params: {
                data: {
                    "orderType" : "subscription-resource-change",
                    "preorder": {
                        "subscriptionResourceChanges": [{
                            "subscriptionId": aps.context.vars.tenant.TENANTID,
                            "notification": {
                                "resourceId": aps.context.vars.tenant.aps.id,
                                "operation": "upsell",
                                "activationParams": params
                            },
                            "resources": [{
                                "resourceId": profile.resourceId,
                                "amount": 1
                            }]
                        }]
                    },
                    "activation_params": params,
                    "description": _('To assign the requested resources to users you need to place the order and pay for it.')
                }
            }
        }).then(function(data) {
            if (data.btnType == "submit") {
                aps.apsc.prev();
            } else {
                aps.apsc.cancelProcessing();
            }
        });
    }

    function setAccountVisible(isVisible) {
        var pwdWidget = this.byId(PASSWORD);
        if (!isVisible) {
            if (pwdWidget && pwdWidget.get('visible')) pwdWidget.reset();
        }
        var accWidget = this.byId(ACCOUNT_CNT);
        if (accWidget) accWidget.set('visible', isVisible);
    }

    return declare(_View, {
        init: function () {
            var self = this;

            return [
                ["aps/Tiles", {
                    id: this.genId(PROFILES_LIST),
                    title: _('Select Profile'),
                    selectionMode: 'single',
                    required: true,
                    onClick: function() {
                        var selection = self.byId(this.get('selectionArray')[0]);
                        var profile = selection._profile;
                        if (profile) updateInfoMessage.call(self, profile.needUpsell);
                    }
                }],
                ["aps/Container", {
                    id: this.genId(ACCOUNT_CNT),
                    title: _('Setup File Sharing Account'),
                    visible: false
                }, [
                    ["aps/Panel", {
                        id: this.genId(ACCOUNT_PANEL)
                    }, [
                        ["aps/FieldSet", {
                            id: this.genId(ACCOUNT_PROPS)
                        }, [
                            ["aps/Output", {
                                id: this.genId(USERNAME),
                                gridSize: 'xs-12 md-0',
                                label: _("Username")
                            }],
                            ["aps/Password", {
                                id: this.genId(PASSWORD),
                                autoSize: false,
                                gridSize: 'xs-12 md-3',
                                label: _("New Password"),
                                required: false,
                                showStrengthIndicator: true,
                                extraValidator: function(value) {
                                    if (value === null || (value.length > 0 && (value.length < PWD_MIN || value.length > PWD_MAX))) return false;
                                    return true;
                                }
                            }]
                        ]]
                    ]]
                ]]
            ];
        },
        onContext: function() {
            aps.context.subscriptionId = aps.context.vars.tenant.aps.subscription;
            var deferred = new Deferred(),
                userId = aps.context.vars.user.aps.id,
                owcUser;

            if ('params' in aps.context && aps.context.params && 'owcUser' in aps.context.params) {
                owcUser = aps.context.params.owcUser;
            }
            var isOwnService = (aps.context.user.aps.id === userId);


            loadProfiles.getProfilesAmount(aps.context.vars.tenant).then(function (results) {
                aps.apsc.buttonState("submit", {disabled: false});

                var tilesOptions = {additional: 1, used: {}};
                if (owcUser && owcUser.assignedprofile) {
                    tilesOptions.selectedId = owcUser.assignedprofile;
                    tilesOptions.used[owcUser.assignedprofile] = 1;
                }

                updateInfoMessage.call(
                    this,
                    profilesTools.addProfiles(
                        this, this.byId(PROFILES_LIST), PROFILE_TILE, results,
                        tilesOptions
                    )
                );

                var showAccountInfo = !owcUser && isOwnService;
                setAccountVisible.call(this, showAccountInfo);
                if (showAccountInfo) {
                    this.byId(PASSWORD).reset();
                    var userName = this.byId(USERNAME);
                    if (owcUser) {
                        userName.set('value', owcUser.owncloudusername);
                    } else if (userId) {
                        userName.set('value', aps.context.user.login);
                    }
                }
                deferred.resolve();
            }.bind(this));

            return deferred.then(aps.apsc.hideLoading);
        },
        onSubmit: function() {
            var owcUser;

            if ('params' in aps.context && aps.context.params && 'owcUser' in aps.context.params) {
                owcUser = aps.context.params.owcUser;
            }
            var selection = this.byId(this.byId(PROFILES_LIST).get('selectionArray')[0]),
                profile;
            if (!selection || !(profile = selection._profile)) {
                displayError(_('Please select a profile'));
                aps.apsc.cancelProcessing();
                return;
            }

            var pwdInput = this.byId(PASSWORD);
            if(!pwdInput.validate()){
                pwdInput.focus();
                aps.apsc.cancelProcessing();
                return;
            }

            if (profile.needUpsell) {
                buyMoreUserProfile.call(this, aps.context.vars.user.aps.id, profile);
            } else {
                if (owcUser) {
                    updateUserProfile.call(this, owcUser, profile);
                } else {
                    assignUserProfile.call(this, aps.context.vars.user.aps.id, profile);
                }
            }
        },
        onHide: function() {
            setAccountVisible.call(this, false);
        }
    });
});
