define([
    "dojo/_base/declare",
    "dojo/_base/lang",
    "dojo/Deferred",
    "dojo/when",
    "dojox/mvc/getStateful",
    "aps/_View",
    "dijit/registry",
    "./loadProfiles",
    "./profilesTools",
    "../common/TYPES"
], function (
    declare, lang, Deferred, when, getStateful, _View, registry, loadProfiles, profilesTools, TYPES
) {
    "use strict";

    var PROFILES_LIST  = 'owc-meu-profiles-list',
        PROFILE_PREFIX = 'owc-meu-profile-',
        SELECTOR_GROUP    = 'owc-meu-selector',
        SELECTOR_PREFIX   = 'owc-meu-selitem-',
        NONE_ID           = 'none',
        REMOVE_ID         = 'remove',
        ASSIGN_ID         = 'assign',
        WIZARD_DATA_KEY   = 'http://www.parallels.com/ccp-users#massEditUsers',
        WIZARD_ORDER_KEY  = 'http://www.parallels.com/samples/sample-aps2-owncloud-ldap-nextcp#users-edit';

    var profileValue;

    var selectorFunc = function(){
        var selectedTile = registry.byId(this.get('selectionArray')[0]);
        var tilesEnabled = (selectedTile.owcSelectorId === ASSIGN_ID),
            profilesList = registry.byId(PROFILES_LIST),
            nextDisabled = true,
            selection = [];

        profilesList.set({
            visible: tilesEnabled,
            required: tilesEnabled
        });

        if(!tilesEnabled){
            profileValue = selectedTile.owcSelectorId;
            nextDisabled = false;
        }else{
            profileValue = NONE_ID;
            var profilesTiles = profilesList.getChildren();
            for(var idx = 0; idx < profilesTiles.length; idx++){
                var nextProfile = profilesTiles[idx];
                if(!nextProfile.get('disabled')){
                    nextDisabled = false;
                    if(selection.length < 1){
                        profileValue = nextProfile.owcProfileId;
                        selection.push(nextProfile.id);

                    }
                }
            }
        }
        profilesList.set('selectionArray', getStateful([selection[0]]));
        aps.apsc.buttonState("next", {disabled: nextDisabled});
    };

    function recalculate(self) {
        var resultDeferred = new Deferred();

        registry.byId(SELECTOR_GROUP).set(
            'selectionArray', getStateful([SELECTOR_PREFIX + ASSIGN_ID])
        );
        registry.byId(PROFILES_LIST).set({
            visible: true,
            required: true
        });

        var usersToBeChanged = 0,
            keys,
            wizardData = aps.context.wizardData[WIZARD_DATA_KEY],
            selectedProfile,
            wizardOrderData = aps.context.wizardData[WIZARD_ORDER_KEY];

        if (wizardOrderData) {
            selectedProfile = lang.getObject('objects.0.samples.aps.id', false, wizardOrderData);
        }

        keys = Object.keys(wizardData);
        var usedProfiles = {};

        for (var n=0; n < keys.length; ++n) {
            var nData = wizardData[keys[n]];
            var nKeys = Object.keys(nData);
            for (var m=0; m < nKeys.length; ++m) {
                var nextUser = nData[nKeys[m]];

                if (nextUser.aps.type == 'http://parallels.com/aps/types/pa/admin-user/1.2' ||
                    nextUser.aps.type == 'http://parallels.com/aps/types/pa/service-user/1.2')
                {
                    ++usersToBeChanged;
                    if ('services' in nextUser) {
                        var noProfile = true;
                        nextUser.services.forEach(function(service) {
                            if (service.aps.type === TYPES.USERS_TYPE) {
                                noProfile = false;
                                if (service.assignedprofile in usedProfiles) {
                                    ++usedProfiles[service.assignedprofile];
                                } else {
                                    usedProfiles[service.assignedprofile] = 1;
                                }
                            }
                        });
                    }
                }
            }
        }

        loadProfiles.getProfilesAmount(aps.context.vars.tenant).then(function (results) {
            profilesTools.addProfiles(
                self, registry.byId(PROFILES_LIST), PROFILE_PREFIX, results,
                {
                    additionalProfiles: usersToBeChanged,
                    selectedId: selectedProfile,
                    used: usedProfiles
                }
            );
            selectorFunc();
            resultDeferred.resolve();
        });

        return resultDeferred;
    }
    return declare(_View, {
        init: function () {
            return [
                ["aps/Container", [
                    ["aps/Tiles", {
                        id: SELECTOR_GROUP,
                        title: _("Configure File Sharing for Selected Users"),
                        selectionMode: 'single',
                        required: true,
                        onClick: selectorFunc
                    }, [
                        ["aps/Tile", {
                            id: SELECTOR_PREFIX + ASSIGN_ID,
                            title: _("Assign Profile"),
                            owcSelectorId: ASSIGN_ID,
                            showPie: false
                        }, [
                            ["aps/Output", {
                                id: 'owc-meu-assign-out',
                                value: _("Assign some File Sharing profile to all selected users.")
                            }]
                        ]],
                        ["aps/Tile", {
                            id: SELECTOR_PREFIX + REMOVE_ID,
                            title: _("Remove service"),
                            owcSelectorId: REMOVE_ID,
                            showPie: false
                        }, [
                            ["aps/Output", {
                                id: 'owc-meu-remove-out',
                                value: _("Leave selected users without File Sharing service.")
                            }]
                        ]]
                    ]],
                    ["aps/Tiles", {
                        id: PROFILES_LIST,
                        title: _("Select Profile to Assign"),
                        selectionMode: 'single',
                        required: false,
                        onClick: function(){
                            var selectedTile = registry.byId(this.get('selectionArray')[0]);
                            profileValue = selectedTile.owcProfileId;
                        }
                    }]
                ]]
            ];
        },
        onPrev: function(){
            aps.apsc.prev();
        },
        onNext: function(){
            var userStep = aps.context.wizardData[WIZARD_DATA_KEY];
            var result = [],
                toDetete = [];
            if (userStep.objects && userStep.objects.length > 0 && profileValue !== undefined && profileValue !== NONE_ID) {
                var users = userStep.objects;
                users.forEach(function(user) {
                    var isServiceAssigned = false;
                    if (user.services && user.services.length > 0) {
                        user.services.forEach(function(userService) {
                            if (userService.aps.type === TYPES.USERS_TYPE) {
                                if(userService.assignedprofile !== profileValue) {
                                    if (profileValue == REMOVE_ID) {
                                        toDetete.push(userService.aps.id);
                                    } else {
                                        userService.samples = {aps: {id: profileValue}};
                                        result.push(userService);
                                    }
                                }
                                isServiceAssigned = true;
                            }
                        });
                    }
                    if (!isServiceAssigned && profileValue !== REMOVE_ID) {
                        result.push({
                            aps:    {type: TYPES.USERS_TYPE},
                            samples: {aps: {id: profileValue}},
                            tenant:  {aps: {id: aps.context.vars.tenant.aps.id}},
                            user:    {aps: {id: user.aps.id}}
                        });
                    }
                });
            }
            aps.apsc.next({objects: result, objectsToDelete: toDetete});
        },
        onContext: function() {
            return recalculate(this).then(aps.apsc.hideLoading);
        }
    });
});
