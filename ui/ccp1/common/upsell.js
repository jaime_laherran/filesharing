require([
	"aps/ResourceStore",
	"aps/load",
	"aps/Message",
	"aps/TextBox",
	"dojo/Deferred",
	"dojo/when",
	"dojo/promise/all",
	"aps/Memory",
	"dijit/registry",
	"./common/reportError.js",
	"./common/getBillingInfo.js",
	"../../common/TYPES.js",
	"aps/ready!"
], function (Store, load, Message, TextBox, Deferred, when, all, Memory, registry, reportError, getBillingInfo, TYPES) {
	var planLimitsStore = new Memory({ data: [] }),
		currencyInfo,
		billingResource;

	function getPlanLimits() {
		var def = new Deferred();

		all([
			getBillingInfo(aps.context.vars.tenant.aps.id),
            (new Store({ target: "/aps/2/resources?implementing(" + encodeURIComponent(TYPES.SAMPLES_TYPE) + ")" })).query()
		]).then(function (data) {
			var billing = data[0];
			var plans = data[1];
			currencyInfo = billing.currencyDefinition;
			billingResource = billing.billingResource;
			for (var i = 0; i < plans.length; i++) {
				var planRes = billing.getResourceByApsId(plans[i].aps.id);
				if(planRes == null){
					continue;
				}

				var planPbaResource = billing.getPbaResourceByApsId(plans[i].aps.id);

				var planInfo = {};
				planInfo.displayName = plans[i].name;
				planInfo.limit = planRes.limit;
				planInfo.newLimit = planInfo.limit;
				planInfo.usage = planRes.usage;
				planInfo.setupFee = (planPbaResource.setupFee === 0) ? planPbaResource.setupFee.toFixed(currencyInfo.precision) : planPbaResource.setupFee;
				planInfo.recurringFee = (planPbaResource.recurringFee === 0) ? planPbaResource.recurringFee.toFixed(currencyInfo.precision) : planPbaResource.recurringFee;
				planInfo.id = planPbaResource.id;
				planInfo.units = planPbaResource.units;

				if(planPbaResource.maxUnits==-1){
					planInfo.max=planRes.usage+10;
				}
				else{
					planInfo.max=planInfo.limit;
				}
				planInfo.min=planPbaResource.minUnits;
				planLimitsStore.data.push(planInfo);
			}
			def.resolve();
		}, function (err) {
			aps.apsc.cancelProcessing();
			reportError(err);
		});
		return def;
	}

	var renderPrice = function (row, data) {
		var price = currencyInfo.inFrontOfSum ? (currencyInfo.symbol + " " + data) : (data + " " + currencyInfo.symbol);
		return _("__price__ per __units__", {
			"price" : price,
			"units" : row.units
		});
	};
	var el_val = ["aps/PageContainer", {
			id : "page"
		},
		[["aps/FieldSet", {
					title : _("Change Subscription Resources")
				}
			], ["aps/Grid", {
					columns : [{
							name : _("Name"),
							field : "displayName"
						}, {
							name : _("Current Usage / Limit"),
							field : "usage",
							renderCell : function (planInfo) {
								return _("__usage__ / __limit__ __units__", {
									"usage" : planInfo.usage,
									"limit" : planInfo.limit,
									"units" : planInfo.units
								});
							}
						}, {
							name : _("New Limit"),
							field : "newLimit",
							"renderCell" : function (planInfo) {
								return new TextBox({
									size: 5,
									value : planInfo.newLimit,
									onChange : function (value) {
										planInfo.newLimit = value;
									},
									type : "number",
									constraints : {min: planInfo.usage},
									rangeMessage : _("Limit cannot be less than usage")
								});
							}
						}, {
							name : _("Setup Fee"),
							field : "setupFee",
							renderCell : renderPrice
						}, {
							name : _("Recurring Fee"),
							field : "recurringFee",
							renderCell : renderPrice
						}
					],
					id : "subscriptions",
					store : planLimitsStore,
					pageSizeOptions : ["GRID_UNLIMITED"],
					rowsPerPage: 100000
		}
			]]
	];

	when(getPlanLimits(), function () {
		load(el_val);
	});

	var upsellData = {
		limits : [],

		// Name of the application view where UI will navigate after
		// upsell wizard completion (values of parameters of that view
		// will depend on completion status):
		returnView : "http://www.parallels.com/samples/sample-aps2-owncloud-ldap#users",

		// Resource id that should be passed to the view
		// after upsell wizard completion
		//returnViewResourceId : registry.byId("grid").get("selectionArray")[0],
		returnViewResourceId : aps.context.vars.tenant.aps.id,

		notification : {
			// ID of APS Resource that will receive notification
			"resourceId" : aps.context.vars.tenant.aps.id,

			// Operation that will be called on resource
			// Note: this operation must be defined in Type of that resource
			// and "verb" and "path" to execute it will be used from there.
			// See http://debug.dev.aps.sw.ru/doc/spec/type-definition/type-definition.html#operations

			"operation" : "upsell",
			"parameters" : {}
		}
	};
	aps.app.onSubmit = function () {
		var page = registry.byId("page"),
			messages = page.get("messageList");

		messages.removeAll();
		if (!page.validate()) {
			aps.apsc.cancelProcessing();
			return;
		}

		var limits = [];
		for (var i = 0; i < planLimitsStore.data.length; i++) {
			var plan = planLimitsStore.data[i];
			if (plan.newLimit != plan.limit) {
				var limit = {};
				limit.id = plan.id;
				limit.limit = plan.newLimit;
				limits.push(limit);
			}
		}
		if (limits.length === 0) {
			aps.apsc.cancelProcessing();
			messages.addChild(
				new Message({
					type : "error",
					closable : true,
					description : _("No limits were changed")
				}));
			return;
		} else {
			upsellData.notification.resourceId = aps.context.vars.tenant.aps.id;
			upsellData.returnViewResourceId = aps.context.vars.tenant.aps.id;
			upsellData.limits = limits;
			aps.apsc.gotoView({
				resourceId : billingResource.aps.id,
				viewId : "upsell"
			}, null, upsellData);
		}

	};

	aps.app.onCancel = function () {
		aps.apsc.gotoView("usage.summary");
	};
});
