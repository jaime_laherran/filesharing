define(["dijit/registry", "aps/Message"],
	function (registry, Message)
{
	return function reportError(error, keepCurrentMessages){
		function addMsg(page){
			if (page){
				var ml = page.get("messageList");
				if (ml) {
					if ((typeof keepCurrentMessages == "undefined") || !keepCurrentMessages) ml.removeAll();
					var msg = error.responseText;
					if (typeof msg == "undefined")  {
						msg = (error.response.data ? JSON.parse(error.response.data) : {message:"Internal error"}).message;
					}
					ml.addChild(
						new Message(
						{
							type : "error",
							description : msg
						}
					));
				}
			}
		}
		if (error.response){
			var page = registry.byId("page");
			if (page)
				addMsg(page);
		}
	};
});
