define([
	"dojo/_base/declare",
	"dojo/Deferred",
	"dojo/promise/all",
	"./common/displayError.js",
	"aps/xhr"
], function (declare, Deferred, all, displayError, xhr) {
	return function getBillingInfo(contextId) {
		var def = new Deferred(),
			pricesByApsId = {},
			resourcesByApsId = {},
			pricesByProperty = {},
			resourcesByPropery = {},
			// XXX errCbk,
			context,
			billing,
			subscription,
			resourcesArray;

		function errCbk(err) { def.reject(err);	}

		xhr.get("/aps/2/resources/" + contextId).then(function(res){
			context = res;
			return xhr.get("/aps/2/resources?implementing(" + encodeURIComponent("http://www.parallels.com/pba") + ")");
		}).then(function (res) {
			billing = res;

			all([
				xhr.get(context.subscription.aps.href),
				xhr.get(context.subscription.aps.href + "/resources")
			]).then(function (data) {
				subscription = data[0];
				resourcesArray = data[1];

				for (var i = 0; i < resourcesArray.length; i++) {
					if(resourcesArray[i].apsId != null){
						resourcesByApsId[resourcesArray[i].apsId] = resourcesArray[i];
					}
				}

				if(typeof billing === null || billing.length === 0 || typeof resourcesByApsId[billing[0].aps.id] === null){
					def.resolve({ poaResources : resourcesArray });
					return;
				}

				for (var i = 0; i < resourcesArray.length; i++) {
					if (resourcesArray[i].property != null) {
						resourcesByPropery[resourcesArray[i].property] = resourcesArray[i];
					}
				}

				return xhr.get("/aps/2/resources/" + billing[0].aps.id + "/v1/csm/subscriptions/" + subscription.subscriptionId + "/resources");
			}).then(function (pbaResources) {
				var currencyDefinition = pbaResources.currencyDefinition,
					pricesById = {};

				for (var i = 0; i < pbaResources.resources.length; i++) {
					pricesById[pbaResources.resources[i].id] = pbaResources.resources[i];
				}
				// counters
				for (var i = 0; i < resourcesArray.length; i++) {
					var property = resourcesArray[i].property,
						id = resourcesArray[i].id,
						price = pricesById[id];

					if (property != null && price != null) {
						pricesByProperty[property] = price;
					}
				}

				for (var i = 0; i < resourcesArray.length; i++) {
					if(resourcesArray[i].apsId != null){
						pricesByApsId[resourcesArray[i].apsId] = pricesById[resourcesArray[i].id];
					}
				}
				def.resolve({
					billingResource: billing[0],
					currencyDefinition : currencyDefinition,
					pbaResources : pbaResources.resources,
					poaResources : resourcesArray,
					getPbaResourceByApsId: function(apsId){
						return pricesByApsId[apsId];
					},
					getPbaResourceById: function(id){
						return pricesById[id];
					},
					getCounterPbaResource: function(name){
						return pricesByProperty[name];
					},
					getResourceByApsId: function(apsId){
						return resourcesByApsId[apsId];
					},
					getCounterResource: function(name){
						return resourcesByPropery[name];
					}
				});
			}).otherwise(errCbk);
		}).otherwise(function(){
			// no billing, just collect resources
			xhr.get(context.subscription.aps.href + "/resources").then(function (resources) {
				def.resolve({ poaResources : resources });
			}, errCbk);
		});

		return def;
	};
});
