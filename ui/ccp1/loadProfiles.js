define([
    "aps/ResourceStore",
    "dojo/Deferred",
    "dojo/promise/all"
], function (Store, Deferred, all) {
    return function loadProfiles(subscriptionApsId, profilesMemory, types) {
        var def = new Deferred(),

            store = new Store({
                target: "/aps/2/resources/"
            }),
            store2 = new Store({
                target: "/aps/2/resources/"
            }),

            profilesList = [];

        all([store.get(subscriptionApsId + "/resources"),
            store2.query("implementing(" + types.SAMPLES_TYPE + "),sort(-quota)")]).then(function (results) {

            var resources = results[0],
                samples = results[1];

            for (var l=0; l < samples.length; l++) {
                for (var i = 0; i < resources.length; i++) {
                    if (resources[i].apsType === types.SAMPLES_TYPE &&
                        resources[i].apsId === samples[l].aps.id)
                    {
                        var shallBeChecked = false;

                        samples[l].limit = resources[i].limit;
                        samples[l].paresource = resources[i].id;
                        samples[l].usage = resources[i].usage;
                        if (samples[l].limit > samples[l].usage) {
                            samples[l].disabled = false;
                            if (shallBeChecked === false) {
                                shallBeChecked = true;
                                samples[l].checked = true;
                            } else {
                                samples[l].checked = false;
                            }
                        } else {
                            samples[l].checked = false;
                            samples[l].disabled = true;
                        }

                        if (profilesMemory !== undefined) {
                            profilesMemory.put(samples[l]);
                        }
                        profilesList[profilesList.length] = samples[l];
                    }
                }
            }
            def.resolve(profilesList);
        }, function (err) {
            def.reject(err);
        });
        return def;
    };
});
