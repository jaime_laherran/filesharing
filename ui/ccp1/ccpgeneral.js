require([
    "aps/load",
    "dojo/promise/all",
    "dojo/when",
    "aps/Memory",
    "aps/ResourceStore",
    "./loadProfiles.js",
    "../common/TYPES.js",
    "aps/Gauge",
    "dijit/registry",
    "aps/Output",
    "dojox/mvc/at",
    "dojox/mvc/getStateful",
    "aps/ready!"
    ], function(load, all, when, Memory, Store, loadProfiles, TYPES, Gauge, registry, Output, at, getStateful){
        var generalStore = new Store({
            target: "/aps/2/resources"
        });
        when(generalStore.get(aps.context.vars.tenant.aps.id), function(theTenantOnContext){
            var profilesMemory = new Memory({
                idProperty: "aps.id"
            });
            all([
                loadProfiles(theTenantOnContext.subscription.aps.id, profilesMemory, TYPES),
                generalStore.get(theTenantOnContext.owncloudglobals.aps.id)]).then(function(results){
                    var  globalsTenant= results[1];
                    var buyMore = false;
                    if (aps.context.vars.tenant.billing === 1){
                        buyMore = true;
                    }
                    var osData = [
                        {
                            id: "0",
                            label: _('Select OS to start download...'),
                            value: 'None'
                        },
                        {
                            id: "1",
                            label: _('Windows 7+'),
                            value: globalsTenant.winurl
                        },
                        {
                            id: "2",
                            label: _('MacOs X 10.6+'),
                            value: globalsTenant.macurl
                        },
                        {
                            id: "3",
                            label: _('Linux'),
                            value: globalsTenant.linurl
                        }
                    ];
                    var mobileData = [
                        {
                            id: "0",
                            label: _('Select OS to visit app Store...'),
                            value: 'None'
                        },
                        {
                            id: "1",
                            label: _('IOS Devices (Iphone, IPad, IPod)'),
                            value: globalsTenant.iosurl
                        },
                        {
                            id: "2",
                            label: _('Android Devices'),
                            value: globalsTenant.androidurl
                        }
                    ];
                    var selectedDesktop = getStateful({distro: { id: 0 }});
                    var selectedMobile = getStateful({distro: { id: 0 }});
                    var osMemory = new Memory({
                        data: osData,
                        idProperty: "id"
                    });
                    var mobileMemory = new Memory({
                        data: mobileData,
                        idProperty: "id"
                    });
                    when(profilesMemory.query('limit=ne=0,sort(-quota)'), function(profilesToDisplay){
                        load(["aps/PageContainer", [
                            ["aps/ActiveList", {
                                id: "owc-ccp1-general-activelist",
                                cols: 2,
                                sortDescending: true
                            }, [
                                ["aps/ActiveItem", {
                                    collapsible: false,
                                    iconName: "./images/summary-report.png",
                                    title: _('Profiles Overview')
                                }, [
                                    ["aps/FieldSet", { id: "owc-ccp1-general-container-profiles"}],
                                    ["aps/Button", {
                                        id: "BuyMore",
                                        iconName: "/pem/images/icons/shop_32x32.gif",
                                        label: _('Buy More'),
                                        onClick: function(){
                                            aps.apsc.gotoView("upsell");
                                        },
                                        billing: aps.context.vars.tenant.billing,
                                        visible: buyMore
                                    }]
                                ]],
                                ["aps/ActiveItem", {
                                    collapsible: false,
                                    iconName: "/pem/images/icons/download_32x32.gif",
                                    title: _('Download Clients'),
                                    id: "owc-ccp1-download-activeitem"
                                }, [
                                    [ "aps/FieldSet", [
                                        ["aps/Select", {
                                            label: _('Desktop Client'),
                                            id: "aps-select-desktop",
                                            store: osMemory,
                                            value: at(selectedDesktop.distro, "id")
                                        }]
                                    ]],
                                    [ "aps/FieldSet", [
                                        ["aps/Select", {
                                            label: _('Mobile Client'),
                                            id: "aps-select-mobile",
                                            store: mobileMemory,
                                            value: at(selectedMobile.distro, "id")
                                        }]
                                    ]]
                                ]]
                            ]]
                        ]]);
                        function desktopChanged(prop, oldVal, newVal){
                            if(newVal !== "0"){
                                when(osMemory.get(registry.byId("aps-select-desktop").value), function(distroDownload){
                                    window.open(distroDownload.value, "_blank");
                                });
                            }
                        }
                        selectedDesktop.distro.watch("id", desktopChanged);
                        function mobileChanged(prop, oldVal, newVal){
                            if(newVal !== "0"){
                                when(mobileMemory.get(registry.byId("aps-select-mobile").value), function(distroDownload){
                                    window.open(distroDownload.value, "_blank");
                                });
                            }
                        }
                        selectedMobile.distro.watch("id", mobileChanged);
                        for (var i=0; i < profilesToDisplay.length; i++){
                            if(profilesToDisplay[i].limit > 0){
                                registry.byId("owc-ccp1-general-container-profiles").addChild(new Gauge({
                                    id: "owc-profile-" + profilesToDisplay[i].aps.id,
                                    maximum: profilesToDisplay[i].limit,
                                    value: profilesToDisplay[i].usage,
                                    minimum: 0,
                                    label: profilesToDisplay[i].name + " " + _('User Profile'),
                                    legend: "${value} " + _('of') + " ${maximum} " + _('Profiles Used')
                                }));
                            }
                            else{
                                registry.byId("owc-ccp1-general-container-profiles").addChild(new Gauge({
                                    id: "owc-profile-" + profilesToDisplay[i].aps.id,
                                    maximum: profilesToDisplay[i].limit,
                                    value: 0,
                                    minimum: 0,
                                    current: profilesToDisplay[i].usage,
                                    label: profilesToDisplay[i].name + " " + _('User Profile'),
                                    legend: "${current} " + _('of') + " ${maximum} " + _('Profiles Used')
                                }));
                            }
                        }
                    });
                });
        });
    });
